/*
 * TestRdf3xQuery.cpp
 *
 *  Created on: Apr 22, 2009
 *      Author: rhaberla
 */

//#include "test/rdf3xload/rdf3xquery.cpp"
#include <gtest/gtest.h>

namespace {

class Rdf3xQueryTest: public testing::Test {
protected:
	/// Destructor
	~Rdf3xQueryTest();
};

static const char tempFileName[] = "filepartitiontest.tmp";

Rdf3xQueryTest::~Rdf3xQueryTest()
// Destructor
{
	remove(tempFileName);
}

TEST_F(Rdf3xQueryTest,SimpleRead)
{
	// ...
}

}
