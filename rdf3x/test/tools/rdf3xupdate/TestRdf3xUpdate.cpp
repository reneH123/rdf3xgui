/*
 * TestRdf3xUpdate.cpp
 *
 *  Created on: Apr 22, 2009
 *      Author: rhaberla
 */

//#include "test/rdf3xload/rdf3xupdate.cpp"
#include <gtest/gtest.h>

namespace {

class Rdf3xUpdateTest: public testing::Test {
protected:
	/// Destructor
	~Rdf3xUpdateTest();
};

static const char tempFileName[] = "filepartitiontest.tmp";

Rdf3xUpdateTest::~Rdf3xUpdateTest()
// Destructor
{
	remove(tempFileName);
}

TEST_F(Rdf3xUpdateTest,SimpleRead)
{
	// ...
}

}
