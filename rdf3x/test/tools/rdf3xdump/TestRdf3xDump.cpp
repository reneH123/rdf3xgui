/*
 * TestRdf3xDump.cpp
 *
 *  Created on: Apr 22, 2009
 *      Author: rhaberla
 */

//#include "test/rdf3xload/rdf3xdump.cpp"
#include <gtest/gtest.h>

namespace {

class Rdf3xDumpTest: public testing::Test {
protected:
	/// Destructor
	~Rdf3xDumpTest();
};

static const char tempFileName[] = "filepartitiontest.tmp";

Rdf3xDumpTest::~Rdf3xDumpTest()
// Destructor
{
	remove(tempFileName);
}

TEST_F(Rdf3xDumpTest,SimpleRead)
{
	// ...
}

}
