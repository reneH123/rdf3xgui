/*
 * TestFactsSegment.cpp
 *
 *  Created on: Apr 22, 2009
 *      Author: rhaberla
 */
#include "rts/segment/FactsSegment.hpp"
#include "rts/database/Database.hpp"
#include "rts/database/DatabasePartition.hpp"
#include <gtest/gtest.h>
#include <sstream>

using namespace std;

namespace {

 class MyFactsSegmentDecorator: protected FactsSegment{
 private:
 MyFactsSegmentDecorator(DatabasePartition& partition);
 //MyFactsSegmentDecorator(const MyFactsSegment&){}
 //	MyFactsSegmentDecorator(const FactsSegment&); //{}
 public:
 //MyFactsSegmentDecorator(DatabasePartition& partition);
 const char* toString(){
 return "abc";
 }
 };

 MyFactsSegmentDecorator::MyFactsSegmentDecorator(DatabasePartition& partition)
 :FactsSegment(partition)
 {
 //this.FactsSegment(partition);
 }

static const char swapFileName[] = "filepartitiontest.tmp";

class TestFactsSegment: public testing::Test {
public:
	FactsSegment *fs;
	Database db;
	// TODO toString() und writeln() als Decorator in MyFactsSegmentDecorator implementieren
	void writeln();
	const char* toString();
protected:
	void SetUp();
	void TearDown();
};

void TestFactsSegment::SetUp() {
	fs = NULL;
}

void TestFactsSegment::TearDown() {
	EXPECT_NE(fs,(FactsSegment*)NULL);
	delete fs;

	remove(swapFileName);
}

void TestFactsSegment::writeln() {
	// print object's state on cout
	cout << toString() << endl;
}

const char* TestFactsSegment::toString() {
	// FactsSegment's state: (Cardinality,Level1Groups,Level2Groups,GetPages)

	EXPECT_NE(fs,(FactsSegment*)NULL);
	EXPECT_GE(fs->getCardinality(),(unsigned)0);
	EXPECT_GE(fs->getLevel1Groups(),(unsigned)0);
	EXPECT_GE(fs->getLevel2Groups(),(unsigned)0);
	EXPECT_GE(fs->getPages(),(unsigned)0);

	stringstream Str;
	Str << "("
	<< fs->getCardinality() << ","
	<< fs->getLevel1Groups() << ","
	<< fs->getLevel2Groups() << ","
	<< fs->getPages() << ")";

	string ret = Str.str();
	return ret.c_str();
}

TEST_F(TestFactsSegment,SimpleTestNull)
// case: database partition is NULL
{
	fs = new FactsSegment(db.getFirstPartition());
	// Fehler: da fs==NULL sein muss. EXPECT_EQ(fs,(FactsSegment*)NULL)
	//EXPECT_EQ(fs,(FactsSegment*)NULL);
}

TEST_F(TestFactsSegment,SimpleTestEmpty1)
// case: database partition is empty
{
	bool isCreated;

	isCreated = db.create(swapFileName);
	EXPECT_EQ(isCreated,true);

	//...
	//fs = dbp->lookupSegment(DatabasePartition::Tag_Generic); // ...
	//EXPECT_EQ();
	//lookup muss leer zurueckliefern! weitere EXPECTs hier!

	db.close();
}

TEST_F(TestFactsSegment,SimpleTestEmpty2)
// case: database partition is empty
{
	bool isCreated;

	isCreated = db.create(swapFileName);
	EXPECT_EQ(isCreated,true);

	db.getFirstPartition().addSegment(new FactsSegment(db.getFirstPartition()),DatabasePartition::Tag_Generic); /// mit und/oder ohne dieser Zeile
	db.getFirstPartition().addSegment(new FactsSegment(db.getFirstPartition()),123); /// mit und/oder ohne dieser Zeile

	db.close();

	//fs->loadFullFacts();

	EXPECT_STRNE("(0,0,0,0)",toString());
}

/*TEST_F(TestFactsSegment,SimpleTestOneSegment)  // nicht fertig!
 {
 // case: database partition has been ...
 SetUp();
 db = new Database();
 db.create(swapFileName);
 dbp = db.getFirstPartition();

 //structured: fs.getType();    + toString();
 //			  fs.getPartition();
 }*/

/*
 TEST_F(TestFactsSegment,SimpleTestTwoSegment)
 {
 // ... preparation ...
 // f1,f2 erzeugen ! => in andere Testfaelle verschieben!
 //FactsSegment *fs1 = new FactsSegment(*dbp);
 //FactsSegment *fs2 = new FactsSegment(*dbp);
 //EXPECT_NE(fs1,(FactsSegment*)NULL);
 //EXPECT_NE(fs2,(FactsSegment*)NULL);
 //
 //dbp->addSegment(fs1,DatabasePartition::Tag_Generic);
 //dbp->addSegment(fs2,DatabasePartition::Tag_Generic);
 //dbp->create();
 }


 TEST_F(TestFactsSegment,SimpleTestThreeSegment)
 {
 }

 TEST_F(TestFactsSegment,SimpleTestAutoGenSegments1)
 {
 }

 TEST_F(TestFactsSegment,SimpleTestAutoGenSegments2)
 {
 }
 */

// TODO Testfaelle vervollstaendigen

}
