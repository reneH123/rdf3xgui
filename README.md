(C)Copyright, 1st July 2009,  Saarbrücken, Germany


This software (GUI only) is authored by Rene Haberland and licensed under the GNU lesser license version 3, however, license conditions for the GUI may not be more permissive than those for the rdf3x project. The latter part said always dominates and in case of any dispute or uncertainty must always be preferred over the GNU lesser license.

License restrictions shall be as for the rdf3x-project whose authors are Thomas Neumann and Gerhard Weikum.



Author attribution must always be given when used/modified: to Rene Haberland for the GUI-part, and Thomas Neumann and Gerhard Weikum for the rdf3x-project.


The following 3rd-party packages are used for rdf3x with (exclusively) those specific versions added to this repository:
 antLR, POI, jGraph, HtmlParser



The license terms are added to this code repository and may anytime be reviewed at:
   https://www.gnu.org/licenses/lgpl-3.0.html
