DROP DATABASE IF EXISTS `yago`;

CREATE DATABASE `yago`;
CREATE TABLE `yago`.`einstein` (
  `Id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` VARCHAR(45) NOT NULL,
  `predicate` VARCHAR(45) NOT NULL,
  `object` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id`)
)
ENGINE = InnoDB;


USE `yago`;

INSERT INTO `einstein` VALUES (1, 'http://en.wikipedia.org/wiki/Albert Einstein', 'describes', 'Albert Einstein');
INSERT INTO `einstein` VALUES (2, 'Einstein', 'familyNameOf', 'Albert Einstein');
INSERT INTO `einstein` VALUES (3, 'Albert', 'givenNameOf', 'Albert Einstein');
INSERT INTO `einstein` VALUES (4, 'Arthur Schopenhauer', 'influences', 'Albert Einstein');
INSERT INTO `einstein` VALUES (5, 'Baruch Spinoza', 'influences', 'Albert Einstein');
INSERT INTO `einstein` VALUES (6, 'David Hume', 'influences', 'Albert Einstein');
INSERT INTO `einstein` VALUES (7, 'Ernst Mach', 'influences','Albert Einstein');
INSERT INTO `einstein` VALUES (8, 'Moritz Schlick', 'influences','Albert Einstein');
INSERT INTO `einstein` VALUES (9, 'Mileva Maric', 'isMarriedTo','Albert Einstein');
INSERT INTO `einstein` VALUES (10, 'Albert Eienstein', 'means', 'Albert Einstein');
INSERT INTO `einstein` VALUES (11, 'Albert Einstein', 'means', 'Albert Einstein');
INSERT INTO `einstein` VALUES (12, 'Albert LaFache Einstein', 'means', 'Albert Einstein');
INSERT INTO `einstein` VALUES (13, 'Einstein on socialism', 'means','Albert Einstein');
INSERT INTO `einstein` VALUES (14, 'Einstein', 'means','Albert Einstein');
INSERT INTO `einstein` VALUES (50, 'Albert Einstein','bornIn', 'Ulm');
INSERT INTO `einstein` VALUES (51, 'Albert Einstein','bornOnDate', '1879-03-14');
INSERT INTO `einstein` VALUES (52, 'Albert Einstein','diedIn', 'Princeton, New Jersey');
INSERT INTO `einstein` VALUES (53, 'Albert Einstein','diedOnDate', '1955-04-18');
INSERT INTO `einstein` VALUES (54, 'Albert Einstein','graduatedFrom', 'University of Z�rich');
INSERT INTO `einstein` VALUES (55, 'Albert Einstein','hasAcademicAdvisor', 'Alfred Kleiner');
INSERT INTO `einstein` VALUES (56, 'Albert Einstein','type', 'Academics of the Charles University');
INSERT INTO `einstein` VALUES (57, 'Albert Einstein','type', 'American agnostics');
INSERT INTO `einstein` VALUES (58, 'Albert Einstein','type', 'American humanists');
INSERT INTO `einstein` VALUES (59, 'Albert Einstein','type', 'American pacifists');
INSERT INTO `einstein` VALUES (60, 'Albert Einstein','type', 'person');