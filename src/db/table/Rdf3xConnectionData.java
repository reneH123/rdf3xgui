package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class Rdf3xConnectionData extends ConnectionData{
	public Rdf3xConnectionData(String database) {
		super("rdf3x", null, null, database, null, null);
	}
}
