package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class MySqlJDBCConnector extends AbstractJDBCConnector {
	private MySqlJDBCConnector(MySqlConnectionData connectionData) {
		super(connectionData);
	}
	
	public String getQuerySubjects(){
		return "SELECT DISTINCT subject FROM " + getConnectionData().getDatabase() + ".einstein ORDER BY subject";
	}
	public String getQueryObjects(){
		return "SELECT DISTINCT object FROM " + getConnectionData().getDatabase() + ".einstein ORDER BY object";
	}
	public String getQueryPredicates(){
		return "SELECT DISTINCT predicate FROM " + getConnectionData().getDatabase() + ".einstein ORDER BY predicate";
	}
	
	public String getQuerySubjectsPredicate(String selectedTopic){
		return "SELECT subject,predicate FROM " + getConnectionData().getDatabase() + ".einstein WHERE object LIKE '" + selectedTopic + "';";
	}
	
	public String getQueryPredicateObjects(String selectedTopic){
		return "SELECT predicate,object FROM " + getConnectionData().getDatabase() + ".einstein WHERE subject LIKE '" + selectedTopic + "';";
	}

	private static MySqlJDBCConnector instance;

	public static MySqlJDBCConnector getInstance(MySqlConnectionData connectionData) {
		if (instance == null) {
			instance = new MySqlJDBCConnector(connectionData);
		}
		return instance;
	}
	
	public static MySqlJDBCConnector getInstance(){
		return instance;
	}
}