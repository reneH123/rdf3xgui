package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import javax.swing.table.DefaultTableModel;

public abstract class AbstractSerializer{
	private String fileOut;
	private DefaultTableModel model;
	
	protected DefaultTableModel getModel() {
		return model;
	}

	protected void setModel(DefaultTableModel model) {
		this.model = model;
	}

	protected String getFileOut() {
		return fileOut;
	}

	protected void setFileOut(String fileOut) {
		this.fileOut = fileOut;
	}
	
	public AbstractSerializer(String fileOut, DefaultTableModel model) throws SerializerException{
		setFileOut(fileOut);
		setModel(model);
		if (fileOut==null){
			throw new SerializerException("File name cannot be empty in Serializer initialization!");
		}
		if (model==null){
			throw new SerializerException("Default table model is not allowed to null during Serializer initialization!");
		}
	}
	
	protected abstract void serializeTableHead();
	protected abstract void serializeTableBody();
}
