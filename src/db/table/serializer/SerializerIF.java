package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public interface SerializerIF {
	abstract public void serialize();
}