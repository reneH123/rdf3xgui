package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class SerializerException extends Exception {
	private static final long serialVersionUID = 1L;

	public SerializerException(String msg) {
		System.err.println(msg);
	}
}
