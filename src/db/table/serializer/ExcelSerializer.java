package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.table.DefaultTableModel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelSerializer extends AbstractSerializer implements SerializerIF {
	private HSSFWorkbook workBook;
	private HSSFSheet sheet;
	
	public HSSFSheet getSheet() {
		return sheet;
	}

	public void setSheet(HSSFSheet sheet) {
		this.sheet = sheet;
	}

	public HSSFWorkbook getWorkBook() {
		return workBook;
	}

	public void setWorkBook(HSSFWorkbook workBook) {
		this.workBook = workBook;
	}

	public ExcelSerializer(String fileOut, DefaultTableModel model)
	throws SerializerException {
		super(fileOut, model);
		
		setWorkBook(new HSSFWorkbook());
		setSheet(getWorkBook().createSheet());
		getWorkBook().setSheetName(0, "rdf3x export", HSSFWorkbook.ENCODING_COMPRESSED_UNICODE);
	}
		
	@Override
	protected void serializeTableHead() {
		int cols = getModel().getColumnCount();
		if (cols == 0){
			return;
		}
		
		HSSFRow header = getSheet().createRow(0);
		HSSFCell newCell=null;
		for (short k = 0; k < cols; k++) {
			newCell = header.createCell(k);
			newCell.setCellType(HSSFCell.CELL_TYPE_STRING);
			newCell.setCellValue(getModel().getColumnName(k));
		}
	}
	
	@Override
	protected void serializeTableBody() {
		int rows = getModel().getRowCount();
		int cols = getModel().getColumnCount();
		if (rows == 0){
			return;
		}
		
		HSSFRow dataRow;
		HSSFCell newCell=null;
		// skip one row because of the table header
		for (int j = 1; j < rows+1; j++) {
			dataRow = getSheet().createRow(j);
			for (short i = 0; i < cols; i++) {
				newCell = dataRow.createCell(i);
				newCell.setCellType(HSSFCell.CELL_TYPE_STRING);
				newCell.setCellValue((String)getModel().getValueAt(j-1,i));
			}
		}		
	}

	//
	public void serialize() {
		try{
			serializeTableHead();
			serializeTableBody();
			FileOutputStream out = new FileOutputStream(getFileOut());
			getWorkBook().write(out);
			out.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}