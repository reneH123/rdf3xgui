package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.io.FileWriter;

import javax.swing.table.DefaultTableModel;

public class TXTSerializer extends AbstractSerializer implements SerializerIF {
	private String tableHead;
	private String tableBody;

	/**
	 * standard constructor: parameters are mandatory and are not allowed to
	 * null
	 * 
	 * @param fileOut
	 * @param model
	 */
	public TXTSerializer(String fileOut, DefaultTableModel model)
			throws SerializerException {
		super(fileOut, model);
		tableHead = null;
		tableBody = null;
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@Override
	protected void serializeTableHead() {
		int cols = getModel().getColumnCount();

		if (cols == 0) {
			return;
		}

		this.tableHead = "";
		for (int k = 0; k < cols; k++) {
			this.tableHead += getModel().getColumnName(k);
			if (k < cols - 1) {
				this.tableHead += "\t";
			}
		}
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	@Override
	protected void serializeTableBody() {
		int rows = getModel().getRowCount();
		int cols = getModel().getColumnCount();

		if (rows == 0) {
			return;
		}

		this.tableBody = "";
		for (int j = 0; j < rows; j++) {
			String newRow = "";
			for (int i = 0; i < cols; i++) {
				newRow += getModel().getValueAt(j, i);
				if (i < cols - 1) {
					newRow += "\t";
				}
			}
			this.tableBody += newRow;
			if (j < rows - 1) {
				this.tableBody += "\n";
			}
		}
	}

	/**
	 * serialize into file iff table head and table body are not empty
	 */
	public void serialize() {
		serializeTableHead();
		if (this.tableHead == null) {
			return;
		}
		serializeTableBody();
		if (this.tableBody == null) {
			return;
		}
		String msg = this.tableHead + "\n" + this.tableBody;
		try {
			FileWriter fw = new FileWriter(getFileOut());
			fw.write(msg);
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}