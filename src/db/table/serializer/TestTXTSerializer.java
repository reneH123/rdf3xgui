package db.table.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import javax.swing.table.DefaultTableModel;

import junit.framework.TestCase;

public class TestTXTSerializer extends TestCase {
	private TXTSerializer ser;
	private DefaultTableModel mod;

	static final String[] TABLEHEAD = { "Id", "subject", "predicate", "object" };
	static final String[][] TABLE1 = {
			{ "1", "http://en.wikipedia.org/wiki/Albert Einstein", "describes",
					"Albert Einstein" },
			{ "2", "Einstein", "familyNameOf", "Albert Einstein" },
			{ "3", "Albert", "givenNameOf", "Albert Einstein" },
			{ "4", "Arthur Schopenhauer", "influences", "Albert Einstein" }};
	
	static final String TABLE1SERIALIZED = "Id\tsubject\tpredicate\tobject\n" +
	"1\thttp://en.wikipedia.org/wiki/Albert Einstein\tdescribes\tAlbert Einstein\n" +
	"2\tEinstein\tfamilyNameOf\tAlbert Einstein\n" +
	"3\tAlbert\tgivenNameOf\tAlbert Einstein\n" +
	"4\tArthur Schopenhauer\tinfluences\tAlbert Einstein";
	
	static final String[][] TABLE2 = {
			{ "5", "Baruch Spinoza", "influences", "Albert Einstein" },
			{ "6", "David Hume", "influences", "Albert Einstein" },
			{ "7", "Ernst Mach", "influences", "Albert Einstein" },
			{ "8", "Moritz Schlick", "influences", "Albert Einstein" },
			{ "9", "Mileva Maric", "isMarriedTo", "Albert Einstein" },
			{ "10", "Albert Eienstein", "means", "Albert Einstein" },
			{ "11", "Albert Einstein", "means", "Albert Einstein" },
			{ "12", "Albert LaFache Einstein", "means", "Albert Einstein" },
			{ "13", "Einstein on socialism", "means", "Albert Einstein" },
			{ "14", "Einstein", "means", "Albert Einstein" }};
	static final String[][] TABLE3 = {
			{ "50", "Albert Einstein", "bornIn", "Ulm" },
			{ "51", "Albert Einstein", "bornOnDate", "1879-03-14" },
			{ "52", "Albert Einstein", "diedIn", "Princeton, New Jersey" },
			{ "53", "Albert Einstein", "diedOnDate", "1955-04-18" },
			{ "54", "Albert Einstein", "graduatedFrom", "University of Z?rich" },
			{ "55", "Albert Einstein", "hasAcademicAdvisor", "Alfred Kleiner" },
			{ "56", "Albert Einstein", "type",
					"Academics of the Charles University" },
			{ "57", "Albert Einstein", "type", "American agnostics" },
			{ "58", "Albert Einstein", "type", "American humanists" },
			{ "59", "Albert Einstein", "type", "American pacifists" },
			{ "60", "Albert Einstein", "type", "person" }};

	public TestTXTSerializer(String name){
		super(name);
	}
	
	public void testToString() throws SerializerException{
		mod = new DefaultTableModel(TABLE1, TABLEHEAD);		
		ser = new TXTSerializer("tmp.txt",mod);
		assertEquals(ser.toString(), TABLE1SERIALIZED);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
}
