package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import junit.framework.TestCase;

public class TestAbstractJDBCConnector extends TestCase {
	private ConnectionData connectionData;
	private AbstractJDBCConnector connector;

	public TestAbstractJDBCConnector(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		
		connectionData = new MySqlConnectionData("localhost", "3306",
				"test", "root", "superuser");
		connector = MySqlJDBCConnector.getInstance((MySqlConnectionData)connectionData);
		
		connector.connect();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	private String queryLimited1(int limit) {
		return "SELECT * FROM peoplempi p1,peoplempi p2 limit "
				+ new Integer(limit).toString() + ";";
	}

	private String queryLimited2(int limit) {
		return "SELECT id,vorname,name FROM peoplempi limit "
				+ new Integer(limit).toString() + ";";
	}

	private String queryLimited3(String atts) {
		return "SELECT " + atts + " FROM peoplempi;";
	}

	private void setDBProperties(int driver, int host, int port, int database,
			int user, int password) {
		connectionData.setDriver((driver == 0) ? null : "jdbc:mysql");
		connectionData.setHost((host == 0) ? null : "localhost");
		connectionData.setPort((port == 0) ? null : "3306");
		connectionData.setDatabase((database == 0) ? null : "test");
		connectionData.setUser((user == 0) ? null : "root");
		connectionData.setPassword((password == 0) ? null : "superuser");
	}

	public void testUrl() {
		// (driver, host, port, database, user, password)
		setDBProperties(0, 0, 0, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 0, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 1, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 1, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 1, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 0, 1, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 0, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 1, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 1, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 1, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 0, 1, 1, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 0, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 1, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 1, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 1, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 0, 1, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 0, 1, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 1, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 1, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 1, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(0, 1, 1, 1, 1, 1);
		assertNull(connectionData.getURL());

		setDBProperties(1, 0, 0, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 0, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 0, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 0, 0, 1, 1);
		assertNull(connectionData.getURL());

		setDBProperties(1, 0, 1, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 1, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 1, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 0, 1, 0, 1, 1);
		assertNull(connectionData.getURL());

		setDBProperties(1, 1, 0, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 0, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 0, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 0, 0, 1, 1);
		assertNull(connectionData.getURL());

		setDBProperties(1, 1, 1, 0, 0, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 1, 0, 0, 1);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 1, 0, 1, 0);
		assertNull(connectionData.getURL());
		setDBProperties(1, 1, 1, 0, 1, 1);
		assertNull(connectionData.getURL());

		// Positive F�lle: (1, ?, ?, 1, ?, ?)
		setDBProperties(1, 0, 0, 1, 0, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://test"));
		
		setDBProperties(1, 0, 0, 1, 0, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://test&password=superuser"));
		
		setDBProperties(1, 0, 0, 1, 1, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://test?user=root"));
				
		setDBProperties(1, 0, 0, 1, 1, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://test?user=root&password=superuser"));
		
		setDBProperties(1, 0, 1, 1, 0, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://:3306/test"));

		setDBProperties(1, 0, 1, 1, 0, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://:3306/test&password=superuser"));
		
		setDBProperties(1, 0, 1, 1, 1, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://:3306/test?user=root"));
		
		setDBProperties(1, 0, 1, 1, 1, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://:3306/test?user=root&password=superuser"));
		
		setDBProperties(1, 1, 0, 1, 0, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost/test"));
		setDBProperties(1, 1, 0, 1, 0, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost/test&password=superuser"));
		setDBProperties(1, 1, 0, 1, 1, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost/test?user=root"));

		setDBProperties(1, 1, 0, 1, 1, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost/test?user=root&password=superuser"));
		setDBProperties(1, 1, 1, 1, 0, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost:3306/test"));
		setDBProperties(1, 1, 1, 1, 0, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost:3306/test&password=superuser"));
		setDBProperties(1, 1, 1, 1, 1, 0);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost:3306/test?user=root"));
		setDBProperties(1, 1, 1, 1, 1, 1);
		assertNotNull(connectionData.getURL());
		assertTrue(connectionData.getURL().equals("jdbc:mysql://localhost:3306/test?user=root&password=superuser"));
	}

	// -1,[0,1,3,8,9],10
	public void testCountRows() {
		// -1
		boolean b = connector.query(queryLimited1(-1));
		assertFalse(b);
		assertEquals(connector.countColumns(), 0);
		// 0
		b = connector.query(queryLimited1(0));
		assertTrue(b);
		assertEquals(connector.countRows(), 0);
		// 1
		b = connector.query(queryLimited1(1));
		assertTrue(b);
		assertEquals(connector.countRows(), 1);
		// 3
		b = connector.query(queryLimited1(3));
		assertTrue(b);
		assertEquals(connector.countRows(), 3);
		// 8
		b = connector.query(queryLimited1(8));
		assertTrue(b);
		assertEquals(connector.countRows(), 8);
		// 9
		b = connector.query(queryLimited1(9));
		assertTrue(b);
		assertEquals(connector.countRows(), 9);
		// 10
		b = connector.query(queryLimited1(10));
		assertTrue(b);
		assertEquals(connector.countRows(), 9);
	}

	public void testCountColumns() {
		boolean b = connector.query(queryLimited3(""));
		assertFalse(b);
		assertEquals(connector.countColumns(), 0);

		// {id}
		b = connector.query(queryLimited3("id"));
		assertTrue(b);
		assertEquals(connector.countColumns(), 1);

		// {id,vorname}
		b = connector.query(queryLimited3("id,vorname"));
		assertTrue(b);
		assertEquals(connector.countColumns(), 2);

		// {id,vorname,name}
		b = connector.query(queryLimited3("id,vorname,name"));
		assertTrue(b);
		assertEquals(connector.countColumns(), 3);

		// illegal: {id,vorname,name,x}
		b = connector.query(queryLimited3("id,vorname,name,x"));
		assertFalse(b);
		assertEquals(connector.countColumns(), 0);
	}

	public void testGetColNames() {
		boolean b = connector.query(queryLimited3(""));
		assertFalse(b);
		String[] cols = connector.getColNames();
		assertNull(cols);

		// {id}
		b = connector.query(queryLimited3("id"));
		assertTrue(b);
		cols = connector.getColNames();
		assertNotNull(cols);
		assertEquals(cols.length, 1);
		assertTrue(cols[0].equals("id"));

		// {id,vorname}
		b = connector.query(queryLimited3("id,vorname"));
		assertTrue(b);
		cols = connector.getColNames();
		assertNotNull(cols);
		assertEquals(cols.length, 2);
		assertTrue(cols[0].equals("id"));
		assertTrue(cols[1].equals("vorname"));

		// {id,vorname,name}
		b = connector.query(queryLimited3("id,vorname,name"));
		assertTrue(b);
		cols = connector.getColNames();
		assertNotNull(cols);
		assertEquals(cols.length, 3);
		assertTrue(cols[0].equals("id"));
		assertTrue(cols[1].equals("vorname"));
		assertTrue(cols[2].equals("name"));

		// illegal: {id,vorname,name,x}
		b = connector.query(queryLimited3("id,vorname,name,x"));
		assertFalse(b);
		cols = connector.getColNames();
		assertNull(cols);
	}

	public void testGetFirstCol() {
		// no rows
		boolean b = connector.query("SELECT id,vorname,name FROM peoplempi limit 0");
		assertTrue(b);
		assertNull(connector.getFirstCol());

		// 1 row and 1 col
		b = connector.query("SELECT id FROM peoplempi limit 1");
		assertTrue(b);
		String[] res = connector.getFirstCol();
		assertNotNull(res);
		assertEquals(res.length, 1);
		assertTrue(res[0].equals("1"));

		// 1 row and many cols
		b = connector.query("SELECT id,vorname,name FROM peoplempi limit 1");
		assertTrue(b);
		res = connector.getFirstCol();
		assertNotNull(res);
		assertEquals(res.length, 1);
		assertTrue(res[0].equals("1"));

		// many rows and 1 col
		b = connector.query("SELECT id FROM peoplempi");
		assertTrue(b);
		res = connector.getFirstCol();
		assertNotNull(res);
		assertEquals(res.length, 3);
		assertTrue(res[0].equals("1"));
		assertTrue(res[1].equals("2"));
		assertTrue(res[2].equals("3"));

		// many rows and many cols
		b = connector.query("SELECT id,vorname,name FROM peoplempi");
		assertTrue(b);
		res = connector.getFirstCol();
		assertNotNull(res);
		assertEquals(res.length, 3);
		assertTrue(res[0].equals("1"));
		assertTrue(res[1].equals("2"));
		assertTrue(res[2].equals("3"));
	}

	public void testGetRows() {
		boolean b;
		String[][] rows = null;

		// no rows (wrong query)
		b = connector.query("xyz");
		assertFalse(b);
		assertNull(connector.getRows());

		// no rows (no result)
		b = connector.query(queryLimited2(0));
		assertTrue(b);
		rows = connector.getRows();
		assertNull(connector.getRows());

		// one row
		b = connector.query(queryLimited2(1));
		assertTrue(b);
		rows = connector.getRows();
		assertNotNull(rows);
		assertEquals(rows.length, 1);
		assertEquals(rows[0].length, 3);
		assertTrue(rows[0][0].equals("1"));
		assertTrue(rows[0][1].equals("rene"));
		assertTrue(rows[0][2].equals("haberland"));

		// two rows
		b = connector.query(queryLimited2(2));
		assertTrue(b);
		rows = connector.getRows();
		assertNotNull(rows);
		assertEquals(rows.length, 2);
		assertEquals(rows[0].length, 3);
		assertTrue(rows[0][0].equals("1"));
		assertTrue(rows[0][1].equals("rene"));
		assertTrue(rows[0][2].equals("haberland"));
		assertTrue(rows[1][0].equals("2"));
		assertTrue(rows[1][1].equals("thomas"));
		assertTrue(rows[1][2].equals("neumann"));

		// illegal row counts (=4)
		b = connector.query(queryLimited2(4));
		assertTrue(b);
		rows = connector.getRows();
		assertNotNull(rows);
		assertEquals(rows.length, 3);
		assertEquals(rows[0].length, 3);
		assertTrue(rows[0][0].equals("1"));
		assertTrue(rows[0][1].equals("rene"));
		assertTrue(rows[0][2].equals("haberland"));
		assertTrue(rows[1][0].equals("2"));
		assertTrue(rows[1][1].equals("thomas"));
		assertTrue(rows[1][2].equals("neumann"));
		assertTrue(rows[2][0].equals("3"));
		assertTrue(rows[2][1].equals("gerhard"));
		assertTrue(rows[2][2].equals("weikum"));
	}

}