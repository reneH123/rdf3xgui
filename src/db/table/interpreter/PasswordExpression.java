package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class PasswordExpression extends JDBCExpression {

	private EmptyLiteral emptyLiteral;
	private String password;
	
	
	public PasswordExpression(String password) {
		setPassword(password);
		setEmptyLiteral(null);
	}
	
	@Override
	public String toString() {
		String ret;
		
		ret="password=" + getPassword();
		if (emptyLiteral!=null){
			return ret;
		}
		
		setErrorMessage("Missing end after password!");
		return null;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public EmptyLiteral getEmptyLiteral() {
		return emptyLiteral;
	}

	public void setEmptyLiteral(EmptyLiteral emptyLiteral) {
		this.emptyLiteral = emptyLiteral;
	}
}