package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class PortExpression extends JDBCExpression {

	private DatabaseExpression databaseExpression;
	private String port;
	
	public PortExpression(String port) {
		setPort(port);
		setDatabaseExpression(null);
	}
	
	@Override
	public String toString() {
		String ret;
		String toString;
		
		ret=getPort();
		
		if (databaseExpression!=null){
			ret+="/";
			toString = databaseExpression.toString();
			if (toString==null){
				return null;
			}
			ret+=toString;
			return ret;
		}
		setErrorMessage("Missing database expression after port expression!");
		return null;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public DatabaseExpression getDatabaseExpression() {
		return databaseExpression;
	}

	public void setDatabaseExpression(DatabaseExpression databaseExpression) {
		this.databaseExpression = databaseExpression;
	}
}