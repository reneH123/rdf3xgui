package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class EmptyLiteral extends JDBCExpression {
	
	@Override
	public String toString() {
		return "";
	}
}