package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class DriverExpression extends JDBCExpression {
	private HostExpression hostExpression;
	private PortExpression portExpression;
	private DatabaseExpression databaseExpression;
	private String driver;

	public DriverExpression(String driver) {
		setDriver(driver);
		setHostExpression(null);
		setPortExpression(null);
		setDatabaseExpression(null);
	}

	@Override
	public String toString() {
		String ret;
		String toString;

		//ret = getDriver();
		ret = getDriver() + "://";  // neu!
		
		if (hostExpression != null) {
			//ret += "://";
			toString = hostExpression.toString();
			if (toString == null) {
				return null;
			}
			ret += toString;
			return ret;
		} else if (portExpression != null) {
			ret += ":";
			toString = portExpression.toString();
			if (toString == null) {
				return null;
			}
			ret += toString;
			return ret;
		} else if (databaseExpression != null) {
			//ret += "/";
			toString = databaseExpression.toString();
			if (toString == null) {
				return null;
			}
			ret += toString;
			return ret;
		}

		setErrorMessage("Missing Host, Port or Database after Driver expression!");
		return null;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public HostExpression getHostExpression() {
		return hostExpression;
	}

	public void setHostExpression(HostExpression hostExpression) {
		this.hostExpression = hostExpression;
	}

	public PortExpression getPortExpression() {
		return portExpression;
	}

	public void setPortExpression(PortExpression portExpression) {
		this.portExpression = portExpression;
	}

	public DatabaseExpression getDatabaseExpression() {
		return databaseExpression;
	}

	public void setDatabaseExpression(DatabaseExpression databaseExpression) {
		this.databaseExpression = databaseExpression;
	}

}