package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public abstract class JDBCExpression {
  private String errorMessage=null;
  
  protected void setErrorMessage(String errorMessage){
	  this.errorMessage = errorMessage;
  }
  public String getErrorMessage(){
	  return errorMessage;
  }
}