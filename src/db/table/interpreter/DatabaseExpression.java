package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class DatabaseExpression extends JDBCExpression {

	private UserExpression userExpression;
	private PasswordExpression passwordExpression;
	private EmptyLiteral emptyLiteral;
	private String database;

	public DatabaseExpression(String database) {
		setDatabase(database);
		setUserExpression(null);
		setPasswordExpression(null);
		setEmptyLiteral(null);
	}

	@Override
	public String toString() {
		String ret;
		String toString;
		
		ret = getDatabase();

		if (userExpression != null) {
			ret += "?";
			toString = userExpression.toString();
			if (toString==null){
				return null;
			}
			ret += toString;
			return ret;
		} else if (passwordExpression != null) {
			ret += "&";
			toString = passwordExpression.toString();
			if (toString==null){
				return null;
			}
			ret += toString;
			return ret;
		} else if (emptyLiteral != null) {
			return ret;
		}

		setErrorMessage("Missing user, password or end after database expression!");
		return null;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public UserExpression getUserExpression() {
		return userExpression;
	}

	public void setUserExpression(UserExpression userExpression) {
		this.userExpression = userExpression;
	}

	public PasswordExpression getPasswordExpression() {
		return passwordExpression;
	}

	public void setPasswordExpression(PasswordExpression passwordExpression) {
		this.passwordExpression = passwordExpression;
	}

	public EmptyLiteral getEmptyLiteral() {
		return emptyLiteral;
	}

	public void setEmptyLiteral(EmptyLiteral emptyLiteral) {
		this.emptyLiteral = emptyLiteral;
	}
}