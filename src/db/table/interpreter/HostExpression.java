package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class HostExpression extends JDBCExpression {
	private DatabaseExpression databaseExpression;
	private PortExpression portExpression;
	private String host;
	
	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}


	public HostExpression(String host) {
		setHost(host);
		setDatabaseExpression(null);
		setPortExpression(null);
	}
	
	@Override
	public String toString() {
		String ret;
		String toString;
		
		ret = getHost();
		
		if (portExpression!=null){
			ret+=":";
			toString = portExpression.toString();
			if (toString==null){
				return null;
			}
			ret+=toString;
			return ret;
		} else if (databaseExpression!=null){
			ret+="/";
			toString = databaseExpression.toString();
			if (toString==null){
				return null;
			}
			ret+=toString;
			return ret;
		}		
		setErrorMessage("Missing port or database after host expression!");
		return null;
	}
		
	public DatabaseExpression getDatabaseExpression() {
		return databaseExpression;
	}

	public void setDatabaseExpression(DatabaseExpression databaseExpression) {
		this.databaseExpression = databaseExpression;
	}

	public PortExpression getPortExpression() {
		return portExpression;
	}

	public void setPortExpression(PortExpression portExpression) {
		this.portExpression = portExpression;
	}
}