package db.table.interpreter;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class UserExpression extends JDBCExpression {

	private PasswordExpression passwordExpression;
	private EmptyLiteral emptyLiteral;
	private String user;
	
	public UserExpression(String user) {
		setUser(user);
		setPasswordExpression(null);
		setEmptyLiteral(null);
	}
	
	@Override
	public String toString() {
		String ret;
		String toString;
		
		ret="user=" + getUser();
		if (passwordExpression!=null){
			ret+="&";
			toString = passwordExpression.toString();
			if (toString==null){
				return null;
			}
			ret+=toString;
			return ret;
		} else if (emptyLiteral!=null){
			return ret;
		}
		
		setErrorMessage("Missing password or end after user expression!");
		return null;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public PasswordExpression getPasswordExpression() {
		return passwordExpression;
	}

	public void setPasswordExpression(PasswordExpression passwordExpression) {
		this.passwordExpression = passwordExpression;
	}

	public EmptyLiteral getEmptyLiteral() {
		return emptyLiteral;
	}

	public void setEmptyLiteral(EmptyLiteral emptyLiteral) {
		this.emptyLiteral = emptyLiteral;
	}
}