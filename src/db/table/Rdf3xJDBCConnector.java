package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class Rdf3xJDBCConnector extends AbstractJDBCConnector {
	/**
	 * URI: rdf3x://db
	 * 
	 * @param database
	 */
	private Rdf3xJDBCConnector(Rdf3xConnectionData connectionData) {
		super(connectionData);
	}

	public String getQuerySubjects(){
		String query = "SELECT DISTINCT ?s WHERE {?s ?p ?o.}"; // BUG: ORDER BY ?s";
		return query;
	}
	public String getQueryObjects(){
		String query = "SELECT DISTINCT ?o WHERE {?s ?p ?o.}"; // BUG: ORDER BY ?o";
		return query;
	}
	public String getQueryPredicates(){
		String query = "SELECT DISTINCT ?p WHERE {?s ?p ?o.}"; // BUG: ORDER BY ?p";
		return query;
	}
	
	public String getQuerySubjectsPredicate(String selectedTopic){
		String query = "SELECT ?s ?p WHERE {?s ?p " + selectedTopic + "}";
		return query;
	}
	
	public String getQueryPredicateObjects(String selectedTopic){
		String query = "SELECT ?p ?o WHERE {" + selectedTopic + " ?p ?o}";
		return query;
	}

	private static Rdf3xJDBCConnector instance;

	public static Rdf3xJDBCConnector getInstance(Rdf3xConnectionData connectionData) {
		if (instance == null) {
			instance = new Rdf3xJDBCConnector(connectionData);
		}
		return instance;
	}
	
	public static Rdf3xJDBCConnector getInstance(){
		return instance;
	}	
}