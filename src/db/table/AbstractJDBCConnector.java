package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public abstract class AbstractJDBCConnector {
	private ConnectionData connectionData;
	private Connection connection;
	private Statement statement;

	private String[] colNames;
	private String[][] rows;

	public abstract String getQuerySubjects();

	public abstract String getQueryObjects();

	public abstract String getQueryPredicates();

	public abstract String getQuerySubjectsPredicate(String topic);

	public abstract String getQueryPredicateObjects(String topic);

	/**
	 * standard constructor, must be explicitly called by each subclass
	 */
	public AbstractJDBCConnector(ConnectionData connectionData) {
		this.connectionData = connectionData;
		connection = null;
		statement = null;

		rows = null;
		colNames = null;
	}

	/**
	 * internal connect method for JDBC connections
	 * 
	 */
	public final void connect() {
		try {
			connection = DriverManager.getConnection(connectionData.getURL());
			statement = connection.createStatement();
		} catch (SQLException e) {
			System.err.println("Exception: " + e.getMessage());
			System.err.println("State: " + e.getSQLState());
			System.err.println("VendorError: " + e.getErrorCode());
			System.err.println("URL: " + connectionData.getURL());
		}
	}

	public final void disconnect() throws Throwable {
		if (connection == null) {
			return;
		}
		connection.close();
	}

	/**
	 * destructor shutting down JDBC connection
	 */
	@Override
	protected final void finalize() throws Throwable {
		connection.close();
		super.finalize();
	}

	/**
	 * process the given SQL/RDF statement 'query'
	 * 
	 * example: SELECT * FROM peoplempi
	 * 
	 * @param query
	 * @return true, if successful, otherwise false
	 */
	public final boolean query(String query) {
		try {
			rows = null;
			colNames = null;

			ResultSet resultSet = statement.executeQuery(query);
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			// 1. colNames auslesen:
			int cols = resultSetMetaData.getColumnCount();

			colNames = new String[cols];
			for (int index = 0; index < cols; index++) {
				colNames[index] = resultSetMetaData.getColumnLabel(index + 1);
			}

			// 2. rows auslesen:
			LinkedList<String[]> ret = new LinkedList<String[]>();
			
			if (resultSet.first()){
				do{
					String[] newRow = new String[cols];
					for (int i = 0; i < cols; i++) {
						newRow[i] = resultSet.getString(i + 1);
					}
					ret.add(newRow);
				}while (resultSet.next());
			}

			// rows puffern
			int maxRows = ret.size();

			if (maxRows <= 0) {
				return true;
			}

			this.rows = new String[maxRows][cols];

			for (int j = 0; j < maxRows; j++) {
				String[] row = ret.get(j);
				for (int i = 0; i < cols; i++) {
					this.rows[j][i] = row[i];
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * columns in the result set
	 * 
	 * 0 indicates that there had not been a valid result set or result set is
	 * empty
	 * 
	 * @return int
	 */
	public final int countColumns() {
		if (colNames == null) {
			return 0;
		}
		return colNames.length;
	}

	/**
	 * rows in the result set
	 * 
	 * precondition/postcondition: pointer on result set before first element
	 * 
	 * 0 indicates that there is no valid result set or result set is empty
	 * 
	 * @return int
	 */
	public final int countRows() {
		if (rows == null) {
			return 0;
		}
		return rows.length;
	}

	/**
	 * name list of all table columns
	 * 
	 * @return String[]
	 */
	public final String[] getColNames() {
		return this.colNames;
	}

	/**
	 * all rows (data) of the table as noninterpreted strings
	 * 
	 * empty and not interpreted result sets will return 'null'
	 * 
	 * @return String[][]
	 */
	public final String[][] getRows() {
		return this.rows;
	}

	/**
	 * returns the first row of a query
	 * 
	 * @return String[]
	 */
	public final String[] getFirstCol() {
		if (this.rows == null) {
			return null;
		}
		String[] result = new String[this.rows.length];
		for (int row = 0; row < this.rows.length; row++) {
			result[row] = this.rows[row][0];
		}
		return result;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection conn) {
		this.connection = conn;
	}

	public void setStatement(Statement stmt) {
		this.statement = stmt;
	}

	public ConnectionData getConnectionData() {
		return connectionData;
	}

	public void setConnectionData(ConnectionData connectionData) {
		this.connectionData = connectionData;
	}
}