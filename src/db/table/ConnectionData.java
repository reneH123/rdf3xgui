package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import db.table.interpreter.DatabaseExpression;
import db.table.interpreter.DriverExpression;
import db.table.interpreter.EmptyLiteral;
import db.table.interpreter.HostExpression;
import db.table.interpreter.PasswordExpression;
import db.table.interpreter.PortExpression;
import db.table.interpreter.UserExpression;

public abstract class ConnectionData {
	private String driver;
	private String host;
	private String port;
	private String database;
	private String user;
	private String password;
	private String url;
	
	public ConnectionData(String driver, String host, String port, String database, String user, String password) {
		this.driver = driver;
		this.host = host;
		this.port = port;
		this.database = database;
		this.user = user;
		this.password = password;
		buildURL();
	}
	
	
	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
		buildURL();
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
		buildURL();		
	}
	
	public String getPort(){
		return port;
	}
	
	public void setPort(String port){
		this.port = port;
		buildURL();
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
		buildURL();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
		buildURL();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
		buildURL();
	}
	
	public String getURL(){
		return url;
	}
	
	public void setURL(String url){
		this.url = url;
	}
	
	/**
	 * internal helper, builds a valid JDBC location expression from (driver,host,port,db,user,password) 	
	 */
	private void buildURL(){
		EmptyLiteral eps;
		eps=new EmptyLiteral();
		
		PasswordExpression pswdExpr;
		if (getPassword()==null){
			pswdExpr=null; 
		} else{
			pswdExpr = new PasswordExpression(getPassword());
			pswdExpr.setEmptyLiteral(eps);
		}		
		
		UserExpression usrExpr;
		if (getUser()==null){
			usrExpr = null;
		} else {
			usrExpr = new UserExpression(getUser());
			usrExpr.setPasswordExpression(pswdExpr);
			usrExpr.setEmptyLiteral(eps);
		}		

		DatabaseExpression dbExpr;
		if (getDatabase()==null){
			dbExpr = null;
		} else{
			dbExpr = new DatabaseExpression(getDatabase());
			dbExpr.setUserExpression(usrExpr);
			dbExpr.setPasswordExpression(pswdExpr);
			dbExpr.setEmptyLiteral(eps);
		}
		
		PortExpression portExpr;
		if (getPort()==null){
			portExpr = null;
		} else{
			portExpr = new PortExpression(getPort());
			portExpr.setDatabaseExpression(dbExpr);
		}
				
		HostExpression hostExpr;
		if (getHost()==null){
			hostExpr = null;
		} else{
			hostExpr = new HostExpression(getHost());
			hostExpr.setPortExpression(portExpr);
			hostExpr.setDatabaseExpression(dbExpr);
		}				
		
		DriverExpression expr;
		// invalid URL
		if (getDriver()==null){
			expr = null;
			setURL(null);
		} else{
			expr = new DriverExpression(getDriver());
			expr.setHostExpression(hostExpr);
			expr.setPortExpression(portExpr);
			expr.setDatabaseExpression(dbExpr);
			// traverse expression and generate String
			setURL(expr.toString());
		}		
	}
}
