package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class MySqlConnectionData extends ConnectionData {

	/**
	 * URL: jdbc:mysql://host/db?user=anonymous&password=123
	 * 
	 */
	public MySqlConnectionData(String host, String port, String database,
			String user, String password) {
		super("jdbc:mysql", host, port, database, user, password);
	}
}
