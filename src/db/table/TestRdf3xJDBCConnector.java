package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import junit.framework.TestCase;

public class TestRdf3xJDBCConnector extends TestCase {
	private Rdf3xJDBCConnector connector;
	private Rdf3xConnectionData connectionData;

	public TestRdf3xJDBCConnector(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		
		connectionData = new Rdf3xConnectionData("test");
		connector = Rdf3xJDBCConnector.getInstance(connectionData);
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void test() {
	}
}
