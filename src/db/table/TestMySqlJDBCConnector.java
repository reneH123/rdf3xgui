package db.table;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import junit.framework.TestCase;

public class TestMySqlJDBCConnector extends TestCase {
	private MySqlJDBCConnector connector;
	private MySqlConnectionData connectionData;

	public TestMySqlJDBCConnector(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		connectionData = new MySqlConnectionData("localhost", "3306", "test",
				"root", "superuser");
		connector = MySqlJDBCConnector.getInstance(connectionData);
	}

	public void test() {
	}
}