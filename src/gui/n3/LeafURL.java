package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class LeafURL extends Node {
	private Predicate ref;

	public Predicate getRef() {
		return ref;
	}

	public void setRef(Predicate ref) {
		this.ref = ref;
	}

	@Override
	public String toString() {
		// use 'PrettyPrinter' implementation for 'Entity' objects
		return null;
	}

	public LeafURL(String name) {
		ref = null;
		setName(name);
	}

	public LeafURL(String name, Predicate ref) {
		this.ref = ref;
		setName(name);
	}
}