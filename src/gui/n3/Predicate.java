package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.LinkedList;

/**
 * Concrete Composite for Predicates
 * 
 * offset is needed for leaves management for limited html views
 * 
 * @author rene
 * 
 */

public class Predicate extends Node {
	private N3Entity ref;
	private LinkedList<LeafURL> children;
	private int offset = 0;
	
	public void addOffset(int diff){
		offset+=diff;
	}
	public int getOffset(){
		return offset;
	}
	public void setOffset(int offset){
		this.offset = offset;
	}
	

	public N3Entity getRef() {
		return ref;
	}

	public void setRef(N3Entity ref) {
		this.ref = ref;
	}

	public LinkedList<LeafURL> getChildren() {
		return children;
	}

	public void setChildren(LinkedList<LeafURL> children) {
		this.children = children;
	}

	public void addLeafURL(LeafURL child) {
		if (children == null) {
			children = new LinkedList<LeafURL>();
		}
		children.add(child);
	}

	@Override
	public String toString() {
		// use 'PrettyPrinter' implementation for 'Entity' objects
		return null;
	}

	public Predicate(String name) {
		ref = null;
		children = null;
		setName(name);
	}

	public Predicate(String name, N3Entity ref, LinkedList<LeafURL> children) {
		this.ref = ref;
		this.children = children;
		setName(name);
	}
}