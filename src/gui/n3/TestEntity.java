package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.LinkedList;

import junit.framework.TestCase;

public class TestEntity extends TestCase {
	private N3Entity entity;
	
	// einfaches Pr�dikat ohne LeafURL
	private Predicate pred1;
	private Predicate pred2;
	// einfaches LeafURL
	private LeafURL leaf1;
	// einfaches LeafURL
	private LeafURL leaf2;
	// einfaches Pr�dikat mit einem LeafURL
	private Predicate pred12;
	// einfaches Pr�dikat mit einem LeafURL
	private Predicate pred21;
	// einfaches Pr�dikat mit beliebiger Anzahl von LeafURL
	private Predicate pred1n;
	private Predicate pred1n2;
	

	static public N3Entity buildTestEinstein() {
		LinkedList<Predicate> subjects = new LinkedList<Predicate>();

		Predicate p11 = new Predicate("describes");
		p11.addLeafURL(new LeafURL(
				"http://en.wikipedia.org/wiki/Albert Einstein"));
		subjects.add(p11);

		Predicate p12 = new Predicate("familyNameOf");
		p12.addLeafURL(new LeafURL("Einstein"));
		subjects.add(p12);

		Predicate p13 = new Predicate("givenNameOf");
		p13.addLeafURL(new LeafURL("Albert"));
		subjects.add(p13);

		Predicate p14 = new Predicate("influences");
		p14.addLeafURL(new LeafURL("Arthur Schopenhauer"));
		p14.addLeafURL(new LeafURL("Baruch Spinoza"));
		p14.addLeafURL(new LeafURL("David Hume"));
		p14.addLeafURL(new LeafURL("Ernst Mach"));
		p14.addLeafURL(new LeafURL("Moritz Schlick"));
		subjects.add(p14);

		Predicate p15 = new Predicate("isMariedTo");
		p15.addLeafURL(new LeafURL("Mileva Maric"));
		subjects.add(p15);

		Predicate p16 = new Predicate("means");
		p16.addLeafURL(new LeafURL("Albert Eienstein"));
		p16.addLeafURL(new LeafURL("Albert Einstein"));
		p16.addLeafURL(new LeafURL("Albert LaFache Einstein"));
		p16.addLeafURL(new LeafURL("Einstein on socialism"));
		p16.addLeafURL(new LeafURL("Einstein"));
		subjects.add(p16);

		LinkedList<Predicate> objects = new LinkedList<Predicate>();
		Predicate p50 = new Predicate("bornIn");
		p50.addLeafURL(new LeafURL("Ulm"));
		objects.add(p50);

		Predicate p51 = new Predicate("bornOnDate");
		p51.addLeafURL(new LeafURL("1879-03-14"));
		objects.add(p51);

		Predicate p52 = new Predicate("diedIn");
		p52.addLeafURL(new LeafURL("Princeton, New Jersey"));
		objects.add(p52);

		Predicate p53 = new Predicate("diedOnDate");
		p53.addLeafURL(new LeafURL("1955-04-18"));
		objects.add(p53);

		Predicate p54 = new Predicate("graduatedFrom");
		p54.addLeafURL(new LeafURL("University of Z�rich"));
		objects.add(p54);

		Predicate p55 = new Predicate("hasAcademicAdvisor");
		p55.addLeafURL(new LeafURL("Alfred Kleiner"));
		objects.add(p55);

		Predicate p56 = new Predicate("type");
		p56.addLeafURL(new LeafURL("Academics of the Charles University"));
		p56.addLeafURL(new LeafURL("American agnostics"));
		p56.addLeafURL(new LeafURL("American humanists"));
		p56.addLeafURL(new LeafURL("American pacifists"));
		p56.addLeafURL(new LeafURL("person"));
		objects.add(p56);

		N3Entity out = new N3Entity("Albert Einstein", subjects, objects);
		return out;
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		entity = new N3Entity("top", null, null);
		
		pred1 = new Predicate("pred1");
		
		pred2 = new Predicate("pred2");
		
		leaf1 = new LeafURL("leaf1");
		
		leaf2 = new LeafURL("leaf2");
		
		pred12 = new Predicate("pred11");
		pred12.addLeafURL(leaf1);
		pred12.addLeafURL(leaf2);
		
		pred21 = new Predicate("pred21");
		pred21.addLeafURL(leaf2);
		pred21.addLeafURL(leaf1);
		
		pred1n = new Predicate("pred1n");
		pred1n2 = new Predicate("pred1n'");
		for (int i=0;i<5;i++){
			pred1n.addLeafURL(new LeafURL("leaf"+new Integer(i).toString()));
			pred1n2.addLeafURL(new LeafURL("leaf"+new Integer(4-i).toString()));
		}
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testConstructor(){
		// Standard Initialisierung
		assertNotNull(entity);
		assertNotNull(entity.getName());
		assertEquals(entity.getName(), "top");
		assertNull(entity.getSubjects());
		assertNull(entity.getObjects()); 
	}

	// testAddPredicate:
	
	public void testAdd1PredNoEnitities(){
		assertNull(entity.getObjects());
		assertNull(entity.getSubjects());
		
		// 1 Pr�dikat ohne Entities hinzuf�gen
		pred1.setChildren(null);
		entity.addPredicate(pred1, true);
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		
		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).equals(pred1));
	}
	
		
	public void testAdd1Pred1Entity(){
		// 1 Pr�dikat mit 1 Entity hinzuf�gen
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, true);
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).equals(pred1));
		assertNotNull(entity.getSubjects().get(0).getChildren());
		assertEquals(entity.getSubjects().get(0).getChildren().size(),1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).equals(leaf1));
	}
	
	
	public void testAdd1PredManyEntities(){
		// 1 Pr�dikat mit beliebig vielen Entities hinzuf�gen
		entity.addPredicate(pred1n, true);

		assertTrue(entity.getName().equals("top"));
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getSubjects().get(0), pred1n);
	}
	
	public void testAdd2SubPredsNoEntities(){
		// 2 gleichgerichtete Pr�dikate ohne Entities hinzuf�gen		
		entity.addPredicate(pred1, true); // bzw. false
		entity.addPredicate(pred1, true); // ~ false
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),2);
		assertEquals(entity.getSubjects().get(0), pred1);
		assertEquals(entity.getSubjects().get(1), pred1);
	}
	
	
	public void testAdd2ObjPredsSameEntities(){
		// 2 gleichgerichtete Pr�dikate mit gleichen Entities hinzuf�gen
		entity.addPredicate(pred1n, false);
		entity.addPredicate(pred1n2, false);
		
		assertNull(entity.getSubjects());
		assertNotNull(entity.getObjects());
		assertEquals(entity.getObjects().size(),2);
		assertEquals(entity.getObjects().get(0), pred1n);
		assertEquals(entity.getObjects().get(1), pred1n2);
	}
	
	public void testAdd2SubPredDiffEntities(){
		// 2 gleichgerichtete Pr�dikate mit verschiedenen Entities hinzuf�gen
		entity.addPredicate(pred12, true);
		entity.addPredicate(pred21, true);

		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),2);
		assertEquals(entity.getSubjects().get(0), pred12);
		assertEquals(entity.getSubjects().get(1), pred21);
	}
		
	public void testAdd2DiffPredNoEntities(){
		// 2 verschieden gerichtete Pr�dikate ohne Entities hinzuf�gen
		entity.addPredicate(pred1, true);
		entity.addPredicate(pred1, false);
		
		assertNotNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred1);
		assertEquals(entity.getSubjects().get(0), pred1);
	}
	
	public void testAdd2DiffPredSameEntities(){
		// 2 verschieden gerichtete Pr�dikate mit gleichen Entities hinzuf�gen
		entity.addPredicate(pred1n, true);
		entity.addPredicate(pred1n, false);
		
		assertNotNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred1n);
		assertEquals(entity.getSubjects().get(0), pred1n);	
	}

	public void testAdd2DiffPredDiffEntities(){
		// 2 verschieden gerichtete Pr�dikate mit verschiedenen Entities hinzuf�gen
		entity.addPredicate(pred12, false);
		entity.addPredicate(pred21, true);
		
		assertNotNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred12);
		assertEquals(entity.getSubjects().get(0), pred21);
	}


	public void testAdd1Subject() {
		// 1 Subjekt hinzuf�gen
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, true);
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getSubjects().get(0), pred1);
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertEquals(entity.getSubjects().get(0).getChildren().get(0), leaf1);
	}
	
	public void testAddNSubjects() {
		// n Subjekte hinzuf�gen		
		entity.addPredicate(pred1n, true);
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getSubjects().get(0), pred1n);
	}
	
	public void testNSubjectsWithDuplicates() {
		// n Subjekte mit Duplikaten hinzuf�gen
		pred1.addLeafURL(leaf1);
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, true);
		
		assertNull(entity.getObjects());
		assertNotNull(entity.getSubjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getSubjects().get(0), pred1);
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 2);
		assertEquals(entity.getSubjects().get(0).getChildren().get(0), leaf1);
	}
	
	// analogous to 'testSubjects'
	public void testAdd1Object() {
		// 1 Objekt hinzuf�gen
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, false);
		
		assertNull(entity.getSubjects());   
		assertNotNull(entity.getObjects());
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred1);
		assertEquals(entity.getObjects().get(0).getChildren().size(), 1);
		assertEquals(entity.getObjects().get(0).getChildren().get(0), leaf1);
	}
	
	public void testAdd2ObjectsWithDuplicates() {
		// 2 Duplikat-Objekte hinzuf�gen
		pred1.addLeafURL(leaf1);
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, false);
		
		assertNull(entity.getSubjects());   
		assertNotNull(entity.getObjects());
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred1);
		assertEquals(entity.getObjects().get(0).getChildren().size(), 2);
		assertEquals(entity.getObjects().get(0).getChildren().get(0), leaf1);
	}
	
	public void testAdd1ObjectAnd1Subject() {
		// 1 Subjekt und 1 Objekt hinzuf�gen
		pred1.addLeafURL(leaf1);
		pred2.addLeafURL(leaf2);
		entity.addPredicate(pred1, true);
		entity.addPredicate(pred2, false);
		
		assertNotNull(entity.getSubjects());   
		assertNotNull(entity.getObjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred2);
		assertEquals(entity.getSubjects().get(0), pred1);
	}
	
	public void testAdd1ObjectAnd1SubjectWithSameNames() {	
		// 1 Subjekt und 1 Objekt mit gleichem Namen hinzuf�gen
		pred1.addLeafURL(leaf1);
		entity.addPredicate(pred1, true);
		entity.addPredicate(pred1, false);
		
		assertNotNull(entity.getSubjects());   
		assertNotNull(entity.getObjects());
		assertEquals(entity.getSubjects().size(),1);
		assertEquals(entity.getObjects().size(),1);
		assertEquals(entity.getObjects().get(0), pred1);
		assertEquals(entity.getSubjects().get(0), pred1);
	}
	
	public void testObjectsAndSubjects() {	
		// m Subjekte und n Objekte hinzuf�gen
		entity = buildTestEinstein();
		
		assertNotNull(entity.getSubjects());
		assertNotNull(entity.getObjects());
		assertEquals(entity.getSubjects().size(),6);
		assertEquals(entity.getObjects().size(),7);
		
		assertTrue(entity.getSubjects().get(0).getName().equals("describes"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(),1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName().equals("http://en.wikipedia.org/wiki/Albert Einstein"));
		
		assertTrue(entity.getSubjects().get(5).getName().equals("means"));
		assertEquals(entity.getSubjects().get(5).getChildren().size(),5);
		assertTrue(entity.getSubjects().get(5).getChildren().get(0).getName().equals("Albert Eienstein"));
		assertTrue(entity.getSubjects().get(5).getChildren().get(1).getName().equals("Albert Einstein"));
		assertTrue(entity.getSubjects().get(5).getChildren().get(2).getName().equals("Albert LaFache Einstein"));
		assertTrue(entity.getSubjects().get(5).getChildren().get(3).getName().equals("Einstein on socialism"));
		assertTrue(entity.getSubjects().get(5).getChildren().get(4).getName().equals("Einstein"));
		
		assertTrue(entity.getObjects().get(0).getName().equals("bornIn"));
		assertEquals(entity.getObjects().get(0).getChildren().size(),1);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName().equals("Ulm"));
		
		assertTrue(entity.getObjects().get(6).getName().equals("type"));
		assertEquals(entity.getObjects().get(6).getChildren().size(),5);
		assertTrue(entity.getObjects().get(6).getChildren().get(0).getName().equals("Academics of the Charles University"));
		assertTrue(entity.getObjects().get(6).getChildren().get(1).getName().equals("American agnostics"));
		assertTrue(entity.getObjects().get(6).getChildren().get(2).getName().equals("American humanists"));
		assertTrue(entity.getObjects().get(6).getChildren().get(3).getName().equals("American pacifists"));
		assertTrue(entity.getObjects().get(6).getChildren().get(4).getName().equals("person"));
	}

	public void testToString() {
		assertNull(entity.toString());
	}

}
