package gui.n3.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.LeafURL;
import gui.n3.N3Entity;
import gui.n3.Predicate;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;

public class HTMLVisitor extends PrettyPrinter {
	
	public final static int SUBJECTPREDICATE = 1;
	public final static int PREDICATEOBJECT = 2;
	private final static String HTMLVISITORXSDSCHEMA = "HTMLSerialize.xsd";
		
	private int alignmentState;
	private String imagesPath;
	private String XSDUri;
	private boolean showNamespaces = true;
	private boolean showImages = true;
	
	//new: limitation of entities
	private final static int srcollLimit = 50; //2
	private N3Entity top=null;
	
	public static int getScrollLimit(){
		return srcollLimit;
	}
	
	public void setShowNamespaces(boolean showNamespaces){
		this.showNamespaces = showNamespaces;
	}
	
	public void setShowImages(boolean showImages){
		this.showImages = showImages;
	}
	
	public String getXSDUri() {
		return XSDUri;
	}

	public void setXSDUri(String uri) {
		XSDUri = uri;
	}

	public String getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}

	public HTMLVisitor() {
		URL url = getClass().getResource("../../../images/");
		String path = url.toString();
		setImagesPath(path);
		
		URL xsdURL = getClass().getResource("../../../"+HTMLVISITORXSDSCHEMA);
		setXSDUri(xsdURL.toString());
	}

	public int getAlignmentState() {
		return alignmentState;
	}

	public void setAlignmentState(int alignmentState) {
		this.alignmentState = alignmentState;
	}
	
	/**
	 * print a leaf node as html
	 */
	protected String visitLeaf(LeafURL node) {	
		return "<li><a href='" + node.getHTMLName() + "'>" + node.getHTMLName() + "</a></li>";
	}

	/**
	 * print a predicate with entity list (subjects or objects)
	 * 
	 * assertion: node != null
	 * 
	 */
	protected String visitPredicate(Predicate node) {
		if (node.getChildren() == null){
			return null;
		}
		
		String ret = "<tr>";

		if (getAlignmentState() == HTMLVisitor.PREDICATEOBJECT) {
			if (showImages==true){
				ret+= "<td width='5' style='background-image:url(" + getImagesPath() + "barV.gif)'/>";
			}
			else{
				ret+= "<td width='5'/>";
			}
			
			/*ret += "<td align='left'>"
				+ node.getName()
				+ "</td>";*/
			ret += "<td align='left'>"
				+ node.getHTMLName()
				+ "</td>";
		}
		ret += "<td>";
		ret += "<ul>";

		Iterator<LeafURL> it = node.getChildren().iterator();
		
		int entityCount = node.getChildren().size();
		int offset = node.getOffset();
		
		// Navigation for entities is required
		if (entityCount>srcollLimit){
			// check valid intervall [offset.. +limit]
			if (offset+srcollLimit>entityCount){
				offset = entityCount-srcollLimit;
				node.setOffset(offset);
			}
			// switch up button
			if (node.getOffset()!=0){
				ret += "<li><a href='^" + top.getHTMLName() + "##" + node.getHTMLName() + "##" + getAlignmentState() + "'><b>...</b></a></li>";
			}
			
			// it is implied that there exists a sequence up to min(offset+limit,entityCount)
			for (int j=offset;j<Math.min(offset+srcollLimit, entityCount);j++){
				LeafURL o = node.getChildren().get(j);
				ret += visitLeaf(o);
			}
			
			// switch down button
			if (node.getOffset()+srcollLimit<entityCount){ 
				ret += "<li><a href='#" + top.getHTMLName() + "##" + node.getHTMLName() + "##" + getAlignmentState() + "'><b>...</b></a></li>";
			}
		}
		// direct output of entities
		else{
			while (it.hasNext()) {
				LeafURL o = it.next();
				ret += visitLeaf(o);
			}
		}
		
		ret += "</ul>";
		ret += "</td>";
		if (getAlignmentState() == HTMLVisitor.SUBJECTPREDICATE) {
			ret += "<td align='right'>"
				+ node.getHTMLName()
				+ "</td>";
			if (showImages==true){
				ret+= "<td width='5' style='background-image:url(" + getImagesPath() + "barV.gif)'/>";
			}
			else{
				ret+= "<td width='5'/>";
			}
		}	
		
		ret += "</tr>";
		return ret;
	}

	
	private String visitPredicates(LinkedList<Predicate> predicates) {
		Iterator<Predicate> it;
		Predicate o;
		String ret = "<table border='0'>";

		if (predicates != null) {
			it = predicates.iterator();
			while (it.hasNext()) {
				o = it.next();
				ret += visitPredicate(o);
				if (getAlignmentState() == HTMLVisitor.PREDICATEOBJECT) {
					if (showImages==true){
						ret += "<tr><td width='5' style='background-image:url(" + getImagesPath() + "barV.gif)'/><td colspan='2' style='background-image:url(" + getImagesPath() + "barH.gif)'/></tr>";
					}
					else{
						ret += "<tr><td colspan='3'/></tr>";
					}
				}
				else{
					if (showImages==true){
						ret += "<tr><td colspan='2' style='background-image:url(" + getImagesPath() + "barH.gif)'/><td width='5' style='background-image:url(" + getImagesPath() + "barV.gif)'/></tr>";
					}
					else{
						ret += "<tr><td colspan='3'/></tr>";
					}
				}
			}
		}
		ret += "</table>";
		return ret;
	}

	/**
	 * print an entity with all its predicates and leaves as html
	 * for further operations the node is saved in top
	 * 
	 */
	protected String visitEntity(N3Entity node){
		if (node==null){
			return null;
		}
		
		this.top = node;
		
		String ret = "";
		String head = "<title>RDF3x Data Browser</title>";
		setAlignmentState(HTMLVisitor.SUBJECTPREDICATE);
		String leftTable = visitPredicates(node.getSubjects());
		setAlignmentState(HTMLVisitor.PREDICATEOBJECT);
		String rightTable = visitPredicates(node.getObjects());
				
		String entityTag = "";
		
		if ((node.getSubjects()==null) && (node.getObjects()!=null)){
			entityTag = "<td colspan='2' align='left'>" + "<b>"+ node.getHTMLName() + "</b>" + "</td>";
		}
		
		if ((node.getSubjects()!=null) && (node.getObjects()==null)){
			entityTag = "<td colspan='2' align='right'>" + "<b>"+ node.getHTMLName() + "</b>" + "</td>";
		}
		
		else{
			entityTag = "<td colspan='2' align='center'>" + "<b>"+ node.getHTMLName() + "</b>" + "</td>";
		}
		
		String body =
				"<table border='0'>"
				  + "<tr>"
				  + entityTag
				  + "</tr>"
			      + "<tr>"
			        + "<td valign='top'>" + leftTable + "</td>"
				    + "<td valign='top'>" + rightTable + "</td>"
				  + "</tr>"
				+ "</table>";

		String htmlschema = "<html xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"" + getXSDUri() + "\">";
		if (showNamespaces==true){
			ret = htmlschema + "<head>" + head + "</head><body>" + body
			+ "</body></html>";
		}
		else{
			ret = "<html>" + "<head>" + head + "</head><body>" + body
			+ "</body></html>";
		}	

		return ret;
	}
}