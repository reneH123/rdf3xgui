package gui.n3.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.N3Entity;
import gui.n3.N3EntityBuilder;
import junit.framework.TestCase;

public class TestTextVisitor extends TestCase {
	private TextVisitor visitor;
	private N3EntityBuilder builder;
	private N3Entity entity;

	private String[][] a = { { "s1", "a" }, { "s2", "a" }, { "s3", "a" } };
	private String[][] b = { { "b", "o1" }, { "b", "o2" }, { "b", "o3" } };

	public TestTextVisitor(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		visitor = new TextVisitor();
		builder = new N3EntityBuilder("top");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testToString0() {
		builder.addEntities(null, true);
		builder.addEntities(null, false);
		entity = builder.getEntity();
		assertEquals(entity.accept(visitor), "(top,{},{})");

	}

	public void testToString1() {
		builder.addEntities(a, true);
		builder.addEntities(b, false);
		entity = builder.getEntity();
		assertEquals(entity.accept(visitor),
				"(top,{(a,{s1,s2,s3})},{(b,{o1,o2,o3})})");
	}

	public void testToString2() {
		builder.addEntities(a, true);
		builder.addEntities(b, false);
		builder.addEntities(a, false);
		entity = builder.getEntity();
		assertEquals(entity.accept(visitor),
				"(top,{(a,{s1,s2,s3})},{(b,{o1,o2,o3}),(s1,{a}),(s2,{a}),(s3,{a})})");
	}

	public void testToString3() {
		builder.addEntities(a, true);
		entity = builder.getEntity();
		assertEquals(entity.accept(visitor), "(top,{(a,{s1,s2,s3})},{})");
	}

	public void testToString4() {
		builder.addEntities(b, false);
		entity = builder.getEntity();
		assertEquals(entity.accept(visitor), "(top,{},{(b,{o1,o2,o3})})");
	}
}