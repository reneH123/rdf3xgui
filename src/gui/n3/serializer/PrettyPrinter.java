package gui.n3.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.LeafURL;
import gui.n3.N3Entity;
import gui.n3.Node;
import gui.n3.Predicate;

public abstract class PrettyPrinter {
	protected abstract String visitEntity(N3Entity node);
	protected abstract String visitPredicate(Predicate node);
	protected abstract String visitLeaf(LeafURL node);
	
	public String visit(Node node) {
		if (node instanceof N3Entity){
			return visitEntity((N3Entity)node);
		}
		
		if (node instanceof Predicate){
			return visitPredicate((Predicate)node);
		}
		
		if (node instanceof LeafURL){
			return visitLeaf((LeafURL)node);
		}
		
		return null;
	}
}