package gui.n3.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.LeafURL;
import gui.n3.N3Entity;
import gui.n3.Predicate;

import java.util.Iterator;
import java.util.LinkedList;

public class TextVisitor extends PrettyPrinter {

	private String visitPredicates(LinkedList<Predicate> predicates) {
		Iterator<Predicate> it;
		Predicate o;
		String ret = "{";

		if (predicates != null) {
			it = predicates.iterator();
			if (it.hasNext()) {
				ret += visit(it.next());
			}
			while (it.hasNext()) {
				o = it.next();
				ret += "," + visit(o);
			}
		}
		return ret + "}";
	}

	protected String visitPredicate(Predicate node) {
		String ret = "(";
		ret += node.getName();

		if (node.getChildren() == null)
			return ret;
		Iterator<LeafURL> it = node.getChildren().iterator();
		ret += ",{";
		if (it.hasNext()) {
			ret += visit(it.next());
		}
		while (it.hasNext()) {
			LeafURL o = it.next();
			ret += "," + visit(o);
		}
		return ret + "})";
	}

	protected String visitEntity(N3Entity node) {
		if (node == null) {
			return null;
		}

		String ret = "(" + node.getName();
		ret += ",";
		ret += visitPredicates(node.getSubjects());
		ret += ",";
		ret += visitPredicates(node.getObjects());
		ret += ")";

		return ret;
	}

	protected String visitLeaf(LeafURL node) {
		return node.getName();
	}
}
