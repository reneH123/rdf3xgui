package gui.n3.serializer;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.N3Entity;
import gui.n3.N3EntityBuilder;
import junit.framework.TestCase;

public class TestHTMLVisitor extends TestCase {
	private N3EntityBuilder builder;
	private N3Entity entity;
	private HTMLVisitor visitor;

	public TestHTMLVisitor(String name) {
		super(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
		// visitor = new HTMLVisitor("");
		visitor = new HTMLVisitor();
		visitor.setShowNamespaces(false);
		visitor.setShowImages(false);
		builder = new N3EntityBuilder("top");
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	// ("top",{},{})
	public void testToString0() {
		builder.addEntities(null, true);
		builder.addEntities(null, false);
		entity = builder.getEntity();
		visitor.setShowNamespaces(false);
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
		visitor.setShowNamespaces(true);

		assertEquals(
				entity.accept(visitor),
				"<html xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\""
						+ visitor.getXSDUri()
						+ "\"><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{})},{})
	public void testToString1() {
		String[][] a = { { "", "a" } };
		builder.addEntities(a, true);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='right'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href=''></a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
	}

	// ("top",{},{(a,{})})
	public void testToString2() {
		String[][] a = { { "a", "" } };
		builder.addEntities(a, false);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>a</td><td><ul><li><a href=''></a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{s1})},{})
	public void testToString3() {
		String[][] a = { { "s1", "a" } };
		builder.addEntities(a, true);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='right'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href='s1'>s1</a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
	}

	// ("top",{},{(a,{o1})})
	public void testToString4() {
		String[][] a = { { "a", "o1" } };
		builder.addEntities(a, false);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>a</td><td><ul><li><a href='o1'>o1</a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{s1,s2})},{})
	public void testToString5() {
		String[][] a = { { "s1", "a" }, { "s2", "a" } };
		builder.addEntities(a, true);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='right'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href='s1'>s1</a></li><li><a href='s2'>s2</a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
	}

	// ("top",{},{(a,{o1,o2})})
	public void testToString6() {
		String[][] a = { { "a", "o1" }, { "a", "o2" } };
		builder.addEntities(a, false);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>a</td><td><ul><li><a href='o1'>o1</a></li><li><a href='o2'>o2</a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{s1})},{b,{o1}})
	public void testToString7() {
		String[][] a = { { "s1", "a" } };
		String[][] b = { { "b", "o1" } };
		builder.addEntities(a, true);
		builder.addEntities(b, false);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href='s1'>s1</a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>b</td><td><ul><li><a href='o1'>o1</a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{s1,s2}),(a,{s3})},{})
	public void testToString8() {
		String[][] a = { { "s1", "a" }, { "s2", "a" } };
		builder.addEntities(a, true);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='right'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href='s1'>s1</a></li><li><a href='s2'>s2</a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'></table></td></tr></table></body></html>");
	}

	// ("top",{},{(b,{o1,o2}),(b,{o3})})
	public void testToString9() {
		String[][] b1 = { { "b", "o1" }, { "b", "o2" } };
		String[][] b2 = { { "b", "o3" } };
		builder.addEntities(b1, false);
		builder.addEntities(b2, false);
		entity = builder.getEntity();
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>b</td><td><ul><li><a href='o1'>o1</a></li><li><a href='o2'>o2</a></li><li><a href='o3'>o3</a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}

	// ("top",{(a,{s1,s2}),(a,{s3})},{(b,{o3}),(b,{o1,o2})})
	public void testToString10() {
		String[][] a1 = { { "s1", "a" }, { "s2", "a" } };
		String[][] a2 = { { "s3", "a" } };
		String[][] b1 = { { "b", "o3" } };
		String[][] b2 = { { "b", "o1" }, { "b", "o2" } };
		builder.addEntities(a1, true);
		builder.addEntities(a2, true);
		builder.addEntities(b1, false);
		builder.addEntities(b2, false);
		entity = builder.getEntity();
		
		assertEquals(
				entity.accept(visitor),
				"<html><head><title>RDF3x Data Browser</title></head><body><table border='0'><tr><td colspan='2' align='center'><b>top</b></td></tr><tr><td valign='top'><table border='0'><tr><td><ul><li><a href='s1'>s1</a></li><li><a href='s2'>s2</a></li><li><a href='s3'>s3</a></li></ul></td><td align='right'>a</td><td width='5'/></tr><tr><td colspan='3'/></tr></table></td><td valign='top'><table border='0'><tr><td width='5'/><td align='left'>b</td><td><ul><li><a href='o3'>o3</a></li><li><a href='o1'>o1</a></li><li><a href='o2'>o2</a></li></ul></td></tr><tr><td colspan='3'/></tr></table></td></tr></table></body></html>");
	}
}