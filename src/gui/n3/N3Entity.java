package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.evaluation.InternalURL;
import gui.n3.serializer.HTMLVisitor;

import java.util.Iterator;
import java.util.LinkedList;

public class N3Entity extends Node {
	private LinkedList<Predicate> objects;
	private LinkedList<Predicate> subjects;

	public LinkedList<Predicate> getObjects() {
		return objects;
	}

	public void setObjects(LinkedList<Predicate> objects) {
		this.objects = objects;
	}

	public LinkedList<Predicate> getSubjects() {
		return subjects;
	}

	public void setSubjects(LinkedList<Predicate> subjects) {
		this.subjects = subjects;
	}

	public void addPredicate(Predicate predicate, boolean isSubject) {
		// add subject
		if (isSubject) {
			if (subjects == null) {
				subjects = new LinkedList<Predicate>();
			}
			subjects.add(predicate);
		}
		// add object
		else {
			if (objects == null) {
				objects = new LinkedList<Predicate>();
			}
			objects.add(predicate);
		}
	}

	@Override
	public String toString() {
		// use 'PrettyPrinter' implementation
		return null;
	}

	public N3Entity(String name) {
		objects = null;
		subjects = null;
		setName(name);
	}

	public N3Entity(String name, LinkedList<Predicate> subjects,
			LinkedList<Predicate> objects) {
		this.subjects = subjects;
		this.objects = objects;
		setName(name);
	}
	
	public void scroll(InternalURL iURL){		
		// navigate within subjects or objects
		Iterator<Predicate> it = (iURL.getAlignment()?subjects:objects).iterator();
		Predicate p;
		
		while (it.hasNext()){
			p = it.next();
			if (p.getName().equals(iURL.getPredicateName())){
				int page = HTMLVisitor.getScrollLimit();
				page*=(iURL.isUp()?-1:+1);
				int newOffset = Math.max(p.getOffset()+page,0);
				p.setOffset(newOffset);
			}
		}
	}	
}
