package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import junit.framework.TestCase;

public class TestEntityBuilder extends TestCase {
	private N3EntityBuilder builder;
	private N3Entity entity;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		builder = new N3EntityBuilder("top");
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	// null
	public void testAddEmptyEntities1() {
		builder.addEntities(null, true);
		builder.addEntities(null, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertTrue(entity.getName().equals("top"));
		assertNull(entity.getSubjects());
		assertNull(entity.getObjects());
	}

	// {{"",""}}
	public void testAddEmptyEntities2() {
		String[][] t = { { "", "" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).getName().equals(""));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals(""));
	}

	// {{"a","b"}}
	public void testAddSimpleEntities1() {
		String[][] t = { { "a", "b" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","b"}}
	public void testAddSimpleEntities2() {
		String[][] t = { { "a", "b" } };
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getObjects());
		assertNull(entity.getSubjects());

		assertEquals(entity.getObjects().size(), 1);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("b"));
	}

	// {{"a","b"},{"a","c"}}
	public void testAddSimpleEntities3() {
		String[][] t = { { "b", "a" }, { "c", "a" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).getName().equals("a"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("b"));
		assertTrue(entity.getSubjects().get(0).getChildren().get(1).getName()
				.equals("c"));
	}

	// {{"a","b"},{"a","c"}}
	public void testAddSimpleEntities4() {
		String[][] t = { { "a", "b" }, { "a", "c" } };
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNull(entity.getSubjects());
		assertNotNull(entity.getObjects());

		assertEquals(entity.getObjects().size(), 1);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("b"));
		assertTrue(entity.getObjects().get(0).getChildren().get(1).getName()
				.equals("c"));
	}

	// {{"a","b"},{"a","b"}}
	public void testAddDuplicates1() {
		String[][] t = { { "a", "b" }, { "a", "b" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 1);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","b"},{"a","c"},{"a","b"}}
	public void testAddDuplicates2() {
		String[][] t = { { "a", "b" }, { "a", "c" }, { "a", "b" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 2);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertTrue(entity.getSubjects().get(1).getName().equals("c"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","b"},{"a","c"},{"a","b"}}
	public void testAddDuplicates3() {
		String[][] t = { { "a", "b" }, { "a", "c" }, { "a", "b" } };
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getObjects());
		assertNull(entity.getSubjects());

		assertEquals(entity.getObjects().size(), 1);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("b"));
		assertTrue(entity.getObjects().get(0).getChildren().get(1).getName()
				.equals("c"));
	}

	// {{"a","c"},{"a","b"},{"a","b"}}
	public void testAddDuplicates4() {
		String[][] t = { { "a", "c" }, { "a", "b" }, { "a", "b" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 2);
		assertTrue(entity.getSubjects().get(0).getName().equals("c"));
		assertTrue(entity.getSubjects().get(1).getName().equals("b"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","c"},{"a","b"},{"a","b"},{"a","d"}}
	public void testAddDuplicates5() {
		String[][] t = { { "a", "c" }, { "a", "b" }, { "a", "b" }, { "a", "d" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("c"));
		assertTrue(entity.getSubjects().get(1).getName().equals("b"));
		assertTrue(entity.getSubjects().get(2).getName().equals("d"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","c"},{"a","b"},{"a","b"},{"a","d"}}
	public void testAddDuplicates6() {
		String[][] t = { { "a", "c" }, { "a", "b" }, { "a", "b" }, { "a", "d" } };
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getObjects());
		assertNull(entity.getSubjects());

		assertEquals(entity.getObjects().size(), 1);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 3);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("c"));
		assertTrue(entity.getObjects().get(0).getChildren().get(1).getName()
				.equals("b"));
		assertTrue(entity.getObjects().get(0).getChildren().get(2).getName()
				.equals("d"));
	}

	// {{"a","c"},{"a","b"},{"a","b"},{"b","a"}}
	public void testAddDuplicates7() {
		String[][] t = { { "a", "c" }, { "a", "b" }, { "a", "b" }, { "b", "a" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("c"));
		assertTrue(entity.getSubjects().get(1).getName().equals("b"));
		assertTrue(entity.getSubjects().get(2).getName().equals("a"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("b"));
	}

	// {{"a","c"},{"a","b"},{"b","a"},{"b","b"},{"a","b"}}
	public void testAddDuplicates8() {
		String[][] t = { { "a", "c" }, { "a", "b" }, { "b", "a" },
				{ "b", "b" }, { "a", "b" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("c"));
		assertTrue(entity.getSubjects().get(1).getName().equals("b"));
		assertTrue(entity.getSubjects().get(2).getName().equals("a"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 2);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
		assertTrue(entity.getSubjects().get(1).getChildren().get(1).getName()
				.equals("b"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("b"));
	}

	// {{"a","b"},{"a","c"},{"b","a"}}
	public void testAddEntities1() {
		String[][] t = { { "a", "b" }, { "a", "c" }, { "b", "a" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertTrue(entity.getName().equals("top"));
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertTrue(entity.getSubjects().get(1).getName().equals("c"));
		assertTrue(entity.getSubjects().get(2).getName().equals("a"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("b"));
	}

	// {{"a","b"},{"a","c"},{"b","a"}}
	public void testAddEntities2() {
		String[][] t = { { "a", "b" }, { "a", "c" }, { "b", "a" } };
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertTrue(entity.getName().equals("top"));
		assertNotNull(entity.getObjects());
		assertNull(entity.getSubjects());

		assertEquals(entity.getObjects().size(), 2);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertTrue(entity.getObjects().get(1).getName().equals("b"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("b"));
		assertTrue(entity.getObjects().get(0).getChildren().get(1).getName()
				.equals("c"));
		assertEquals(entity.getObjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getObjects().get(1).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","b"},{"b","a"},{"a","c"}}
	public void testAddEntities3() {
		String[][] t = { { "a", "b" }, { "b", "a" }, { "a", "c" } };
		builder.addEntities(t, true);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getSubjects());
		assertNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertTrue(entity.getSubjects().get(1).getName().equals("a"));
		assertTrue(entity.getSubjects().get(2).getName().equals("c"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("b"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("a"));
	}

	// {{"a","b"},{"b","a"},{"b","a"},{"b","b"},{"a","c"},{"a","b"}} from both
	// sides with duplicates
	public void testAddEntitiesBothSides() {
		String[][] t = { { "a", "b" }, { "b", "a" }, { "b", "a" },
				{ "b", "b" }, { "a", "c" }, { "a", "b" } };
		builder.addEntities(t, true);
		builder.addEntities(t, false);

		entity = builder.getEntity();
		assertNotNull(entity);
		assertTrue(entity.getName().equals("top"));
		assertNotNull(entity.getSubjects());
		assertNotNull(entity.getObjects());

		assertEquals(entity.getSubjects().size(), 3);
		assertTrue(entity.getSubjects().get(0).getName().equals("b"));
		assertTrue(entity.getSubjects().get(1).getName().equals("a"));
		assertTrue(entity.getSubjects().get(2).getName().equals("c"));
		assertEquals(entity.getSubjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getSubjects().get(0).getChildren().get(0).getName()
				.equals("a"));
		assertTrue(entity.getSubjects().get(0).getChildren().get(1).getName()
				.equals("b"));
		assertEquals(entity.getSubjects().get(1).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(1).getChildren().get(0).getName()
				.equals("b"));
		assertEquals(entity.getSubjects().get(2).getChildren().size(), 1);
		assertTrue(entity.getSubjects().get(2).getChildren().get(0).getName()
				.equals("a"));

		assertEquals(entity.getObjects().size(), 2);
		assertTrue(entity.getObjects().get(0).getName().equals("a"));
		assertTrue(entity.getObjects().get(1).getName().equals("b"));
		assertEquals(entity.getObjects().get(0).getChildren().size(), 2);
		assertTrue(entity.getObjects().get(0).getChildren().get(0).getName()
				.equals("b"));
		assertTrue(entity.getObjects().get(0).getChildren().get(1).getName()
				.equals("c"));
		assertEquals(entity.getObjects().get(1).getChildren().size(), 2);
		assertTrue(entity.getObjects().get(1).getChildren().get(0).getName()
				.equals("a"));
		assertTrue(entity.getObjects().get(1).getChildren().get(1).getName()
				.equals("b"));
	}
}