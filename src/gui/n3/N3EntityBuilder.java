package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.Iterator;
import java.util.LinkedList;

public class N3EntityBuilder {
	private N3Entity entity;

	/**
	 * Looks up for a specified predicate name and a specified leaf name if there
	 * is already an entry in current entity. If not, insert it at appropriate position.
	 * 
	 * precondition: entity has no duplicates in predicates. for each predicate
	 * duplicate leafURLs are also not allowed
	 * 
	 * @param predicate
	 * @param leaf
	 * @param isSubject
	 */

	private void lookup(String predicate, String leaf,
			boolean isSubject) {
		LinkedList<Predicate> children;

		if (isSubject) {
			children = entity.getSubjects();
		} else {
			children = entity.getObjects();
		}

		// case 1) predicate list is empty
		if (children == null) {
			LinkedList<Predicate> newPredicates = new LinkedList<Predicate>();
			LinkedList<LeafURL> newLeafs = new LinkedList<LeafURL>();
			Predicate newPredicate = new Predicate(predicate, entity,
					newLeafs);
			newLeafs.add(new LeafURL(leaf, newPredicate));
			newPredicates.add(newPredicate);

			if (isSubject) {
				entity.setSubjects(newPredicates);
			} else {
				entity.setObjects(newPredicates);
			}
			return;
		}

		// case 2) predicate list is not empty
		Iterator<Predicate> it = children.iterator();

		while (it.hasNext()) {
			Predicate p = it.next();
			// case 2.1) predicate name already in use
			if (p.getName().equals(predicate)) {
				LinkedList<LeafURL> leafs = p.getChildren();
				// case 2.1.1) leaves are empty, so add and cancel
				if (leafs == null) {
					LeafURL l = new LeafURL(leaf, p);
					p.addLeafURL(l);
					return;
				}
				// case 2.1.2) leaves are not empty
				Iterator<LeafURL> it2 = leafs.iterator();
				while (it2.hasNext()) {
					LeafURL l = it2.next();
					// case 2.1.2.1) predicate name matches 'leafName'
					// exists, so cancel
					if (l.getName().equals(leaf) == true) {
						return;
					}
				}
				// case 2.1.2.2) predicate name exists, but w/o 'leafName'
				// => add and cancel
				LeafURL l = new LeafURL(leaf, p);
				p.addLeafURL(l);
				return;
			}
		}
		// case 2.2.) predicate name does not exist
		//   create new predicate with leaf entry (prediateName, leafName), quit
		LinkedList<LeafURL> newLeafs = new LinkedList<LeafURL>();
		Predicate newPredicate = new Predicate(predicate, entity, newLeafs);
		newLeafs.add(new LeafURL(leaf, newPredicate));
		entity.addPredicate(newPredicate, isSubject);
	}

	/**
	 * reads tupels from a result set table and create successive subject-predicate/predicate-object pairs in the current entity
	 * 
	 * precondition: table is a (m,2)-matrix with type (subject, predicate)
	 * 
	 * @param table
	 */
	public void addEntities(String[][] table, boolean isSubject) {
		if (table == null) {
			return;
		}
		String subjectName;
		String objectName;
		String predicateName;

		if (isSubject){
			for (int row = 0; row < table.length; row++) {
				subjectName = table[row][0];
				predicateName = table[row][1];
				lookup(predicateName, subjectName, true);
			}
		}
		else{
			for (int row = 0; row < table.length; row++) {
				predicateName = table[row][0];
				objectName = table[row][1];
				lookup(predicateName, objectName, false);
			}		
		}
	}

	public N3Entity getEntity() {
		return entity;
	}

	public N3EntityBuilder(String entityName) {
		entity = new N3Entity(entityName);
	}
}
