package gui.n3;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import org.htmlparser.util.Translate;

import gui.n3.serializer.PrettyPrinter;

/**
 * Abstract Node class for rdf result tree is a component
 * 
 * @author rene
 *
 */
public abstract class Node {
	private String name;

	public String getName() {
		return name;
	}
	
	public String getHTMLName(){
		return Translate.encode(name);
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String accept(PrettyPrinter visitor){
		return visitor.visit(this);
	}
}
