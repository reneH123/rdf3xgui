package gui.evaluation;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.N3Entity;
import gui.n3.N3EntityBuilder;
import gui.n3.serializer.HTMLVisitor;
import gui.n3.serializer.PrettyPrinter;
import gui.start.MainWindow;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;

import db.table.AbstractJDBCConnector;

public class EvaluationWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	// Window settings
	private EvaluationWindowListener listener;
	private MainWindow parent;
	private EvaluationWindowMenu ewm;
	private JFileChooser fileChooser;

	// Document site
	private JEditorPane jt;
	private PrettyPrinter visitor;
	private N3Entity entity;
	private String selectedTopic;
	private String lastSelectedTopic;
	private File tempFile;

	// DB Connector
	private AbstractJDBCConnector connectorDB;

	// return to last HTML page
	public void back() {
		setSelectedTopic(lastSelectedTopic);
	}

	// return back to main window
	public void exit() {
		parent.setVisible(true);
		this.setVisible(false);
	}

	public void exportHTML() {
		fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("HTML",
				"html");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showSaveDialog(this);
		String selectedFileName = "";

		if (result == JFileChooser.APPROVE_OPTION) {
			selectedFileName = fileChooser.getSelectedFile().toString();

			FileFilter selected = fileChooser.getFileFilter();

			if (selected.equals(filter)) {
				if (selectedFileName.contains(".") == false) {
					selectedFileName += ".html";
				}
			}

			try {
				FileWriter fw = new FileWriter(selectedFileName);
				fw.write(entity.accept(visitor));
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public File saveTemporaryFilePosition() {
		JOptionPane.showMessageDialog(this, "Could not save into temporary file. Choose another one!");
		
		fileChooser = new JFileChooser();
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int choice = fileChooser.showOpenDialog(this);
		
		if (choice == JFileChooser.APPROVE_OPTION) {
			File dir = fileChooser.getSelectedFile();
			
			if (dir==null){
				JOptionPane.showMessageDialog(this, "Error during temporary file save!", "fatal failure!", JOptionPane.ERROR_MESSAGE);
			}
			return dir;
		}
		return null;
	}

	public void entityClicked(String target) {
		setSelectedTopic(target);
	}

	private void buildEditPanel() {
		// sites with hyperlinks
		jt = new JEditorPane();
		jt.setEditable(false);
		jt.setContentType("text/html");

		visitor = new HTMLVisitor();
		jt.addHyperlinkListener(listener);

		JScrollPane pane = new JScrollPane(jt);
		pane.setBorder(BorderFactory.createLoweredBevelBorder());
		pane.scrollRectToVisible(new Rectangle(0, 0, pane.getWidth(), pane
				.getHeight()));

		add(pane);
	}

	private void setLookAndFeel(String className) {
		try {
			UIManager.setLookAndFeel(className);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EvaluationWindow(MainWindow parent) {
		super("rdf3x client - evaluation window");

		this.parent = parent;
		setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				getClass().getResource("../../images/icon.gif")));
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setMinimumSize(new Dimension(400, 300));
		BorderLayout thisLayout = new BorderLayout();
		getContentPane().setLayout(thisLayout);

		selectedTopic = "";
		lastSelectedTopic = "";

		listener = new EvaluationWindowListener(this);
		setListener();

		try {
			tempFile = File.createTempFile("rdf3x$", "tmp");
			tempFile.deleteOnExit();
		} catch (Exception e) {
			tempFile = saveTemporaryFilePosition();
		}
	}
	
	/**
	 * save html document in a temporary files
	 *   this is mandatory due to HTMLKit-Specification
	 * JTextPane.read interprets only non-textual elements (links, images, ...) from external files
	 *  (see specification)
	 */
	private void writeTemp() {
		try {
			// clear cache data / temporary files
			if (selectedTopic.equals(lastSelectedTopic)){
				tempFile.delete();
			}
			
			String tempFileName = tempFile.toString();
			FileWriter fw = new FileWriter(tempFileName);
			fw.write(entity.accept(visitor));
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * rebuild html representing structure and actualize view
	 */
	private void topicChanged() {
		if (connectorDB == null) {
			return;
		}
		
		// 1. build used entity from db in accordance to 'selectedTopic'
		InternalURL iURL = new InternalURL(selectedTopic);
		InternalURL lastURL = new InternalURL(lastSelectedTopic);
				
		// (a) navigation on existing entity
		if ((entity!=null)&&(iURL.isInternalUrl())&&(iURL.getEntityName().equals(lastURL.getEntityName()))){
			entity.scroll(iURL);
		}
		else{
			N3EntityBuilder builder = new N3EntityBuilder(iURL.getEntityName());

			String query1 = connectorDB.getQuerySubjectsPredicate(iURL.getEntityName());
			String query2 = connectorDB.getQueryPredicateObjects(iURL.getEntityName());
			
			String[][] rows1 = null;
			String[][] rows2 = null;
			if (connectorDB.query(query1) == true) {
				rows1 = connectorDB.getRows();
			}
			if (connectorDB.query(query2) == true) {
				rows2 = connectorDB.getRows();
			}
			builder.addEntities(rows1, true);
			builder.addEntities(rows2, false);

			entity = builder.getEntity();
		}
		
		// 2. refresh widgets
		try {
			writeTemp();

			EditorKit htmlKit = jt.getEditorKitForContentType("text/html");
			Document doc = htmlKit.createDefaultDocument();
			URL url = tempFile.toURI().toURL();

			jt.read(url.openStream(), doc);
		} catch (IOException e) {
			System.err
					.println("Exception while navigating! Check if tempFile has been written correctly!");
		}
	}
	
	/**
	 * change view to new 'selectedTopic'
	 * 
	 * @param selectedTopic
	 */
	public void setSelectedTopic(String selectedTopic) {
		this.lastSelectedTopic = this.selectedTopic;
		this.selectedTopic = selectedTopic;
		topicChanged();
	}

	private void setListener() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		// menu bar
		ewm = new EvaluationWindowMenu(this.listener);
		setJMenuBar(ewm);
		// build UI
		buildEditPanel();
	}

	public void setConnectorDB(AbstractJDBCConnector connectorDB) {
		this.connectorDB = connectorDB;
	}
}
