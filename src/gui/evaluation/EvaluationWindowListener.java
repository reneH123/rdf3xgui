package gui.evaluation;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class EvaluationWindowListener implements ActionListener,
		HyperlinkListener {
	private EvaluationWindow evalwnd;

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Back")) {
			evalwnd.back();
		}

		if (e.getActionCommand().equals("Export HTML...")) {
			evalwnd.exportHTML();
		}

		if (e.getActionCommand().equals("Exit")) {
			evalwnd.exit();
		}
	}

	public void hyperlinkUpdate(final HyperlinkEvent e) {
		if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					evalwnd.entityClicked(e.getDescription());
				}
			});
		}
	}

	public EvaluationWindowListener(EvaluationWindow evalwnd) {
		this.evalwnd = evalwnd;
	}
}