package gui.evaluation;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import junit.framework.TestCase;

public class TestInternalURL extends TestCase {
	private InternalURL iURL;
	
	// [^|#] EntityName ## Predicate' ## [SUBJECT|OBJECT] = (up, entityName, predicateName, alignment)
	
	private String state(InternalURL iURL){
		String state = "";
		
		state += (iURL.isUp()?"^":"#");		
		state += "," + iURL.getEntityName();		
		state += "," + iURL.getPredicateName();		
		state += "," + (iURL.getAlignment()?"S":"O");		
		
		return state;
	}
	
	public void testInternalURLPositives(){
		// valid up
		iURL = new InternalURL("^e##p123##1");
		System.out.println(state(iURL));
		assertEquals(state(iURL),"^,e,p123,S");
		assertTrue(iURL.isInternalUrl());
		
		iURL = new InternalURL("^e1##p123##1");
		iURL = new InternalURL("^ent1##p##1");
		iURL = new InternalURL("^ent1##p1##1");
		iURL = new InternalURL("^ent1##p123##1");
		iURL = new InternalURL("^ent1##p123##2");
		
		// valid down
		iURL = new InternalURL("#e##p123##1");
		iURL = new InternalURL("#e1##p123##1");
		iURL = new InternalURL("#ent1##p##1");
		iURL = new InternalURL("#ent1##p1##1");
		iURL = new InternalURL("#ent1##p123##1");
		iURL = new InternalURL("#ent1##p123##2");
	}
	
	public void testInternalURLNegatives(){
		// invalid up
		iURL = new InternalURL("");
		//iURL = new InternalURL("^"); // ab hier BUGs!
		//iURL = new InternalURL("^##");
		iURL = new InternalURL("^#");
		iURL = new InternalURL("^###");
		iURL = new InternalURL("^####");
		iURL = new InternalURL("####");
		
		iURL = new InternalURL("^e####");
		iURL = new InternalURL("^##p##1");
		iURL = new InternalURL("^e##p1##");
		
		iURL = new InternalURL("^e##p123##3");
		
		// invalid down
		iURL = new InternalURL("");
		iURL = new InternalURL("^");
		iURL = new InternalURL("^##");
		iURL = new InternalURL("^#");
		iURL = new InternalURL("^###");
		iURL = new InternalURL("^####");
		iURL = new InternalURL("####");
		
		iURL = new InternalURL("^e####");
		iURL = new InternalURL("^##p##1");
		iURL = new InternalURL("^e##p1##");
		
		iURL = new InternalURL("^e##p123##3");
		
		// invalid navigation
		iURL = new InternalURL("?ent1");
		iURL = new InternalURL("?ent1##p1");
		iURL = new InternalURL("?ent1##p123");
		iURL = new InternalURL("?ent1##p123##s7");
		iURL = new InternalURL("?ent1##p123##1");
		iURL = new InternalURL("?ent1##p123##2");
	}
	
	public void testNormalURL(){
		iURL = new InternalURL("ent1");
		
		iURL = new InternalURL("<ent1>");
		
		iURL = new InternalURL("\"ent1\"");		
		
		iURL = new InternalURL("\"ent1\" \"0 \"1");
	}
}
