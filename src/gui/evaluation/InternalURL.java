package gui.evaluation;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.n3.serializer.HTMLVisitor;

/**
 * represents an internal model for urls used e.g. for navigation within a
 * certain html document
 * 
 * [^|#] EntityName ## Predicate' ## [SUBJECT|OBJECT] = (up, entityName,
 * predicateName, alignment)
 * 
 * @author rene
 * 
 */
public class InternalURL { 
	private String entityName;
	private String predicateName;
	private boolean up;
	private boolean alignment;
	private boolean isInternalUrl;

	public String getEntityName() {
		return entityName;
	}

	public String getPredicateName() {
		return predicateName;
	}

	public boolean isUp() {
		return up;
	}

	public boolean getAlignment() {
		return alignment;
	}
	
	public boolean isInternalUrl(){
		return isInternalUrl;
	}

	public InternalURL(String internalUrl) {
		if (internalUrl==null){
			return;
		}
		if (internalUrl.equals("")){
			this.entityName = "";
			this.isInternalUrl = false;
			return;
		}
		
		char c = internalUrl.charAt(0);

		if ((c == '^') || (c == '#')) {
			int Idx = internalUrl.indexOf("##");
			int lastIdx = internalUrl.lastIndexOf("##");
			String alignmentString;

			this.entityName = internalUrl.substring(1, Idx);
			this.predicateName = internalUrl.substring(Idx + 2, lastIdx);
			alignmentString = internalUrl.substring(lastIdx + 2, internalUrl
					.length());
			this.up = (c == '^');
			this.alignment = (new Integer(alignmentString).intValue())==HTMLVisitor.SUBJECTPREDICATE;
			this.isInternalUrl = true;
		} else {
			this.entityName = internalUrl;
			this.isInternalUrl = false;
		}
	}
}