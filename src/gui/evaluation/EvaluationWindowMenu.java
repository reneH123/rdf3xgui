package gui.evaluation;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class EvaluationWindowMenu extends JMenuBar {
	private static final long serialVersionUID = 1L;
	
	private EvaluationWindowListener listener;
	
	private JMenu createFileMenu() {
		JMenu ret = new JMenu("File");
		ret.setMnemonic('F');
		
		JMenuItem mi;
		
		mi = new JMenuItem("Back",'b');
		mi.addActionListener(listener);
		ret.add(mi);
		
		mi = new JMenuItem("Export HTML...",'E');
		mi.addActionListener(listener);
		ret.add(mi);
		ret.addSeparator();

		mi = new JMenuItem("Exit",'x');
		mi.addActionListener(listener);
		ret.add(mi);

		return ret;
	}

	public EvaluationWindowMenu(EvaluationWindowListener listener) {
		this.listener = listener;

		add(createFileMenu());
	}
}