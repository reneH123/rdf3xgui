package gui.autocomplete;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.start.MainWindow;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class CompleterBox extends JFrame implements KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;
	private JList list;
	private LinkedList<String> suggestions;

	private MainWindow parent;
	private String typedSeq;
	private boolean fired;
	private int posx;
	private int posy;

	public void setPos(int posx, int posy) {
		this.posx = posx;
		this.posy = posy;
		setLocation(posx, posy);
	}

	public CompleterBox(MainWindow parent, int posx, int posy) {
		// set up window
		setUndecorated(true);
		setSize(250, 150);
		setMinimumSize(new Dimension(150, 40));
		setMaximumSize(new Dimension(250, 150));
		typedSeq = "";
		fired = false;
		this.posx = posx + 30;
		this.posy = posy + 50;

		setLocation(posx, posy);
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		this.parent = parent;
		// set up suggestion list
		suggestions = new LinkedList<String>();
		list = new JList();
		list.setSize(80, 30);
		list.setSelectedIndex(0);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addKeyListener(this);
		list.addMouseListener(this);
		add(new JScrollPane(list));
		list.requestFocusInWindow();
	}

	public void switchOn() {
		typedSeq = "";
		setVisible(true);
		list.requestFocusInWindow();
	}

	public void switchOff() {
		setVisible(false);
	}

	public void setSuggestions(String[] suggestions) {
		this.suggestions.clear();
		for (int i = 0; i < suggestions.length; i++) {
			this.suggestions.add(suggestions[i]);
		}
	}

	public void updateSuggestions() {
		list.setListData(suggestions.toArray());
	}

	private void topicSelected() {
		if (this.suggestions.size() == 0) {
			// shut down auto completion
			switchOff();
			parent.activate();
			return;
		}
		if (this.suggestions.size() == 1) {
			// shut down auto completion and paste remaining text
			switchOff();
			parent.activate();

			String selected = this.suggestions.getFirst();
			selected = selected.substring(typedSeq.length(), selected.length());

			parent.autoComplete(selected);
			return;
		}

		// else there are multiple suggestions:
		// condition: it is assumed, that only JList selections fire
		if (fired == true) {
			switchOff();
			parent.activate();

			Object sel = list.getSelectedValue();
			if (sel==null){
				fired = false;
				return;
			}
			String selected = sel.toString();
			selected = selected.substring(typedSeq.length(), selected.length());
			parent.autoComplete(selected);
			// passivate trigger again
			fired = false;
		}

		// move completion box by 10 pixels right
		posx += 10;
		setLocation(posx, posy);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// System.out.println("code: "+e.getKeyCode()+", char: "+e.getKeyChar());

		char c = e.getKeyChar();

		if ((((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
				|| (c == '<') || (c == '?')) {
			typedSeq += c;
			char cs[] = new char[1];
			cs[0] = c;
			String s = new String(cs);
			parent.autoComplete(s);

			LinkedList<String> newSuggestions = new LinkedList<String>();

			// bound 'suggestions'
			Iterator<String> it = suggestions.iterator();
			while (it.hasNext()) {
				String suggestion = (String) it.next();
				if (suggestion.startsWith(typedSeq)) {
					newSuggestions.add(suggestion);
				}
			}
			this.suggestions = newSuggestions;
			updateSuggestions();

			topicSelected();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_ESCAPE:
			switchOff();
			parent.activate();
			break;
		case KeyEvent.VK_ENTER:
		case KeyEvent.VK_SPACE:
			fired = true;
			topicSelected();
			break;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() % 2 == 0) {
			fired = true;
			topicSelected();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}