package gui.start;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.autocomplete.CompleterBox;
import gui.evaluation.EvaluationWindow;
import gui.syntax.SparqlStyledDocument;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Caret;
import javax.swing.text.Document;

import jgraph.ButtonTabComponent;
import jgraph.GraphCellVisitor;
import jgraph.TreeBuilder;
import jgraph.nodes.TreeNode;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;

import sparql.parser.MySparqlLexer;
import sparql.parser.MySparqlParser;
import db.table.AbstractJDBCConnector;
import db.table.ConnectionData;
//import db.table.MySqlConnectionData;
//import db.table.MySqlJDBCConnector;
import db.table.Rdf3xConnectionData;
import db.table.Rdf3xJDBCConnector;
import db.table.serializer.ExcelSerializer;
import db.table.serializer.SerializerIF;
import db.table.serializer.TXTSerializer;

public class MainWindow extends JFrame {
	// Serialization
	private static final long serialVersionUID = 1L;

	// Windows
	private MainWindowListener listener;
	private MainWindowMenu jMenuBar;
	private EvaluationWindow evalwnd;

	// Upper Panel
	private JPanel jPanel1;
	private JTextPane jTextQuery;
	private Document unstyledDocument;
	private SparqlStyledDocument doc;
	private boolean highlighted=true;
	
	private CompleterBox completer;

	private JToolBar toolBar;

	// Lower Panel
	private JTabbedPane tp;
	private JTable jTable;
	private DefaultTableModel tableModel;
	private int selectedRow;
	private int selectedCol;
	private String selectedCell;

	// FileChooser
	private JFileChooser fileChooser;
	private String selectedDatabaseFile;
	private String selectedQueryFile;
	private String selectedResultSetFile;
	private Clipboard clipBoard;
	

	private AbstractJDBCConnector connectorDB;

	/**
	 * sets the main window's look and feel
	 * 
	 * @param className
	 */
	private void setLookAndFeel(String className) {
		try {
			UIManager.setLookAndFeel(className);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * constructs the main window pane containing 2 panels
	 */
	private void buildEditPanel() {
		// 1. SPARQL Panel
		jPanel1 = new JPanel();
		getContentPane().add(jPanel1, BorderLayout.PAGE_START);

		// Toolbar
		toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 400, 30);
		jPanel1.add(toolBar, BorderLayout.NORTH);

		JButton bOpenDB = new JButton();
		bOpenDB.setActionCommand("Open Database");
		bOpenDB.setIcon(new ImageIcon(getClass().getResource("../../images/filesys.gif")));
		bOpenDB.setToolTipText("Select Database");
		bOpenDB.addActionListener(listener);
		toolBar.add(bOpenDB);

		toolBar.addSeparator();

		JButton bLoadQuery = new JButton();
		bLoadQuery.setActionCommand("Load");
		bLoadQuery.setIcon(new ImageIcon(getClass().getResource("../../images/open.gif")));
		bLoadQuery.setToolTipText("Load Query");
		bLoadQuery.addActionListener(listener);
		toolBar.add(bLoadQuery);

		JButton bSaveQuery = new JButton();
		bSaveQuery.setActionCommand("Save");
		bSaveQuery.setIcon(new ImageIcon(getClass().getResource("../../images/save.gif")));
		bSaveQuery.setToolTipText("Save Query");
		bSaveQuery.addActionListener(listener);
		toolBar.add(bSaveQuery);

		JButton bCopy = new JButton();
		bCopy.setActionCommand("Copy");
		bCopy.setIcon(new ImageIcon(getClass().getResource("../../images/copy.gif")));
		bCopy.setToolTipText("Copy");
		bCopy.addActionListener(listener);
		toolBar.add(bCopy);

		JButton bPaste = new JButton();
		bPaste.setActionCommand("Paste");
		bPaste.setIcon(new ImageIcon(getClass().getResource("../../images/paste.gif")));
		bPaste.setToolTipText("Paste");
		bPaste.addActionListener(listener);
		toolBar.add(bPaste);
		
		JButton bSyntax = new JButton();
		bSyntax.setActionCommand("Syntax");
		bSyntax.setIcon(new ImageIcon(getClass().getResource("../../images/syntax.gif")));
		bSyntax.setToolTipText("Syntax Highlighting");
		bSyntax.addActionListener(listener);
		toolBar.add(bSyntax);
		
		JButton bExplain = new JButton();
		bExplain.setActionCommand("Explain");
		bExplain.setIcon(new ImageIcon(getClass().getResource("../../images/explain.gif")));
		bExplain.setToolTipText("Explain query");
		bExplain.addActionListener(listener);
		toolBar.add(bExplain);

		JButton bLaunch = new JButton();
		bLaunch.setActionCommand("Launch");
		bLaunch.setIcon(new ImageIcon(getClass().getResource("../../images/launch.gif")));		
		bLaunch.setToolTipText("Launch Query");
		bLaunch.addActionListener(listener);
		toolBar.add(bLaunch);

		toolBar.addSeparator();

		JButton bSaveStat = new JButton();
		bSaveStat.setActionCommand("Save Result Set");
		bSaveStat.setIcon(new ImageIcon(getClass().getResource("../../images/save.gif")));		
		bSaveStat.setToolTipText("Save Result Set");
		bSaveStat.addActionListener(listener);
		toolBar.add(bSaveStat);

		JButton bDisplay = new JButton();
		bDisplay.setActionCommand("Display");		
		bDisplay.setIcon(new ImageIcon(getClass().getResource("../../images/perspective.gif")));		
		bDisplay.setToolTipText("Display SPO view");
		bDisplay.addActionListener(listener);
		toolBar.add(bDisplay);

		toolBar.addSeparator();

		JButton bExit = new JButton();
		bExit.setActionCommand("Exit");
		bExit.setIcon(new ImageIcon(getClass().getResource("../../images/exit.gif")));		
		bExit.setToolTipText("Exit Application ");
		bExit.addActionListener(listener);
		toolBar.add(bExit);

		// edit field with decoration
		jPanel1.setLayout(null);
		jPanel1.setAlignmentX(0.0f);
		jPanel1.setBounds(0, 30, 498, 212);
		jPanel1.setPreferredSize(new Dimension(500, 165));

		jTextQuery = new JTextPane();
		jTextQuery.setBounds(0, 30, 400, 53);
		jTextQuery.setBorder(new LineBorder(new Color(0, 0, 0), 1, false));
		jTextQuery.setPreferredSize(new Dimension(-1, 22));
		jTextQuery.addKeyListener(listener);
		
		unstyledDocument = jTextQuery.getDocument();
		doc = new SparqlStyledDocument();
		doc.setKeywords(MySparqlParser.KEYWORDS);
		doc.setCommentColor(new Color(63, 197, 95));
		jTextQuery.setDocument(doc);
		//jTextQuery.setText("select * from einstein"); // "select ?a where {?a ?b ?c}"
		jTextQuery.setText("select * where {?a ?b ?c}");
		jPanel1.add(jTextQuery);

		// 2. Result Panel
		tableModel = new DefaultTableModel();

		jTable = new JTable() { 
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		jTable.setLocation(10, 100);
		jTable.setEnabled(true);
		jTable.setCellSelectionEnabled(false);
		jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jTable.setRowSelectionAllowed(true);
		jTable.setColumnSelectionAllowed(true);
		jTable.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		jTable.setModel(tableModel);
		jTable.addMouseListener(listener);

		tp = new JTabbedPane(JTabbedPane.BOTTOM);
		tp.addTab("Results", new JScrollPane(jTable));
		getContentPane().add(tp, BorderLayout.CENTER);
	}

	/**
	 * open file chooser dialog
	 */
	public void OpenFile() {
		fileChooser = new JFileChooser();
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File f = fileChooser.getSelectedFile();
			selectedDatabaseFile = f.toString();
			
			// e.g. "rdf3x:///home/rene/workspace/rdf3xdb/toy"
			ConnectionData connectionData = new Rdf3xConnectionData(selectedDatabaseFile);
			
			connectorDB = Rdf3xJDBCConnector.getInstance((Rdf3xConnectionData)connectionData);
			try {
				connectorDB.disconnect();
				connectorDB.setConnectionData(connectionData);
				connectorDB.connect();
			} catch (Throwable e) {
				return;
			}
		}
	}

	/**
	 * constructor for main window. all listener depending stuff should be put
	 * into method 'setListener()'
	 */
	public MainWindow() {
		super("rdf3x client");
		setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../../images/icon.gif")));
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setMinimumSize(new Dimension(450,300));
		BorderLayout thisLayout = new BorderLayout();
		getContentPane().setLayout(thisLayout);
		listener = null;
		selectedDatabaseFile = null;
		selectedCol = 0;
		selectedRow = 0;
		selectedCell = "";
	}

	/**
	 * register window listener to main window, main menu bar and child windows
	 * 
	 * @param listener
	 */
	public void setListener(MainWindowListener listener) {
		// main window with menu and control
		this.listener = listener;
		addComponentListener(this.listener);

		buildEditPanel();

		jMenuBar = new MainWindowMenu(this.listener);
		setJMenuBar(jMenuBar);

		// link child window
		evalwnd = new EvaluationWindow(this);
		
		// set up auto completion box
		completer = new CompleterBox(this, 30, 40);
		completer.setVisible(false);
				
		ConnectionData connectionData = new Rdf3xConnectionData(selectedDatabaseFile);
		//ConnectionData connectionData = new MySqlConnectionData("localhost", "3306", "yago", "root","superuser");
		
		connectionData.setDatabase(selectedDatabaseFile);
		connectorDB = Rdf3xJDBCConnector.getInstance((Rdf3xConnectionData)connectionData);
		//connectorDB = MySqlJDBCConnector.getInstance((MySqlConnectionData)connectionData);
		
		evalwnd.setConnectorDB(connectorDB);		
		clipBoard = getToolkit().getSystemClipboard();
	}

	/**
	 * show the evaluation window
	 */
	public void switchEvalWnd() {
		evalwnd.setVisible(true);
		evalwnd.setSelectedTopic(selectedCell);
	}

	/**
	 * resize handler for main window
	 */
	public void reSize() {
		if (jTextQuery != null) {
			jTextQuery.setSize(this.getWidth(), this.getHeight() - 30
					- tp.getHeight());
		}
		
		if (toolBar != null) {
			toolBar.setSize(this.getWidth(), 30);
		}
		if (jTable != null) {
			jTable.setLocation(10, 10);
			jTable.setSize(this.getWidth() - 50, this.getHeight()
					- jPanel1.getHeight() - 80);
		}
	}
	
	/**
	 * input:  query text from 'jTextQuery' text pane
	 * output: a new tab in tabbed pane 'tp' with complete query execution plan drawn with jGraph
	 */
	public void explainRDFStatement(){
		String query = "explain " + jTextQuery.getText();
		
		if (connectorDB.query(query) == false) {
			// if error encountered do nothing
			return;
		}
		String[][] data = connectorDB.getRows();
		
		if (data==null){
			// ignore anything
			return;
		}
			
		TreeBuilder builder = new TreeBuilder();
		for (int i=0;i<data.length;i++){
			builder.create(data[i]);
		}
			
		TreeNode node = builder.getRoot();
		if (node==null){
			System.err.println("Explanation query plan is empty!");
			return;
		}
		DefaultGraphCell[] cells = node.accept(new GraphCellVisitor());
		JGraph jGraph = GraphCellVisitor.createJGraph(cells, this);
		
		int tabs = tp.getTabCount();
		tp.insertTab("Explanation", null, new JScrollPane(jGraph), null, tabs);
		tp.setTabComponentAt(tabs, new ButtonTabComponent(tp));
	}

	/**
	 * method called when the 'launch' button is hit
	 * 
	 * the result set is represented into a JTable
	 * 
	 * e.g. for a SQL statement: SELECT * FROM peoplempi e.g. for a RDF
	 * statement: SELECT ?a WHERE {?a ?b ?c}
	 * 
	 */
	public void executeRDFStatement() {
		String query = jTextQuery.getText();

		if (connectorDB.query(query) == true) {
			String[][] data = connectorDB.getRows();
			String[] colNames = connectorDB.getColNames();

			tableModel = new DefaultTableModel(data, colNames);
			jTable.setModel(tableModel);
		} else {
			System.err.println("Wrong query expression!");
		}
	}
	
	/**
	 * save table into a file
	 */
	public void saveResultSet(){
		fileChooser = new JFileChooser();
		FileNameExtensionFilter txtFilter = new FileNameExtensionFilter("Text format","txt");
		FileNameExtensionFilter xlsFilter = new FileNameExtensionFilter("Excel table","xls");
		fileChooser.addChoosableFileFilter(txtFilter);
		fileChooser.addChoosableFileFilter(xlsFilter);
		fileChooser.setFileFilter(txtFilter);
		int result = fileChooser.showSaveDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION){
			selectedResultSetFile = fileChooser.getSelectedFile().toString();
			
			FileFilter selected = fileChooser.getFileFilter();
			SerializerIF serializer = null;
			try{
				if (selected.equals(xlsFilter)){
					if (selectedResultSetFile.contains(".")==false){
						selectedResultSetFile+=".xls";
					}
					serializer = new ExcelSerializer(selectedResultSetFile, tableModel);
				}
				else{
					if (selected.equals(txtFilter)){
						if (selectedResultSetFile.contains(".")==false){
							selectedResultSetFile+=".txt";
						}
					}
					serializer = new TXTSerializer(selectedResultSetFile, tableModel);
				}
			} catch(Exception e){
				System.err.println("Could not save tableModel into file '" + selectedResultSetFile + "'!");
				return;
			} 

			serializer.serialize();
		}
	}

	/**
	 * a cell from the result set had been selected
	 */
	public void cellSelected() {
		selectedCol = jTable.getSelectedColumn();
		selectedRow = jTable.getSelectedRow();
		selectedCell = jTable.getValueAt(selectedRow, selectedCol).toString();
	}
	
	/**
	 * opens a query file
	 */
	public void openQueryFile()
	{
		fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Sparql Query","sparql");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showOpenDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION){
			selectedQueryFile = fileChooser.getSelectedFile().toString();
			try {
				FileReader fr = new FileReader(selectedQueryFile);				
				int c;
				String query = "";
				while ((c = fr.read()) != -1) {
					query += (char) c;
				}
				fr.close();
				jTextQuery.setText(query);
						
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * saves a query into a file
	 */
	public void saveQueryFile(){
		fileChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Sparql Query","sparql");
		fileChooser.setFileFilter(filter);
		int result = fileChooser.showSaveDialog(this);
		
		if (result == JFileChooser.APPROVE_OPTION){
			selectedQueryFile = fileChooser.getSelectedFile().toString();
			if (selectedQueryFile.contains(".")==false){
				selectedQueryFile+=".sparql";
			}
			try {
				FileWriter fw = new FileWriter(selectedQueryFile);
				fw.write(jTextQuery.getText());
				fw.close();						
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * copy into system clipboard
	 */
	public void copyClipboard(){
		clipBoard.setContents(new StringSelection(jTextQuery.getSelectedText()), listener);
		jTextQuery.requestFocus();
	}
	
	/**
	 * paste from system clipboard
	 */
	public void pasteClipboard(){
		Transferable cont = clipBoard.getContents(this);
		if (cont == null){
			return;
		}
		String sel = "";
		try {
			sel = (String)cont.getTransferData(DataFlavor.stringFlavor);
		} catch (UnsupportedFlavorException e) {
			return;
		} catch (IOException e) {
			return;
		}
		if (sel == null){
			return;
		}
		jTextQuery.replaceSelection(sel.toString());
		jTextQuery.requestFocus();
	}
	
	/**
	 * open about window
	 */
	public void aboutDialog(){
		String usedPackes = "\n\n\tUsed Packages: rdf3x, antLR, POI, jGraph, HtmlParser";
		JOptionPane.showMessageDialog(this,
			    "2009 Max Planck Institute for Informatics.\n\n" +
			    "rdf3x - Thomas Neumann, Gerhard Weikum\n" + 
			    "GUI - Rene Haberland" + usedPackes, 
			    "Authors",JOptionPane.INFORMATION_MESSAGE);
	}
	
	
	/////////////////// AUTOCOMPLETION ////////////////////////////
	
	/**
	 * activate window focus
	 * 
	 * this is necessary when switching back from child windows or
	 * while auto completion
	 */
	public void activate() {
		this.setFocusable(true);
		jTextQuery.requestFocus();
	}
	
	/**
	 * passivate window focus
	 * 
	 * this is necessary during interactions in other windows / dialogues 
	 * 
	 */
	public void passivate() {
		this.setFocusable(false);
	}
	
	/**
	 * insert/replace on current cursor selection in query text pane 'selected'
	 * 
	 * all before/after cursor or cursor selection will not be manipulated
	 * 
	 * @param selected
	 */
	public void autoComplete(String selected) {
		jTextQuery.replaceSelection(selected);
	}
	
	public void switchSyntaxHighlighting(){
		String t = jTextQuery.getText();
		if (highlighted){
			// deactivate highlighting
			jTextQuery.setDocument(doc);
			jTextQuery.setText(t);
		}
		else{
			// activate highlighting (again)
			jTextQuery.setDocument(unstyledDocument);
			jTextQuery.setText(t);
		}
		highlighted = ! highlighted;
	}
	
	/**
	 * analyzes 'context' which represents the SPARQL listing til cursor position
	 * 
	 *  depending on cursor position an autocompletion assistant (box) will show alternatives
	 *   within triple statements for SUBJECT, PREDICATE, OBJECT, NONE
	 * 
	 * @param context
	 * @return
	 */	
	private int getTriplePosition(String context){
		InputStream in;
		ANTLRInputStream input;
		MySparqlLexer lexer;
		CommonTokenStream tokens;
		MySparqlParser parser;
		
		// parse query
		in = new ByteArrayInputStream(context.getBytes());
		try {
			input = new ANTLRInputStream(in);
		} catch (IOException e) {
			System.err.println("Cannot open stream!");
			e.printStackTrace();
			return 0;
		}
		lexer = new MySparqlLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new MySparqlParser(tokens);
	
		try{
			parser.query();
		} catch (RecognitionException e){
			return 0;
		}
		return parser.myState;
	}

	/**
	 * lookup suggestions
	 */
	private String[] loadSuggestions(String prefix) {
		int triplePos = getTriplePosition(prefix);
		
		String query;
		switch(triplePos){
		case MySparqlParser.MYSTATE_SUBJECT:
			query = connectorDB.getQuerySubjects();
			break;
		case MySparqlParser.MYSTATE_PREDICATE:
			query = connectorDB.getQueryPredicates();
			break;
		case MySparqlParser.MYSTATE_OBJECT:
			query = connectorDB.getQueryObjects();
			break;
		default:
			return null;
		}
		
		if (connectorDB.query(query) == false) {
			return null;
		}
		return connectorDB.getFirstCol();
	}
	
	public void autoCompletion(){
		String text = jTextQuery.getText();	
		int cpos = jTextQuery.getCaretPosition();

		String prefix = text.substring(0, cpos);
		
		String[] newSuggestions = loadSuggestions(prefix);
		if (newSuggestions == null) {
			return;
		}
		completer.setSuggestions(newSuggestions);
		completer.updateSuggestions();
		// set position of the completion box 
		Caret caret = jTextQuery.getCaret();
		Point p = caret.getMagicCaretPosition();
		Point p_wnd = getLocation();
		completer.setPos((int)(p.getX()+p_wnd.getX())+30, (int)(p.getY()+p_wnd.getY())+30);
		
		passivate();
		// open AutoCompletion
		completer.switchOn();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Useless attempt, start Rdf3xGUI instead !");
	}
}
