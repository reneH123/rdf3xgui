package gui.start;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainWindowListener implements ComponentListener, ActionListener,
		MouseListener, KeyListener, ClipboardOwner {
	private MainWindow mainwnd;

	//@Override
	public void componentHidden(ComponentEvent e) {
	}

	//@Override
	public void componentMoved(ComponentEvent e) {
	}

	//@Override
	public void componentShown(ComponentEvent e) {
	}

	@Override
	public void componentResized(ComponentEvent e) {
		mainwnd.reSize();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Open Database")) {
			mainwnd.OpenFile();
		}
		if (e.getActionCommand().equals("Exit")) {
			System.exit(0);
		}
		if (e.getActionCommand().equals("Load")) {
			mainwnd.openQueryFile();
		}
		if (e.getActionCommand().equals("Save")) {
			mainwnd.saveQueryFile();
		}

		if (e.getActionCommand().equals("Copy")) {
			mainwnd.copyClipboard();
		}
		if (e.getActionCommand().equals("Paste")) {
			mainwnd.pasteClipboard();
		}
		if (e.getActionCommand().equals("Explain")) {
			mainwnd.explainRDFStatement();
		}
		if (e.getActionCommand().equals("Launch")) {
			mainwnd.executeRDFStatement();
		}
		if (e.getActionCommand().equals("Save Result Set")) {
			mainwnd.saveResultSet();
		}
		if (e.getActionCommand().equals("Display")) {
			mainwnd.switchEvalWnd();
		}
		if (e.getActionCommand().equals("About")) {
			mainwnd.aboutDialog();
		}
		if (e.getActionCommand().equals("Syntax")){
			mainwnd.switchSyntaxHighlighting();
		}
		/*
		 * else { String cmd = e.getActionCommand(); System.out.println(cmd); }
		 */
	}

	public MainWindowListener(MainWindow mainwnd) {
		this.mainwnd = mainwnd;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		mainwnd.cellSelected();
		if (e.getClickCount() % 2 == 0) {
			mainwnd.switchEvalWnd();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();

		switch (keyCode) {
		case KeyEvent.VK_SPACE:
			if (e.isControlDown()) {
				mainwnd.autoCompletion();
			}
			break;
		default:
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void lostOwnership(Clipboard cb, Transferable t) {
	}
}