package gui.start;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.awt.Event;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class MainWindowMenu extends JMenuBar {
	private static final long serialVersionUID = 1L;
	
	private MainWindowListener listener;
	
	private void setCtrlAccelerator(JMenuItem mi, char acc) {
		KeyStroke ks = KeyStroke.getKeyStroke(acc, Event.CTRL_MASK);
		mi.setAccelerator(ks);
	}
	
	private JMenu createFileMenu() {
		JMenu ret = new JMenu("File");
		ret.setMnemonic('F');

		JMenuItem mi;
		mi = new JMenuItem("Open Database", 'o');
		setCtrlAccelerator(mi, 'O');
		mi.addActionListener(listener);
		ret.add(mi);
		ret.addSeparator();

		mi = new JMenuItem("Exit", 'x');
		mi.addActionListener(listener);
		ret.add(mi);

		return ret;
	}

	private JMenu createQueryMenu() {
		JMenu ret = new JMenu("Query");
		ret.setMnemonic('Q');

		JMenuItem mi;
		mi = new JMenuItem("Load", 'l');
		mi.addActionListener(listener);
		ret.add(mi);

		mi = new JMenuItem("Save", 's');
		mi.addActionListener(listener);
		ret.add(mi);
		
		mi = new JMenuItem("Explain", 'x');
		mi.addActionListener(listener);
		ret.add(mi);

		mi = new JMenuItem("Launch", 'a');
		mi.addActionListener(listener);
		ret.add(mi);

		return ret;
	}

	private JMenu createStatisticsMenu() {
		JMenu ret = new JMenu("Statistics");
		ret.setMnemonic('S');

		JMenuItem mi;
		mi = new JMenuItem("Save Result Set", 's');
		mi.addActionListener(listener);
		ret.add(mi);
		ret.addSeparator();

		mi = new JMenuItem("Display", 'd');
		mi.addActionListener(listener);
		ret.add(mi);

		return ret;
	}

	private JMenu createHelpMenu() {
		JMenu ret = new JMenu("Help");
		ret.setMnemonic('H');
		
		JMenuItem mi;
		mi = new JMenuItem("About", 'a');
		mi.addActionListener(listener);
		ret.add(mi);

		return ret;
	}
	
	public MainWindowMenu(MainWindowListener listener) {
		this.listener = listener;
		
		add(createFileMenu());
		add(createQueryMenu());
		add(createStatisticsMenu());
		add(createHelpMenu());
	}
}
