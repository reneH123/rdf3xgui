package gui.start;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import javax.swing.WindowConstants;

public class Rdf3xGUI {
	public Rdf3xGUI() {
		MainWindow mainwnd = new MainWindow();
		MainWindowListener listener = new MainWindowListener(mainwnd);
		mainwnd.setListener(listener);
		mainwnd.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		mainwnd.setVisible(true);
	}

	public static void main(String[] args) {
		new Rdf3xGUI();
	}
}