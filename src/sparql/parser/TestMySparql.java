package sparql.parser;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import junit.framework.TestCase;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

public class TestMySparql extends TestCase {
	private String sparql;
	private InputStream in;
	private ANTLRInputStream input;
	private MySparqlLexer lexer;
	private CommonTokenStream tokens;
	private MySparqlParser parser;

	private boolean parse() {
		in = new ByteArrayInputStream(sparql.getBytes());
		try {
			input = new ANTLRInputStream(in);
		} catch (IOException e) {
			System.err.println("Cannot open stream!");
			e.printStackTrace();
			System.exit(-1);
		}
		lexer = new MySparqlLexer(input);
		tokens = new CommonTokenStream(lexer);
		parser = new MySparqlParser(tokens);
		try{
			parser.query();
		} catch (RecognitionException e){
			return false;
		}
		return true;
	}
	
	// it is necessary to check only some simple SPO-tests due to the full syntax specification based on the given formal SPARQL grammar 

	public void testSimple(){
		sparql = "PREFIX cd: <http://example.org/cd/>" + "\n"
				+ "SELECT ?t ?a ?y" + "\n"
				+ "FROM <http://cd.com/listacd.ttl>" + "\n" + "WHERE {" + "\n"
				+ "?t ?a ?a." + "\n" + "?t ?y ?y2."
				+ "\n" + "}";
		assertTrue(parse());
		
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
		assertEquals(parser.getNumberOfSyntaxErrors(),0);
	}
	
	public void testOneTriple(){
		sparql = "select ?a ?b ?c where {";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_SUBJECT);
		
		//
		sparql = "select ?a ?b ?c where {?a ";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_PREDICATE);
		
		//
		sparql = "select ?a ?b ?c where {?a ?b";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_OBJECT);
		
		//
		sparql = "select ?a ?b ?c where {?a ?b ?c";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
		
		//
		sparql = "select ?a ?b ?c where {?a ?b ?c.";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_SUBJECT);
		
		//
		sparql = "select ?a ?b ?c where {?a ?b ?c. }";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
	}
	
	public void testTwoTriple(){
		sparql = "select ?a ?b ?c where {?a ?b ?c. ?a";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_PREDICATE);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c. ?a ?b";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_OBJECT);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c. ?a ?b ?c";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c. ?a ?b ?c.";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_SUBJECT);
	}

	public void testPredObjSeq(){
		sparql = "select ?a ?b ?c where {?a ?b ?c;";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_PREDICATE);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c; ?b2";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_OBJECT);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c; ?b2 ?c";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c; ?b2 ?c.";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_SUBJECT);
	}
	
	public void testObjSeq(){
		sparql = "select ?a ?b ?c where {?a ?b ?c,";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_OBJECT);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c, ?c2";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c,?c2,";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_OBJECT);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c,?c2,?c3.";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_SUBJECT);
		
		sparql = "select ?a ?b ?c where {?a ?b ?c, ?c2, ?c3";
		assertTrue(parse());
		assertEquals(parser.myState,MySparqlParser.MYSTATE_NONE);
	}
		
	public static void main(String[] args) throws Exception{
		ANTLRInputStream input = new ANTLRInputStream(System.in);
		MySparqlLexer lexer = new MySparqlLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MySparqlParser parser = new MySparqlParser(tokens);
		parser.query();
	}	
}