// $ANTLR 3.1.2 D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g 2009-06-22 11:00:19

package sparql.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class MySparqlParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "BASE", "IRI_REF", "PREFIX", "PNAME_NS", "SELECT", "DISTINCT", "REDUCED", "ASTERISK", "CONSTRUCT", "DESCRIBE", "ASK", "FROM", "NAMED", "WHERE", "ORDER", "BY", "ASC", "DESC", "LIMIT", "INTEGER", "OFFSET", "OPEN_CURLY_BRACE", "DOT", "CLOSE_CURLY_BRACE", "OPTIONAL", "GRAPH", "UNION", "FILTER", "OPEN_BRACE", "CLOSE_BRACE", "COMMA", "SEMICOLON", "A", "OPEN_SQUARE_BRACE", "CLOSE_SQUARE_BRACE", "VAR1", "VAR2", "OR", "AND", "EQUAL", "NOT_EQUAL", "LESS", "GREATER", "LESS_EQUAL", "GREATER_EQUAL", "PLUS", "MINUS", "DIVIDE", "NOT", "STR", "LANG", "LANGMATCHES", "DATATYPE", "BOUND", "SAMETERM", "ISIRI", "ISURI", "ISBLANK", "ISLITERAL", "REGEX", "LANGTAG", "REFERENCE", "DECIMAL", "DOUBLE", "INTEGER_POSITIVE", "DECIMAL_POSITIVE", "DOUBLE_POSITIVE", "INTEGER_NEGATIVE", "DECIMAL_NEGATIVE", "DOUBLE_NEGATIVE", "TRUE", "FALSE", "STRING_LITERAL1", "STRING_LITERAL2", "STRING_LITERAL_LONG1", "STRING_LITERAL_LONG2", "PNAME_LN", "BLANK_NODE_LABEL", "WS", "PN_PREFIX", "PN_LOCAL", "VARNAME", "PN_CHARS_BASE", "DIGIT", "EXPONENT", "ECHAR", "PN_CHARS_U", "PN_CHARS", "EOL", "COMMENT", "ANY"
    };
    public static final int PREFIX=6;
    public static final int EXPONENT=88;
    public static final int CLOSE_SQUARE_BRACE=38;
    public static final int GRAPH=29;
    public static final int REGEX=63;
    public static final int PNAME_LN=80;
    public static final int CONSTRUCT=12;
    public static final int NOT=52;
    public static final int EOF=-1;
    public static final int VARNAME=85;
    public static final int ISLITERAL=62;
    public static final int EOL=92;
    public static final int GREATER=46;
    public static final int NOT_EQUAL=44;
    public static final int LESS=45;
    public static final int LANGMATCHES=55;
    public static final int DOUBLE=67;
    public static final int BASE=4;
    public static final int PN_CHARS_U=90;
    public static final int COMMENT=93;
    public static final int OPEN_CURLY_BRACE=25;
    public static final int SELECT=8;
    public static final int CLOSE_CURLY_BRACE=27;
    public static final int DOUBLE_POSITIVE=70;
    public static final int DIVIDE=51;
    public static final int BOUND=57;
    public static final int ISIRI=59;
    public static final int A=36;
    public static final int ASC=20;
    public static final int ASK=14;
    public static final int BLANK_NODE_LABEL=81;
    public static final int SEMICOLON=35;
    public static final int ISBLANK=61;
    public static final int WS=82;
    public static final int NAMED=16;
    public static final int INTEGER_POSITIVE=68;
    public static final int OR=41;
    public static final int STRING_LITERAL2=77;
    public static final int FILTER=31;
    public static final int DESCRIBE=13;
    public static final int STRING_LITERAL1=76;
    public static final int PN_CHARS=91;
    public static final int DATATYPE=56;
    public static final int LESS_EQUAL=47;
    public static final int DOUBLE_NEGATIVE=73;
    public static final int FROM=15;
    public static final int FALSE=75;
    public static final int DISTINCT=9;
    public static final int LANG=54;
    public static final int IRI_REF=5;
    public static final int WHERE=17;
    public static final int ORDER=18;
    public static final int LIMIT=22;
    public static final int AND=42;
    public static final int ASTERISK=11;
    public static final int ISURI=60;
    public static final int STR=53;
    public static final int SAMETERM=58;
    public static final int COMMA=34;
    public static final int OFFSET=24;
    public static final int EQUAL=43;
    public static final int DECIMAL_POSITIVE=69;
    public static final int PLUS=49;
    public static final int DIGIT=87;
    public static final int DOT=26;
    public static final int INTEGER=23;
    public static final int BY=19;
    public static final int REDUCED=10;
    public static final int INTEGER_NEGATIVE=71;
    public static final int PN_LOCAL=84;
    public static final int PNAME_NS=7;
    public static final int REFERENCE=65;
    public static final int CLOSE_BRACE=33;
    public static final int MINUS=50;
    public static final int TRUE=74;
    public static final int OPEN_SQUARE_BRACE=37;
    public static final int UNION=30;
    public static final int ECHAR=89;
    public static final int OPTIONAL=28;
    public static final int ANY=94;
    public static final int STRING_LITERAL_LONG2=79;
    public static final int PN_CHARS_BASE=86;
    public static final int DECIMAL=66;
    public static final int VAR1=39;
    public static final int VAR2=40;
    public static final int STRING_LITERAL_LONG1=78;
    public static final int DECIMAL_NEGATIVE=72;
    public static final int PN_PREFIX=83;
    public static final int DESC=21;
    public static final int OPEN_BRACE=32;
    public static final int GREATER_EQUAL=48;
    public static final int LANGTAG=64;

    // delegates
    // delegators


        public MySparqlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public MySparqlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return MySparqlParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g"; }


      // keywords needed
      public static final String[] KEYWORDS = { "base", "select", "order by", "from",
          "graph", "str", "isuri", "prefix", "construct", "limit", "from",
          "named", "optional", "lang", "isiri", "describe", "offset",
          "where", "union", "langmatches", "isliteral", "ask", "distinct",
          "filter", "datatype", "regex", "reduced", "a", "bound", "true",
          "sameterm", "false" };


      // accepted final states for translated semantics (auto completion) 
      public static final int MYSTATE_NONE = 0;
      public static final int MYSTATE_SUBJECT = 1;
      public static final int MYSTATE_PREDICATE = 2;
      public static final int MYSTATE_OBJECT = 3;
      
      public int myState = MYSTATE_NONE;
        
      public void displayRecognitionError(String[] tokenNames,
                                            RecognitionException e) {
        //String hdr = getErrorHeader(e);
        //String msg = getErrorMessage(e, tokenNames);
        // Now do something with hdr and msg...
      }
      




    // $ANTLR start "query"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:94:1: query : prologue ( selectQuery | constructQuery | describeQuery | askQuery ) EOF ;
    public final void query() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:5: ( prologue ( selectQuery | constructQuery | describeQuery | askQuery ) EOF )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:7: prologue ( selectQuery | constructQuery | describeQuery | askQuery ) EOF
            {
            pushFollow(FOLLOW_prologue_in_query64);
            prologue();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:16: ( selectQuery | constructQuery | describeQuery | askQuery )
            int alt1=4;
            switch ( input.LA(1) ) {
            case SELECT:
                {
                alt1=1;
                }
                break;
            case CONSTRUCT:
                {
                alt1=2;
                }
                break;
            case DESCRIBE:
                {
                alt1=3;
                }
                break;
            case ASK:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:18: selectQuery
                    {
                    pushFollow(FOLLOW_selectQuery_in_query68);
                    selectQuery();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:32: constructQuery
                    {
                    pushFollow(FOLLOW_constructQuery_in_query72);
                    constructQuery();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:49: describeQuery
                    {
                    pushFollow(FOLLOW_describeQuery_in_query76);
                    describeQuery();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:95:65: askQuery
                    {
                    pushFollow(FOLLOW_askQuery_in_query80);
                    askQuery();

                    state._fsp--;


                    }
                    break;

            }

            match(input,EOF,FOLLOW_EOF_in_query84); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "query"


    // $ANTLR start "prologue"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:98:1: prologue : ( baseDecl )? ( prefixDecl )* ;
    public final void prologue() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:5: ( ( baseDecl )? ( prefixDecl )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:7: ( baseDecl )? ( prefixDecl )*
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:7: ( baseDecl )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==BASE) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:7: baseDecl
                    {
                    pushFollow(FOLLOW_baseDecl_in_prologue101);
                    baseDecl();

                    state._fsp--;


                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:17: ( prefixDecl )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==PREFIX) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:99:17: prefixDecl
            	    {
            	    pushFollow(FOLLOW_prefixDecl_in_prologue104);
            	    prefixDecl();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "prologue"


    // $ANTLR start "baseDecl"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:102:1: baseDecl : BASE IRI_REF ;
    public final void baseDecl() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:103:5: ( BASE IRI_REF )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:103:7: BASE IRI_REF
            {
            match(input,BASE,FOLLOW_BASE_in_baseDecl122); 
            match(input,IRI_REF,FOLLOW_IRI_REF_in_baseDecl124); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "baseDecl"


    // $ANTLR start "prefixDecl"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:106:1: prefixDecl : PREFIX PNAME_NS IRI_REF ;
    public final void prefixDecl() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:107:5: ( PREFIX PNAME_NS IRI_REF )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:107:7: PREFIX PNAME_NS IRI_REF
            {
            match(input,PREFIX,FOLLOW_PREFIX_in_prefixDecl141); 
            match(input,PNAME_NS,FOLLOW_PNAME_NS_in_prefixDecl143); 
            match(input,IRI_REF,FOLLOW_IRI_REF_in_prefixDecl145); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "prefixDecl"


    // $ANTLR start "selectQuery"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:110:1: selectQuery : SELECT ( DISTINCT | REDUCED )? ( ( var )+ | ASTERISK ) ( datasetClause )* whereClause solutionModifier ;
    public final void selectQuery() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:5: ( SELECT ( DISTINCT | REDUCED )? ( ( var )+ | ASTERISK ) ( datasetClause )* whereClause solutionModifier )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:7: SELECT ( DISTINCT | REDUCED )? ( ( var )+ | ASTERISK ) ( datasetClause )* whereClause solutionModifier
            {
            match(input,SELECT,FOLLOW_SELECT_in_selectQuery162); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:14: ( DISTINCT | REDUCED )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=DISTINCT && LA4_0<=REDUCED)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
                    {
                    if ( (input.LA(1)>=DISTINCT && input.LA(1)<=REDUCED) ) {
                        input.consume();
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:38: ( ( var )+ | ASTERISK )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=VAR1 && LA6_0<=VAR2)) ) {
                alt6=1;
            }
            else if ( (LA6_0==ASTERISK) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:40: ( var )+
                    {
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:40: ( var )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0>=VAR1 && LA5_0<=VAR2)) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:40: var
                    	    {
                    	    pushFollow(FOLLOW_var_in_selectQuery177);
                    	    var();

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:47: ASTERISK
                    {
                    match(input,ASTERISK,FOLLOW_ASTERISK_in_selectQuery182); 

                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:58: ( datasetClause )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==FROM) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:111:58: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_selectQuery186);
            	    datasetClause();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            pushFollow(FOLLOW_whereClause_in_selectQuery189);
            whereClause();

            state._fsp--;

            pushFollow(FOLLOW_solutionModifier_in_selectQuery191);
            solutionModifier();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "selectQuery"


    // $ANTLR start "constructQuery"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:114:1: constructQuery : CONSTRUCT constructTemplate ( datasetClause )* whereClause solutionModifier ;
    public final void constructQuery() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:115:5: ( CONSTRUCT constructTemplate ( datasetClause )* whereClause solutionModifier )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:115:7: CONSTRUCT constructTemplate ( datasetClause )* whereClause solutionModifier
            {
            match(input,CONSTRUCT,FOLLOW_CONSTRUCT_in_constructQuery208); 
            pushFollow(FOLLOW_constructTemplate_in_constructQuery210);
            constructTemplate();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:115:35: ( datasetClause )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==FROM) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:115:35: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_constructQuery212);
            	    datasetClause();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            pushFollow(FOLLOW_whereClause_in_constructQuery215);
            whereClause();

            state._fsp--;

            pushFollow(FOLLOW_solutionModifier_in_constructQuery217);
            solutionModifier();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "constructQuery"


    // $ANTLR start "describeQuery"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:118:1: describeQuery : DESCRIBE ( ( varOrIRIref )+ | ASTERISK ) ( datasetClause )* ( whereClause )? solutionModifier ;
    public final void describeQuery() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:5: ( DESCRIBE ( ( varOrIRIref )+ | ASTERISK ) ( datasetClause )* ( whereClause )? solutionModifier )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:7: DESCRIBE ( ( varOrIRIref )+ | ASTERISK ) ( datasetClause )* ( whereClause )? solutionModifier
            {
            match(input,DESCRIBE,FOLLOW_DESCRIBE_in_describeQuery234); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:16: ( ( varOrIRIref )+ | ASTERISK )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==IRI_REF||LA10_0==PNAME_NS||(LA10_0>=VAR1 && LA10_0<=VAR2)||LA10_0==PNAME_LN) ) {
                alt10=1;
            }
            else if ( (LA10_0==ASTERISK) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:18: ( varOrIRIref )+
                    {
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:18: ( varOrIRIref )+
                    int cnt9=0;
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==IRI_REF||LA9_0==PNAME_NS||(LA9_0>=VAR1 && LA9_0<=VAR2)||LA9_0==PNAME_LN) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:18: varOrIRIref
                    	    {
                    	    pushFollow(FOLLOW_varOrIRIref_in_describeQuery238);
                    	    varOrIRIref();

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt9 >= 1 ) break loop9;
                                EarlyExitException eee =
                                    new EarlyExitException(9, input);
                                throw eee;
                        }
                        cnt9++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:33: ASTERISK
                    {
                    match(input,ASTERISK,FOLLOW_ASTERISK_in_describeQuery243); 

                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:44: ( datasetClause )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==FROM) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:44: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_describeQuery247);
            	    datasetClause();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:59: ( whereClause )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==WHERE||LA12_0==OPEN_CURLY_BRACE) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:119:59: whereClause
                    {
                    pushFollow(FOLLOW_whereClause_in_describeQuery250);
                    whereClause();

                    state._fsp--;


                    }
                    break;

            }

            pushFollow(FOLLOW_solutionModifier_in_describeQuery253);
            solutionModifier();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "describeQuery"


    // $ANTLR start "askQuery"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:122:1: askQuery : ASK ( datasetClause )* whereClause ;
    public final void askQuery() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:123:5: ( ASK ( datasetClause )* whereClause )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:123:7: ASK ( datasetClause )* whereClause
            {
            match(input,ASK,FOLLOW_ASK_in_askQuery270); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:123:11: ( datasetClause )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==FROM) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:123:11: datasetClause
            	    {
            	    pushFollow(FOLLOW_datasetClause_in_askQuery272);
            	    datasetClause();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            pushFollow(FOLLOW_whereClause_in_askQuery275);
            whereClause();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "askQuery"


    // $ANTLR start "datasetClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:126:1: datasetClause : FROM ( defaultGraphClause | namedGraphClause ) ;
    public final void datasetClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:127:5: ( FROM ( defaultGraphClause | namedGraphClause ) )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:127:7: FROM ( defaultGraphClause | namedGraphClause )
            {
            match(input,FROM,FOLLOW_FROM_in_datasetClause292); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:127:12: ( defaultGraphClause | namedGraphClause )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==IRI_REF||LA14_0==PNAME_NS||LA14_0==PNAME_LN) ) {
                alt14=1;
            }
            else if ( (LA14_0==NAMED) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:127:14: defaultGraphClause
                    {
                    pushFollow(FOLLOW_defaultGraphClause_in_datasetClause296);
                    defaultGraphClause();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:127:35: namedGraphClause
                    {
                    pushFollow(FOLLOW_namedGraphClause_in_datasetClause300);
                    namedGraphClause();

                    state._fsp--;


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "datasetClause"


    // $ANTLR start "defaultGraphClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:130:1: defaultGraphClause : sourceSelector ;
    public final void defaultGraphClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:131:5: ( sourceSelector )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:131:7: sourceSelector
            {
            pushFollow(FOLLOW_sourceSelector_in_defaultGraphClause319);
            sourceSelector();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "defaultGraphClause"


    // $ANTLR start "namedGraphClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:134:1: namedGraphClause : NAMED sourceSelector ;
    public final void namedGraphClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:135:5: ( NAMED sourceSelector )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:135:7: NAMED sourceSelector
            {
            match(input,NAMED,FOLLOW_NAMED_in_namedGraphClause336); 
            pushFollow(FOLLOW_sourceSelector_in_namedGraphClause338);
            sourceSelector();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "namedGraphClause"


    // $ANTLR start "sourceSelector"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:138:1: sourceSelector : iriRef ;
    public final void sourceSelector() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:139:5: ( iriRef )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:139:7: iriRef
            {
            pushFollow(FOLLOW_iriRef_in_sourceSelector355);
            iriRef();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "sourceSelector"


    // $ANTLR start "whereClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:142:1: whereClause : ( WHERE )? groupGraphPattern ;
    public final void whereClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:143:5: ( ( WHERE )? groupGraphPattern )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:143:7: ( WHERE )? groupGraphPattern
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:143:7: ( WHERE )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==WHERE) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:143:7: WHERE
                    {
                    match(input,WHERE,FOLLOW_WHERE_in_whereClause372); 

                    }
                    break;

            }

            pushFollow(FOLLOW_groupGraphPattern_in_whereClause375);
            groupGraphPattern();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "whereClause"


    // $ANTLR start "solutionModifier"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:146:1: solutionModifier : ( orderClause )? ( limitOffsetClauses )? ;
    public final void solutionModifier() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:5: ( ( orderClause )? ( limitOffsetClauses )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:7: ( orderClause )? ( limitOffsetClauses )?
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:7: ( orderClause )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==ORDER) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:7: orderClause
                    {
                    pushFollow(FOLLOW_orderClause_in_solutionModifier392);
                    orderClause();

                    state._fsp--;


                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:20: ( limitOffsetClauses )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==LIMIT||LA17_0==OFFSET) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:147:20: limitOffsetClauses
                    {
                    pushFollow(FOLLOW_limitOffsetClauses_in_solutionModifier395);
                    limitOffsetClauses();

                    state._fsp--;


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "solutionModifier"


    // $ANTLR start "limitOffsetClauses"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:150:1: limitOffsetClauses : ( limitClause ( offsetClause )? | offsetClause ( limitClause )? ) ;
    public final void limitOffsetClauses() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:5: ( ( limitClause ( offsetClause )? | offsetClause ( limitClause )? ) )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:7: ( limitClause ( offsetClause )? | offsetClause ( limitClause )? )
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:7: ( limitClause ( offsetClause )? | offsetClause ( limitClause )? )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==LIMIT) ) {
                alt20=1;
            }
            else if ( (LA20_0==OFFSET) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:9: limitClause ( offsetClause )?
                    {
                    pushFollow(FOLLOW_limitClause_in_limitOffsetClauses415);
                    limitClause();

                    state._fsp--;

                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:21: ( offsetClause )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==OFFSET) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:21: offsetClause
                            {
                            pushFollow(FOLLOW_offsetClause_in_limitOffsetClauses417);
                            offsetClause();

                            state._fsp--;


                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:37: offsetClause ( limitClause )?
                    {
                    pushFollow(FOLLOW_offsetClause_in_limitOffsetClauses422);
                    offsetClause();

                    state._fsp--;

                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:50: ( limitClause )?
                    int alt19=2;
                    int LA19_0 = input.LA(1);

                    if ( (LA19_0==LIMIT) ) {
                        alt19=1;
                    }
                    switch (alt19) {
                        case 1 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:151:50: limitClause
                            {
                            pushFollow(FOLLOW_limitClause_in_limitOffsetClauses424);
                            limitClause();

                            state._fsp--;


                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "limitOffsetClauses"


    // $ANTLR start "orderClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:154:1: orderClause : ORDER BY ( orderCondition )+ ;
    public final void orderClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:155:5: ( ORDER BY ( orderCondition )+ )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:155:7: ORDER BY ( orderCondition )+
            {
            match(input,ORDER,FOLLOW_ORDER_in_orderClause444); 
            match(input,BY,FOLLOW_BY_in_orderClause446); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:155:16: ( orderCondition )+
            int cnt21=0;
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==IRI_REF||LA21_0==PNAME_NS||(LA21_0>=ASC && LA21_0<=DESC)||LA21_0==OPEN_BRACE||(LA21_0>=VAR1 && LA21_0<=VAR2)||(LA21_0>=STR && LA21_0<=REGEX)||LA21_0==PNAME_LN) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:155:16: orderCondition
            	    {
            	    pushFollow(FOLLOW_orderCondition_in_orderClause448);
            	    orderCondition();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    if ( cnt21 >= 1 ) break loop21;
                        EarlyExitException eee =
                            new EarlyExitException(21, input);
                        throw eee;
                }
                cnt21++;
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "orderClause"


    // $ANTLR start "orderCondition"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:158:1: orderCondition : ( ( ( ASC | DESC ) brackettedExpression ) | ( constraint | var ) );
    public final void orderCondition() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:159:5: ( ( ( ASC | DESC ) brackettedExpression ) | ( constraint | var ) )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=ASC && LA23_0<=DESC)) ) {
                alt23=1;
            }
            else if ( (LA23_0==IRI_REF||LA23_0==PNAME_NS||LA23_0==OPEN_BRACE||(LA23_0>=VAR1 && LA23_0<=VAR2)||(LA23_0>=STR && LA23_0<=REGEX)||LA23_0==PNAME_LN) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:159:7: ( ( ASC | DESC ) brackettedExpression )
                    {
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:159:7: ( ( ASC | DESC ) brackettedExpression )
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:159:9: ( ASC | DESC ) brackettedExpression
                    {
                    if ( (input.LA(1)>=ASC && input.LA(1)<=DESC) ) {
                        input.consume();
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }

                    pushFollow(FOLLOW_brackettedExpression_in_orderCondition478);
                    brackettedExpression();

                    state._fsp--;


                    }


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:160:7: ( constraint | var )
                    {
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:160:7: ( constraint | var )
                    int alt22=2;
                    int LA22_0 = input.LA(1);

                    if ( (LA22_0==IRI_REF||LA22_0==PNAME_NS||LA22_0==OPEN_BRACE||(LA22_0>=STR && LA22_0<=REGEX)||LA22_0==PNAME_LN) ) {
                        alt22=1;
                    }
                    else if ( ((LA22_0>=VAR1 && LA22_0<=VAR2)) ) {
                        alt22=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 22, 0, input);

                        throw nvae;
                    }
                    switch (alt22) {
                        case 1 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:160:9: constraint
                            {
                            pushFollow(FOLLOW_constraint_in_orderCondition490);
                            constraint();

                            state._fsp--;


                            }
                            break;
                        case 2 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:160:22: var
                            {
                            pushFollow(FOLLOW_var_in_orderCondition494);
                            var();

                            state._fsp--;


                            }
                            break;

                    }


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "orderCondition"


    // $ANTLR start "limitClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:163:1: limitClause : LIMIT INTEGER ;
    public final void limitClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:164:5: ( LIMIT INTEGER )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:164:7: LIMIT INTEGER
            {
            match(input,LIMIT,FOLLOW_LIMIT_in_limitClause513); 
            match(input,INTEGER,FOLLOW_INTEGER_in_limitClause515); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "limitClause"


    // $ANTLR start "offsetClause"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:167:1: offsetClause : OFFSET INTEGER ;
    public final void offsetClause() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:168:5: ( OFFSET INTEGER )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:168:7: OFFSET INTEGER
            {
            match(input,OFFSET,FOLLOW_OFFSET_in_offsetClause532); 
            match(input,INTEGER,FOLLOW_INTEGER_in_offsetClause534); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "offsetClause"


    // $ANTLR start "groupGraphPattern"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:172:1: groupGraphPattern : OPEN_CURLY_BRACE ( triplesBlock )? ( ( graphPatternNotTriples | filter ) ( DOT )? ( triplesBlock )? )* CLOSE_CURLY_BRACE ;
    public final void groupGraphPattern() throws RecognitionException {
        myState = MYSTATE_NONE;
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:175:5: ( OPEN_CURLY_BRACE ( triplesBlock )? ( ( graphPatternNotTriples | filter ) ( DOT )? ( triplesBlock )? )* CLOSE_CURLY_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:175:7: OPEN_CURLY_BRACE ( triplesBlock )? ( ( graphPatternNotTriples | filter ) ( DOT )? ( triplesBlock )? )* CLOSE_CURLY_BRACE
            {
            match(input,OPEN_CURLY_BRACE,FOLLOW_OPEN_CURLY_BRACE_in_groupGraphPattern569); 
            myState = MYSTATE_SUBJECT;
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:177:7: ( triplesBlock )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==IRI_REF||LA24_0==PNAME_NS||LA24_0==INTEGER||LA24_0==OPEN_BRACE||LA24_0==OPEN_SQUARE_BRACE||(LA24_0>=VAR1 && LA24_0<=VAR2)||(LA24_0>=DECIMAL && LA24_0<=BLANK_NODE_LABEL)) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:177:7: triplesBlock
                    {
                    pushFollow(FOLLOW_triplesBlock_in_groupGraphPattern586);
                    triplesBlock();

                    state._fsp--;


                    }
                    break;

            }

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:7: ( ( graphPatternNotTriples | filter ) ( DOT )? ( triplesBlock )? )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==OPEN_CURLY_BRACE||(LA28_0>=OPTIONAL && LA28_0<=GRAPH)||LA28_0==FILTER) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:9: ( graphPatternNotTriples | filter ) ( DOT )? ( triplesBlock )?
            	    {
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:9: ( graphPatternNotTriples | filter )
            	    int alt25=2;
            	    int LA25_0 = input.LA(1);

            	    if ( (LA25_0==OPEN_CURLY_BRACE||(LA25_0>=OPTIONAL && LA25_0<=GRAPH)) ) {
            	        alt25=1;
            	    }
            	    else if ( (LA25_0==FILTER) ) {
            	        alt25=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 25, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt25) {
            	        case 1 :
            	            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:11: graphPatternNotTriples
            	            {
            	            pushFollow(FOLLOW_graphPatternNotTriples_in_groupGraphPattern599);
            	            graphPatternNotTriples();

            	            state._fsp--;


            	            }
            	            break;
            	        case 2 :
            	            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:36: filter
            	            {
            	            pushFollow(FOLLOW_filter_in_groupGraphPattern603);
            	            filter();

            	            state._fsp--;


            	            }
            	            break;

            	    }

            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:45: ( DOT )?
            	    int alt26=2;
            	    int LA26_0 = input.LA(1);

            	    if ( (LA26_0==DOT) ) {
            	        alt26=1;
            	    }
            	    switch (alt26) {
            	        case 1 :
            	            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:46: DOT
            	            {
            	            match(input,DOT,FOLLOW_DOT_in_groupGraphPattern608); 
            	            myState = MYSTATE_SUBJECT;

            	            }
            	            break;

            	    }

            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:81: ( triplesBlock )?
            	    int alt27=2;
            	    int LA27_0 = input.LA(1);

            	    if ( (LA27_0==IRI_REF||LA27_0==PNAME_NS||LA27_0==INTEGER||LA27_0==OPEN_BRACE||LA27_0==OPEN_SQUARE_BRACE||(LA27_0>=VAR1 && LA27_0<=VAR2)||(LA27_0>=DECIMAL && LA27_0<=BLANK_NODE_LABEL)) ) {
            	        alt27=1;
            	    }
            	    switch (alt27) {
            	        case 1 :
            	            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:178:81: triplesBlock
            	            {
            	            pushFollow(FOLLOW_triplesBlock_in_groupGraphPattern614);
            	            triplesBlock();

            	            state._fsp--;


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            match(input,CLOSE_CURLY_BRACE,FOLLOW_CLOSE_CURLY_BRACE_in_groupGraphPattern626); 

            }

            myState = MYSTATE_NONE;
        }
        catch (RecognitionException re) {

                  reportError(re);
                  consumeUntil(input, EOF);
                  input.consume();
                
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "groupGraphPattern"


    // $ANTLR start "triplesBlock"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:188:1: triplesBlock : triplesSameSubject ( DOT ( triplesBlock )? )? ;
    public final void triplesBlock() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:5: ( triplesSameSubject ( DOT ( triplesBlock )? )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:7: triplesSameSubject ( DOT ( triplesBlock )? )?
            {
            pushFollow(FOLLOW_triplesSameSubject_in_triplesBlock658);
            triplesSameSubject();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:26: ( DOT ( triplesBlock )? )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==DOT) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:28: DOT ( triplesBlock )?
                    {
                    match(input,DOT,FOLLOW_DOT_in_triplesBlock662); 
                    myState = MYSTATE_SUBJECT;
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:61: ( triplesBlock )?
                    int alt29=2;
                    int LA29_0 = input.LA(1);

                    if ( (LA29_0==IRI_REF||LA29_0==PNAME_NS||LA29_0==INTEGER||LA29_0==OPEN_BRACE||LA29_0==OPEN_SQUARE_BRACE||(LA29_0>=VAR1 && LA29_0<=VAR2)||(LA29_0>=DECIMAL && LA29_0<=BLANK_NODE_LABEL)) ) {
                        alt29=1;
                    }
                    switch (alt29) {
                        case 1 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:189:61: triplesBlock
                            {
                            pushFollow(FOLLOW_triplesBlock_in_triplesBlock666);
                            triplesBlock();

                            state._fsp--;


                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }
        catch (RecognitionException re) {

                  reportError(re);
                  consumeUntil(input, EOF);
                  input.consume();
                
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "triplesBlock"


    // $ANTLR start "graphPatternNotTriples"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:197:1: graphPatternNotTriples : ( optionalGraphPattern | groupOrUnionGraphPattern | graphGraphPattern );
    public final void graphPatternNotTriples() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:198:5: ( optionalGraphPattern | groupOrUnionGraphPattern | graphGraphPattern )
            int alt31=3;
            switch ( input.LA(1) ) {
            case OPTIONAL:
                {
                alt31=1;
                }
                break;
            case OPEN_CURLY_BRACE:
                {
                alt31=2;
                }
                break;
            case GRAPH:
                {
                alt31=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:198:7: optionalGraphPattern
                    {
                    pushFollow(FOLLOW_optionalGraphPattern_in_graphPatternNotTriples697);
                    optionalGraphPattern();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:198:30: groupOrUnionGraphPattern
                    {
                    pushFollow(FOLLOW_groupOrUnionGraphPattern_in_graphPatternNotTriples701);
                    groupOrUnionGraphPattern();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:198:57: graphGraphPattern
                    {
                    pushFollow(FOLLOW_graphGraphPattern_in_graphPatternNotTriples705);
                    graphGraphPattern();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "graphPatternNotTriples"


    // $ANTLR start "optionalGraphPattern"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:201:1: optionalGraphPattern : OPTIONAL groupGraphPattern ;
    public final void optionalGraphPattern() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:202:5: ( OPTIONAL groupGraphPattern )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:202:7: OPTIONAL groupGraphPattern
            {
            match(input,OPTIONAL,FOLLOW_OPTIONAL_in_optionalGraphPattern722); 
            pushFollow(FOLLOW_groupGraphPattern_in_optionalGraphPattern724);
            groupGraphPattern();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "optionalGraphPattern"


    // $ANTLR start "graphGraphPattern"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:205:1: graphGraphPattern : GRAPH varOrIRIref groupGraphPattern ;
    public final void graphGraphPattern() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:206:5: ( GRAPH varOrIRIref groupGraphPattern )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:206:7: GRAPH varOrIRIref groupGraphPattern
            {
            match(input,GRAPH,FOLLOW_GRAPH_in_graphGraphPattern741); 
            pushFollow(FOLLOW_varOrIRIref_in_graphGraphPattern743);
            varOrIRIref();

            state._fsp--;

            pushFollow(FOLLOW_groupGraphPattern_in_graphGraphPattern745);
            groupGraphPattern();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "graphGraphPattern"


    // $ANTLR start "groupOrUnionGraphPattern"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:209:1: groupOrUnionGraphPattern : groupGraphPattern ( UNION groupGraphPattern )* ;
    public final void groupOrUnionGraphPattern() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:210:5: ( groupGraphPattern ( UNION groupGraphPattern )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:210:7: groupGraphPattern ( UNION groupGraphPattern )*
            {
            pushFollow(FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern762);
            groupGraphPattern();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:210:25: ( UNION groupGraphPattern )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==UNION) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:210:27: UNION groupGraphPattern
            	    {
            	    match(input,UNION,FOLLOW_UNION_in_groupOrUnionGraphPattern766); 
            	    pushFollow(FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern768);
            	    groupGraphPattern();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "groupOrUnionGraphPattern"


    // $ANTLR start "filter"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:213:1: filter : FILTER constraint ;
    public final void filter() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:214:5: ( FILTER constraint )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:214:7: FILTER constraint
            {
            match(input,FILTER,FOLLOW_FILTER_in_filter788); 
            pushFollow(FOLLOW_constraint_in_filter790);
            constraint();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "filter"


    // $ANTLR start "constraint"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:217:1: constraint : ( brackettedExpression | builtInCall | functionCall );
    public final void constraint() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:218:5: ( brackettedExpression | builtInCall | functionCall )
            int alt33=3;
            switch ( input.LA(1) ) {
            case OPEN_BRACE:
                {
                alt33=1;
                }
                break;
            case STR:
            case LANG:
            case LANGMATCHES:
            case DATATYPE:
            case BOUND:
            case SAMETERM:
            case ISIRI:
            case ISURI:
            case ISBLANK:
            case ISLITERAL:
            case REGEX:
                {
                alt33=2;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt33=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }

            switch (alt33) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:218:7: brackettedExpression
                    {
                    pushFollow(FOLLOW_brackettedExpression_in_constraint807);
                    brackettedExpression();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:218:30: builtInCall
                    {
                    pushFollow(FOLLOW_builtInCall_in_constraint811);
                    builtInCall();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:218:44: functionCall
                    {
                    pushFollow(FOLLOW_functionCall_in_constraint815);
                    functionCall();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "constraint"


    // $ANTLR start "functionCall"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:221:1: functionCall : iriRef argList ;
    public final void functionCall() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:222:5: ( iriRef argList )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:222:7: iriRef argList
            {
            pushFollow(FOLLOW_iriRef_in_functionCall832);
            iriRef();

            state._fsp--;

            pushFollow(FOLLOW_argList_in_functionCall834);
            argList();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "functionCall"


    // $ANTLR start "argList"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:225:1: argList : ( OPEN_BRACE CLOSE_BRACE | OPEN_BRACE expression ( COMMA expression )* CLOSE_BRACE ) ;
    public final void argList() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:5: ( ( OPEN_BRACE CLOSE_BRACE | OPEN_BRACE expression ( COMMA expression )* CLOSE_BRACE ) )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:7: ( OPEN_BRACE CLOSE_BRACE | OPEN_BRACE expression ( COMMA expression )* CLOSE_BRACE )
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:7: ( OPEN_BRACE CLOSE_BRACE | OPEN_BRACE expression ( COMMA expression )* CLOSE_BRACE )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==OPEN_BRACE) ) {
                int LA35_1 = input.LA(2);

                if ( (LA35_1==CLOSE_BRACE) ) {
                    alt35=1;
                }
                else if ( (LA35_1==IRI_REF||LA35_1==PNAME_NS||LA35_1==INTEGER||LA35_1==OPEN_BRACE||(LA35_1>=VAR1 && LA35_1<=VAR2)||(LA35_1>=PLUS && LA35_1<=MINUS)||(LA35_1>=NOT && LA35_1<=REGEX)||(LA35_1>=DECIMAL && LA35_1<=PNAME_LN)) ) {
                    alt35=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 35, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:9: OPEN_BRACE CLOSE_BRACE
                    {
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_argList853); 
                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_argList855); 

                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:34: OPEN_BRACE expression ( COMMA expression )* CLOSE_BRACE
                    {
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_argList859); 
                    pushFollow(FOLLOW_expression_in_argList861);
                    expression();

                    state._fsp--;

                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:56: ( COMMA expression )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==COMMA) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:226:58: COMMA expression
                    	    {
                    	    match(input,COMMA,FOLLOW_COMMA_in_argList865); 
                    	    pushFollow(FOLLOW_expression_in_argList867);
                    	    expression();

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_argList872); 

                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "argList"


    // $ANTLR start "constructTemplate"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:229:1: constructTemplate : OPEN_CURLY_BRACE ( constructTriples )? CLOSE_CURLY_BRACE ;
    public final void constructTemplate() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:230:5: ( OPEN_CURLY_BRACE ( constructTriples )? CLOSE_CURLY_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:230:7: OPEN_CURLY_BRACE ( constructTriples )? CLOSE_CURLY_BRACE
            {
            match(input,OPEN_CURLY_BRACE,FOLLOW_OPEN_CURLY_BRACE_in_constructTemplate891); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:230:24: ( constructTriples )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==IRI_REF||LA36_0==PNAME_NS||LA36_0==INTEGER||LA36_0==OPEN_BRACE||LA36_0==OPEN_SQUARE_BRACE||(LA36_0>=VAR1 && LA36_0<=VAR2)||(LA36_0>=DECIMAL && LA36_0<=BLANK_NODE_LABEL)) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:230:24: constructTriples
                    {
                    pushFollow(FOLLOW_constructTriples_in_constructTemplate893);
                    constructTriples();

                    state._fsp--;


                    }
                    break;

            }

            match(input,CLOSE_CURLY_BRACE,FOLLOW_CLOSE_CURLY_BRACE_in_constructTemplate896); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "constructTemplate"


    // $ANTLR start "constructTriples"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:233:1: constructTriples : triplesSameSubject ( DOT ( constructTriples )? )? ;
    public final void constructTriples() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:5: ( triplesSameSubject ( DOT ( constructTriples )? )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:7: triplesSameSubject ( DOT ( constructTriples )? )?
            {
            pushFollow(FOLLOW_triplesSameSubject_in_constructTriples913);
            triplesSameSubject();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:26: ( DOT ( constructTriples )? )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==DOT) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:28: DOT ( constructTriples )?
                    {
                    match(input,DOT,FOLLOW_DOT_in_constructTriples917); 
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:32: ( constructTriples )?
                    int alt37=2;
                    int LA37_0 = input.LA(1);

                    if ( (LA37_0==IRI_REF||LA37_0==PNAME_NS||LA37_0==INTEGER||LA37_0==OPEN_BRACE||LA37_0==OPEN_SQUARE_BRACE||(LA37_0>=VAR1 && LA37_0<=VAR2)||(LA37_0>=DECIMAL && LA37_0<=BLANK_NODE_LABEL)) ) {
                        alt37=1;
                    }
                    switch (alt37) {
                        case 1 :
                            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:234:32: constructTriples
                            {
                            pushFollow(FOLLOW_constructTriples_in_constructTriples919);
                            constructTriples();

                            state._fsp--;


                            }
                            break;

                    }


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "constructTriples"


    // $ANTLR start "triplesSameSubject"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:238:1: triplesSameSubject : ( varOrTerm propertyListNotEmpty | triplesNode propertyList );
    public final void triplesSameSubject() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:239:5: ( varOrTerm propertyListNotEmpty | triplesNode propertyList )
            int alt39=2;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case INTEGER:
            case VAR1:
            case VAR2:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
            case TRUE:
            case FALSE:
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case STRING_LITERAL_LONG1:
            case STRING_LITERAL_LONG2:
            case PNAME_LN:
            case BLANK_NODE_LABEL:
                {
                alt39=1;
                }
                break;
            case OPEN_SQUARE_BRACE:
                {
                int LA39_2 = input.LA(2);

                if ( (LA39_2==CLOSE_SQUARE_BRACE) ) {
                    alt39=1;
                }
                else if ( (LA39_2==IRI_REF||LA39_2==PNAME_NS||LA39_2==A||(LA39_2>=VAR1 && LA39_2<=VAR2)||LA39_2==PNAME_LN) ) {
                    alt39=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 39, 2, input);

                    throw nvae;
                }
                }
                break;
            case OPEN_BRACE:
                {
                int LA39_3 = input.LA(2);

                if ( (LA39_3==CLOSE_BRACE) ) {
                    alt39=1;
                }
                else if ( (LA39_3==IRI_REF||LA39_3==PNAME_NS||LA39_3==INTEGER||LA39_3==OPEN_BRACE||LA39_3==OPEN_SQUARE_BRACE||(LA39_3>=VAR1 && LA39_3<=VAR2)||(LA39_3>=DECIMAL && LA39_3<=BLANK_NODE_LABEL)) ) {
                    alt39=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 39, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:240:7: varOrTerm propertyListNotEmpty
                    {
                    pushFollow(FOLLOW_varOrTerm_in_triplesSameSubject948);
                    varOrTerm();

                    state._fsp--;

                    myState = MYSTATE_PREDICATE;
                    pushFollow(FOLLOW_propertyListNotEmpty_in_triplesSameSubject964);
                    propertyListNotEmpty();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:244:9: triplesNode propertyList
                    {
                    pushFollow(FOLLOW_triplesNode_in_triplesSameSubject983);
                    triplesNode();

                    state._fsp--;

                    myState = MYSTATE_PREDICATE;
                    pushFollow(FOLLOW_propertyList_in_triplesSameSubject1003);
                    propertyList();

                    state._fsp--;


                    }
                    break;

            }
        }
        catch (RecognitionException re) {

                  reportError(re);
                  consumeUntil(input, EOF);
                  input.consume();
                
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "triplesSameSubject"


    // $ANTLR start "propertyListNotEmpty"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:255:1: propertyListNotEmpty : verb objectList ( SEMICOLON ( verb objectList )? )* ;
    public final void propertyListNotEmpty() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:256:5: ( verb objectList ( SEMICOLON ( verb objectList )? )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:257:7: verb objectList ( SEMICOLON ( verb objectList )? )*
            {
            pushFollow(FOLLOW_verb_in_propertyListNotEmpty1038);
            verb();

            state._fsp--;

            myState = MYSTATE_OBJECT;
            pushFollow(FOLLOW_objectList_in_propertyListNotEmpty1054);
            objectList();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:261:7: ( SEMICOLON ( verb objectList )? )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==SEMICOLON) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:261:9: SEMICOLON ( verb objectList )?
            	    {
            	    match(input,SEMICOLON,FOLLOW_SEMICOLON_in_propertyListNotEmpty1071); 
            	    myState = MYSTATE_PREDICATE;
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:263:9: ( verb objectList )?
            	    int alt40=2;
            	    int LA40_0 = input.LA(1);

            	    if ( (LA40_0==IRI_REF||LA40_0==PNAME_NS||LA40_0==A||(LA40_0>=VAR1 && LA40_0<=VAR2)||LA40_0==PNAME_LN) ) {
            	        alt40=1;
            	    }
            	    switch (alt40) {
            	        case 1 :
            	            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:264:11: verb objectList
            	            {
            	            pushFollow(FOLLOW_verb_in_propertyListNotEmpty1104);
            	            verb();

            	            state._fsp--;

            	            myState = MYSTATE_OBJECT;
            	            pushFollow(FOLLOW_objectList_in_propertyListNotEmpty1128);
            	            objectList();

            	            state._fsp--;


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {

                  reportError(re);
                  consumeUntil(input, EOF);
                  input.consume();
                
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "propertyListNotEmpty"


    // $ANTLR start "propertyList"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:277:1: propertyList : ( propertyListNotEmpty )? ;
    public final void propertyList() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:278:5: ( ( propertyListNotEmpty )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:278:7: ( propertyListNotEmpty )?
            {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:278:7: ( propertyListNotEmpty )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==IRI_REF||LA42_0==PNAME_NS||LA42_0==A||(LA42_0>=VAR1 && LA42_0<=VAR2)||LA42_0==PNAME_LN) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:278:7: propertyListNotEmpty
                    {
                    pushFollow(FOLLOW_propertyListNotEmpty_in_propertyList1176);
                    propertyListNotEmpty();

                    state._fsp--;


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "propertyList"


    // $ANTLR start "objectList"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:282:1: objectList : object ( COMMA object )* ;
    public final void objectList() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:283:5: ( object ( COMMA object )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:284:7: object ( COMMA object )*
            {
            pushFollow(FOLLOW_object_in_objectList1202);
            object();

            state._fsp--;

            myState = MYSTATE_NONE;
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:286:7: ( COMMA object )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==COMMA) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:286:9: COMMA object
            	    {
            	    match(input,COMMA,FOLLOW_COMMA_in_objectList1220); 
            	    myState = MYSTATE_OBJECT;
            	    pushFollow(FOLLOW_object_in_objectList1244);
            	    object();

            	    state._fsp--;

            	    myState = MYSTATE_NONE;

            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {

                  reportError(re);
                  consumeUntil(input, EOF);
                  input.consume();
                
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "objectList"


    // $ANTLR start "object"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:298:1: object : graphNode ;
    public final void object() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:299:5: ( graphNode )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:299:7: graphNode
            {
            pushFollow(FOLLOW_graphNode_in_object1298);
            graphNode();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "object"


    // $ANTLR start "verb"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:302:1: verb : ( varOrIRIref | A );
    public final void verb() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:303:5: ( varOrIRIref | A )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==IRI_REF||LA44_0==PNAME_NS||(LA44_0>=VAR1 && LA44_0<=VAR2)||LA44_0==PNAME_LN) ) {
                alt44=1;
            }
            else if ( (LA44_0==A) ) {
                alt44=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:303:7: varOrIRIref
                    {
                    pushFollow(FOLLOW_varOrIRIref_in_verb1315);
                    varOrIRIref();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:304:7: A
                    {
                    match(input,A,FOLLOW_A_in_verb1323); 

                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "verb"


    // $ANTLR start "triplesNode"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:307:1: triplesNode : ( collection | blankNodePropertyList );
    public final void triplesNode() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:308:5: ( collection | blankNodePropertyList )
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==OPEN_BRACE) ) {
                alt45=1;
            }
            else if ( (LA45_0==OPEN_SQUARE_BRACE) ) {
                alt45=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }
            switch (alt45) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:308:7: collection
                    {
                    pushFollow(FOLLOW_collection_in_triplesNode1340);
                    collection();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:309:7: blankNodePropertyList
                    {
                    pushFollow(FOLLOW_blankNodePropertyList_in_triplesNode1348);
                    blankNodePropertyList();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "triplesNode"


    // $ANTLR start "blankNodePropertyList"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:312:1: blankNodePropertyList : OPEN_SQUARE_BRACE propertyListNotEmpty CLOSE_SQUARE_BRACE ;
    public final void blankNodePropertyList() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:313:5: ( OPEN_SQUARE_BRACE propertyListNotEmpty CLOSE_SQUARE_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:313:7: OPEN_SQUARE_BRACE propertyListNotEmpty CLOSE_SQUARE_BRACE
            {
            match(input,OPEN_SQUARE_BRACE,FOLLOW_OPEN_SQUARE_BRACE_in_blankNodePropertyList1365); 
            pushFollow(FOLLOW_propertyListNotEmpty_in_blankNodePropertyList1367);
            propertyListNotEmpty();

            state._fsp--;

            match(input,CLOSE_SQUARE_BRACE,FOLLOW_CLOSE_SQUARE_BRACE_in_blankNodePropertyList1369); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "blankNodePropertyList"


    // $ANTLR start "collection"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:316:1: collection : OPEN_BRACE ( graphNode )+ CLOSE_BRACE ;
    public final void collection() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:317:5: ( OPEN_BRACE ( graphNode )+ CLOSE_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:317:7: OPEN_BRACE ( graphNode )+ CLOSE_BRACE
            {
            match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_collection1386); 
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:317:18: ( graphNode )+
            int cnt46=0;
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==IRI_REF||LA46_0==PNAME_NS||LA46_0==INTEGER||LA46_0==OPEN_BRACE||LA46_0==OPEN_SQUARE_BRACE||(LA46_0>=VAR1 && LA46_0<=VAR2)||(LA46_0>=DECIMAL && LA46_0<=BLANK_NODE_LABEL)) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:317:18: graphNode
            	    {
            	    pushFollow(FOLLOW_graphNode_in_collection1388);
            	    graphNode();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    if ( cnt46 >= 1 ) break loop46;
                        EarlyExitException eee =
                            new EarlyExitException(46, input);
                        throw eee;
                }
                cnt46++;
            } while (true);

            match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_collection1391); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "collection"


    // $ANTLR start "graphNode"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:320:1: graphNode : ( varOrTerm | triplesNode );
    public final void graphNode() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:321:5: ( varOrTerm | triplesNode )
            int alt47=2;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case INTEGER:
            case VAR1:
            case VAR2:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
            case TRUE:
            case FALSE:
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case STRING_LITERAL_LONG1:
            case STRING_LITERAL_LONG2:
            case PNAME_LN:
            case BLANK_NODE_LABEL:
                {
                alt47=1;
                }
                break;
            case OPEN_SQUARE_BRACE:
                {
                int LA47_2 = input.LA(2);

                if ( (LA47_2==CLOSE_SQUARE_BRACE) ) {
                    alt47=1;
                }
                else if ( (LA47_2==IRI_REF||LA47_2==PNAME_NS||LA47_2==A||(LA47_2>=VAR1 && LA47_2<=VAR2)||LA47_2==PNAME_LN) ) {
                    alt47=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 2, input);

                    throw nvae;
                }
                }
                break;
            case OPEN_BRACE:
                {
                int LA47_3 = input.LA(2);

                if ( (LA47_3==CLOSE_BRACE) ) {
                    alt47=1;
                }
                else if ( (LA47_3==IRI_REF||LA47_3==PNAME_NS||LA47_3==INTEGER||LA47_3==OPEN_BRACE||LA47_3==OPEN_SQUARE_BRACE||(LA47_3>=VAR1 && LA47_3<=VAR2)||(LA47_3>=DECIMAL && LA47_3<=BLANK_NODE_LABEL)) ) {
                    alt47=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 3, input);

                    throw nvae;
                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }

            switch (alt47) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:321:7: varOrTerm
                    {
                    pushFollow(FOLLOW_varOrTerm_in_graphNode1408);
                    varOrTerm();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:321:19: triplesNode
                    {
                    pushFollow(FOLLOW_triplesNode_in_graphNode1412);
                    triplesNode();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "graphNode"


    // $ANTLR start "varOrTerm"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:324:1: varOrTerm : ( var | graphTerm );
    public final void varOrTerm() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:325:5: ( var | graphTerm )
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( ((LA48_0>=VAR1 && LA48_0<=VAR2)) ) {
                alt48=1;
            }
            else if ( (LA48_0==IRI_REF||LA48_0==PNAME_NS||LA48_0==INTEGER||LA48_0==OPEN_BRACE||LA48_0==OPEN_SQUARE_BRACE||(LA48_0>=DECIMAL && LA48_0<=BLANK_NODE_LABEL)) ) {
                alt48=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;
            }
            switch (alt48) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:325:7: var
                    {
                    pushFollow(FOLLOW_var_in_varOrTerm1429);
                    var();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:326:7: graphTerm
                    {
                    pushFollow(FOLLOW_graphTerm_in_varOrTerm1437);
                    graphTerm();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "varOrTerm"


    // $ANTLR start "varOrIRIref"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:329:1: varOrIRIref : ( var | iriRef );
    public final void varOrIRIref() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:330:5: ( var | iriRef )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( ((LA49_0>=VAR1 && LA49_0<=VAR2)) ) {
                alt49=1;
            }
            else if ( (LA49_0==IRI_REF||LA49_0==PNAME_NS||LA49_0==PNAME_LN) ) {
                alt49=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:330:7: var
                    {
                    pushFollow(FOLLOW_var_in_varOrIRIref1454);
                    var();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:330:13: iriRef
                    {
                    pushFollow(FOLLOW_iriRef_in_varOrIRIref1458);
                    iriRef();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "varOrIRIref"


    // $ANTLR start "var"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:333:1: var : ( VAR1 | VAR2 );
    public final void var() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:334:5: ( VAR1 | VAR2 )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( (input.LA(1)>=VAR1 && input.LA(1)<=VAR2) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "var"


    // $ANTLR start "graphTerm"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:338:1: graphTerm : ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | blankNode | OPEN_BRACE CLOSE_BRACE );
    public final void graphTerm() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:339:5: ( iriRef | rdfLiteral | numericLiteral | booleanLiteral | blankNode | OPEN_BRACE CLOSE_BRACE )
            int alt50=6;
            switch ( input.LA(1) ) {
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt50=1;
                }
                break;
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case STRING_LITERAL_LONG1:
            case STRING_LITERAL_LONG2:
                {
                alt50=2;
                }
                break;
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt50=3;
                }
                break;
            case TRUE:
            case FALSE:
                {
                alt50=4;
                }
                break;
            case OPEN_SQUARE_BRACE:
            case BLANK_NODE_LABEL:
                {
                alt50=5;
                }
                break;
            case OPEN_BRACE:
                {
                alt50=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;
            }

            switch (alt50) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:339:7: iriRef
                    {
                    pushFollow(FOLLOW_iriRef_in_graphTerm1500);
                    iriRef();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:340:7: rdfLiteral
                    {
                    pushFollow(FOLLOW_rdfLiteral_in_graphTerm1508);
                    rdfLiteral();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:341:7: numericLiteral
                    {
                    pushFollow(FOLLOW_numericLiteral_in_graphTerm1516);
                    numericLiteral();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:342:7: booleanLiteral
                    {
                    pushFollow(FOLLOW_booleanLiteral_in_graphTerm1524);
                    booleanLiteral();

                    state._fsp--;


                    }
                    break;
                case 5 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:343:7: blankNode
                    {
                    pushFollow(FOLLOW_blankNode_in_graphTerm1532);
                    blankNode();

                    state._fsp--;


                    }
                    break;
                case 6 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:344:7: OPEN_BRACE CLOSE_BRACE
                    {
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_graphTerm1540); 
                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_graphTerm1542); 

                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "graphTerm"


    // $ANTLR start "expression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:347:1: expression : conditionalOrExpression ;
    public final void expression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:348:5: ( conditionalOrExpression )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:348:7: conditionalOrExpression
            {
            pushFollow(FOLLOW_conditionalOrExpression_in_expression1559);
            conditionalOrExpression();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "expression"


    // $ANTLR start "conditionalOrExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:351:1: conditionalOrExpression : conditionalAndExpression ( OR conditionalAndExpression )* ;
    public final void conditionalOrExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:352:5: ( conditionalAndExpression ( OR conditionalAndExpression )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:352:7: conditionalAndExpression ( OR conditionalAndExpression )*
            {
            pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression1576);
            conditionalAndExpression();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:352:32: ( OR conditionalAndExpression )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( (LA51_0==OR) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:352:34: OR conditionalAndExpression
            	    {
            	    match(input,OR,FOLLOW_OR_in_conditionalOrExpression1580); 
            	    pushFollow(FOLLOW_conditionalAndExpression_in_conditionalOrExpression1582);
            	    conditionalAndExpression();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "conditionalOrExpression"


    // $ANTLR start "conditionalAndExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:355:1: conditionalAndExpression : valueLogical ( AND valueLogical )* ;
    public final void conditionalAndExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:356:5: ( valueLogical ( AND valueLogical )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:356:7: valueLogical ( AND valueLogical )*
            {
            pushFollow(FOLLOW_valueLogical_in_conditionalAndExpression1602);
            valueLogical();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:356:20: ( AND valueLogical )*
            loop52:
            do {
                int alt52=2;
                int LA52_0 = input.LA(1);

                if ( (LA52_0==AND) ) {
                    alt52=1;
                }


                switch (alt52) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:356:22: AND valueLogical
            	    {
            	    match(input,AND,FOLLOW_AND_in_conditionalAndExpression1606); 
            	    pushFollow(FOLLOW_valueLogical_in_conditionalAndExpression1608);
            	    valueLogical();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop52;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "conditionalAndExpression"


    // $ANTLR start "valueLogical"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:359:1: valueLogical : relationalExpression ;
    public final void valueLogical() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:360:5: ( relationalExpression )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:360:7: relationalExpression
            {
            pushFollow(FOLLOW_relationalExpression_in_valueLogical1628);
            relationalExpression();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "valueLogical"


    // $ANTLR start "relationalExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:363:1: relationalExpression : numericExpression ( EQUAL numericExpression | NOT_EQUAL numericExpression | LESS numericExpression | GREATER numericExpression | LESS_EQUAL numericExpression | GREATER_EQUAL numericExpression )? ;
    public final void relationalExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:5: ( numericExpression ( EQUAL numericExpression | NOT_EQUAL numericExpression | LESS numericExpression | GREATER numericExpression | LESS_EQUAL numericExpression | GREATER_EQUAL numericExpression )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:7: numericExpression ( EQUAL numericExpression | NOT_EQUAL numericExpression | LESS numericExpression | GREATER numericExpression | LESS_EQUAL numericExpression | GREATER_EQUAL numericExpression )?
            {
            pushFollow(FOLLOW_numericExpression_in_relationalExpression1645);
            numericExpression();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:25: ( EQUAL numericExpression | NOT_EQUAL numericExpression | LESS numericExpression | GREATER numericExpression | LESS_EQUAL numericExpression | GREATER_EQUAL numericExpression )?
            int alt53=7;
            switch ( input.LA(1) ) {
                case EQUAL:
                    {
                    alt53=1;
                    }
                    break;
                case NOT_EQUAL:
                    {
                    alt53=2;
                    }
                    break;
                case LESS:
                    {
                    alt53=3;
                    }
                    break;
                case GREATER:
                    {
                    alt53=4;
                    }
                    break;
                case LESS_EQUAL:
                    {
                    alt53=5;
                    }
                    break;
                case GREATER_EQUAL:
                    {
                    alt53=6;
                    }
                    break;
            }

            switch (alt53) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:27: EQUAL numericExpression
                    {
                    match(input,EQUAL,FOLLOW_EQUAL_in_relationalExpression1649); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1651);
                    numericExpression();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:53: NOT_EQUAL numericExpression
                    {
                    match(input,NOT_EQUAL,FOLLOW_NOT_EQUAL_in_relationalExpression1655); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1657);
                    numericExpression();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:83: LESS numericExpression
                    {
                    match(input,LESS,FOLLOW_LESS_in_relationalExpression1661); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1663);
                    numericExpression();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:108: GREATER numericExpression
                    {
                    match(input,GREATER,FOLLOW_GREATER_in_relationalExpression1667); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1669);
                    numericExpression();

                    state._fsp--;


                    }
                    break;
                case 5 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:136: LESS_EQUAL numericExpression
                    {
                    match(input,LESS_EQUAL,FOLLOW_LESS_EQUAL_in_relationalExpression1673); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1675);
                    numericExpression();

                    state._fsp--;


                    }
                    break;
                case 6 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:364:167: GREATER_EQUAL numericExpression
                    {
                    match(input,GREATER_EQUAL,FOLLOW_GREATER_EQUAL_in_relationalExpression1679); 
                    pushFollow(FOLLOW_numericExpression_in_relationalExpression1681);
                    numericExpression();

                    state._fsp--;


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "relationalExpression"


    // $ANTLR start "numericExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:367:1: numericExpression : additiveExpression ;
    public final void numericExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:368:5: ( additiveExpression )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:368:7: additiveExpression
            {
            pushFollow(FOLLOW_additiveExpression_in_numericExpression1701);
            additiveExpression();

            state._fsp--;


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "numericExpression"


    // $ANTLR start "additiveExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:371:1: additiveExpression : multiplicativeExpression ( PLUS multiplicativeExpression | MINUS multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )* ;
    public final void additiveExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:5: ( multiplicativeExpression ( PLUS multiplicativeExpression | MINUS multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:7: multiplicativeExpression ( PLUS multiplicativeExpression | MINUS multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )*
            {
            pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression1718);
            multiplicativeExpression();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:32: ( PLUS multiplicativeExpression | MINUS multiplicativeExpression | numericLiteralPositive | numericLiteralNegative )*
            loop54:
            do {
                int alt54=5;
                switch ( input.LA(1) ) {
                case PLUS:
                    {
                    alt54=1;
                    }
                    break;
                case MINUS:
                    {
                    alt54=2;
                    }
                    break;
                case INTEGER_POSITIVE:
                case DECIMAL_POSITIVE:
                case DOUBLE_POSITIVE:
                    {
                    alt54=3;
                    }
                    break;
                case INTEGER_NEGATIVE:
                case DECIMAL_NEGATIVE:
                case DOUBLE_NEGATIVE:
                    {
                    alt54=4;
                    }
                    break;

                }

                switch (alt54) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:34: PLUS multiplicativeExpression
            	    {
            	    match(input,PLUS,FOLLOW_PLUS_in_additiveExpression1722); 
            	    pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression1724);
            	    multiplicativeExpression();

            	    state._fsp--;


            	    }
            	    break;
            	case 2 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:66: MINUS multiplicativeExpression
            	    {
            	    match(input,MINUS,FOLLOW_MINUS_in_additiveExpression1728); 
            	    pushFollow(FOLLOW_multiplicativeExpression_in_additiveExpression1730);
            	    multiplicativeExpression();

            	    state._fsp--;


            	    }
            	    break;
            	case 3 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:99: numericLiteralPositive
            	    {
            	    pushFollow(FOLLOW_numericLiteralPositive_in_additiveExpression1734);
            	    numericLiteralPositive();

            	    state._fsp--;


            	    }
            	    break;
            	case 4 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:372:124: numericLiteralNegative
            	    {
            	    pushFollow(FOLLOW_numericLiteralNegative_in_additiveExpression1738);
            	    numericLiteralNegative();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop54;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "additiveExpression"


    // $ANTLR start "multiplicativeExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:375:1: multiplicativeExpression : unaryExpression ( ASTERISK unaryExpression | DIVIDE unaryExpression )* ;
    public final void multiplicativeExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:376:5: ( unaryExpression ( ASTERISK unaryExpression | DIVIDE unaryExpression )* )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:376:7: unaryExpression ( ASTERISK unaryExpression | DIVIDE unaryExpression )*
            {
            pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression1758);
            unaryExpression();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:376:23: ( ASTERISK unaryExpression | DIVIDE unaryExpression )*
            loop55:
            do {
                int alt55=3;
                int LA55_0 = input.LA(1);

                if ( (LA55_0==ASTERISK) ) {
                    alt55=1;
                }
                else if ( (LA55_0==DIVIDE) ) {
                    alt55=2;
                }


                switch (alt55) {
            	case 1 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:376:25: ASTERISK unaryExpression
            	    {
            	    match(input,ASTERISK,FOLLOW_ASTERISK_in_multiplicativeExpression1762); 
            	    pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression1764);
            	    unaryExpression();

            	    state._fsp--;


            	    }
            	    break;
            	case 2 :
            	    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:376:52: DIVIDE unaryExpression
            	    {
            	    match(input,DIVIDE,FOLLOW_DIVIDE_in_multiplicativeExpression1768); 
            	    pushFollow(FOLLOW_unaryExpression_in_multiplicativeExpression1770);
            	    unaryExpression();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop55;
                }
            } while (true);


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "multiplicativeExpression"


    // $ANTLR start "unaryExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:379:1: unaryExpression : ( NOT primaryExpression | PLUS primaryExpression | MINUS primaryExpression | primaryExpression );
    public final void unaryExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:380:5: ( NOT primaryExpression | PLUS primaryExpression | MINUS primaryExpression | primaryExpression )
            int alt56=4;
            switch ( input.LA(1) ) {
            case NOT:
                {
                alt56=1;
                }
                break;
            case PLUS:
                {
                alt56=2;
                }
                break;
            case MINUS:
                {
                alt56=3;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case INTEGER:
            case OPEN_BRACE:
            case VAR1:
            case VAR2:
            case STR:
            case LANG:
            case LANGMATCHES:
            case DATATYPE:
            case BOUND:
            case SAMETERM:
            case ISIRI:
            case ISURI:
            case ISBLANK:
            case ISLITERAL:
            case REGEX:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
            case TRUE:
            case FALSE:
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case STRING_LITERAL_LONG1:
            case STRING_LITERAL_LONG2:
            case PNAME_LN:
                {
                alt56=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }

            switch (alt56) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:380:7: NOT primaryExpression
                    {
                    match(input,NOT,FOLLOW_NOT_in_unaryExpression1790); 
                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression1792);
                    primaryExpression();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:381:7: PLUS primaryExpression
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_unaryExpression1800); 
                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression1802);
                    primaryExpression();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:382:7: MINUS primaryExpression
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_unaryExpression1810); 
                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression1812);
                    primaryExpression();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:383:7: primaryExpression
                    {
                    pushFollow(FOLLOW_primaryExpression_in_unaryExpression1820);
                    primaryExpression();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "unaryExpression"


    // $ANTLR start "primaryExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:386:1: primaryExpression : ( brackettedExpression | builtInCall | iriRefOrFunction | rdfLiteral | numericLiteral | booleanLiteral | var );
    public final void primaryExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:5: ( brackettedExpression | builtInCall | iriRefOrFunction | rdfLiteral | numericLiteral | booleanLiteral | var )
            int alt57=7;
            switch ( input.LA(1) ) {
            case OPEN_BRACE:
                {
                alt57=1;
                }
                break;
            case STR:
            case LANG:
            case LANGMATCHES:
            case DATATYPE:
            case BOUND:
            case SAMETERM:
            case ISIRI:
            case ISURI:
            case ISBLANK:
            case ISLITERAL:
            case REGEX:
                {
                alt57=2;
                }
                break;
            case IRI_REF:
            case PNAME_NS:
            case PNAME_LN:
                {
                alt57=3;
                }
                break;
            case STRING_LITERAL1:
            case STRING_LITERAL2:
            case STRING_LITERAL_LONG1:
            case STRING_LITERAL_LONG2:
                {
                alt57=4;
                }
                break;
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt57=5;
                }
                break;
            case TRUE:
            case FALSE:
                {
                alt57=6;
                }
                break;
            case VAR1:
            case VAR2:
                {
                alt57=7;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 57, 0, input);

                throw nvae;
            }

            switch (alt57) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:7: brackettedExpression
                    {
                    pushFollow(FOLLOW_brackettedExpression_in_primaryExpression1837);
                    brackettedExpression();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:30: builtInCall
                    {
                    pushFollow(FOLLOW_builtInCall_in_primaryExpression1841);
                    builtInCall();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:44: iriRefOrFunction
                    {
                    pushFollow(FOLLOW_iriRefOrFunction_in_primaryExpression1845);
                    iriRefOrFunction();

                    state._fsp--;


                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:63: rdfLiteral
                    {
                    pushFollow(FOLLOW_rdfLiteral_in_primaryExpression1849);
                    rdfLiteral();

                    state._fsp--;


                    }
                    break;
                case 5 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:76: numericLiteral
                    {
                    pushFollow(FOLLOW_numericLiteral_in_primaryExpression1853);
                    numericLiteral();

                    state._fsp--;


                    }
                    break;
                case 6 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:93: booleanLiteral
                    {
                    pushFollow(FOLLOW_booleanLiteral_in_primaryExpression1857);
                    booleanLiteral();

                    state._fsp--;


                    }
                    break;
                case 7 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:387:110: var
                    {
                    pushFollow(FOLLOW_var_in_primaryExpression1861);
                    var();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "primaryExpression"


    // $ANTLR start "brackettedExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:390:1: brackettedExpression : OPEN_BRACE expression CLOSE_BRACE ;
    public final void brackettedExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:391:5: ( OPEN_BRACE expression CLOSE_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:391:7: OPEN_BRACE expression CLOSE_BRACE
            {
            match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_brackettedExpression1878); 
            pushFollow(FOLLOW_expression_in_brackettedExpression1880);
            expression();

            state._fsp--;

            match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_brackettedExpression1882); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "brackettedExpression"


    // $ANTLR start "builtInCall"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:394:1: builtInCall : ( STR OPEN_BRACE expression CLOSE_BRACE | LANG OPEN_BRACE expression CLOSE_BRACE | LANGMATCHES OPEN_BRACE expression COMMA expression CLOSE_BRACE | DATATYPE OPEN_BRACE expression CLOSE_BRACE | BOUND OPEN_BRACE var CLOSE_BRACE | SAMETERM OPEN_BRACE expression COMMA expression CLOSE_BRACE | ISIRI OPEN_BRACE expression CLOSE_BRACE | ISURI OPEN_BRACE expression CLOSE_BRACE | ISBLANK OPEN_BRACE expression CLOSE_BRACE | ISLITERAL OPEN_BRACE expression CLOSE_BRACE | regexExpression );
    public final void builtInCall() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:395:5: ( STR OPEN_BRACE expression CLOSE_BRACE | LANG OPEN_BRACE expression CLOSE_BRACE | LANGMATCHES OPEN_BRACE expression COMMA expression CLOSE_BRACE | DATATYPE OPEN_BRACE expression CLOSE_BRACE | BOUND OPEN_BRACE var CLOSE_BRACE | SAMETERM OPEN_BRACE expression COMMA expression CLOSE_BRACE | ISIRI OPEN_BRACE expression CLOSE_BRACE | ISURI OPEN_BRACE expression CLOSE_BRACE | ISBLANK OPEN_BRACE expression CLOSE_BRACE | ISLITERAL OPEN_BRACE expression CLOSE_BRACE | regexExpression )
            int alt58=11;
            switch ( input.LA(1) ) {
            case STR:
                {
                alt58=1;
                }
                break;
            case LANG:
                {
                alt58=2;
                }
                break;
            case LANGMATCHES:
                {
                alt58=3;
                }
                break;
            case DATATYPE:
                {
                alt58=4;
                }
                break;
            case BOUND:
                {
                alt58=5;
                }
                break;
            case SAMETERM:
                {
                alt58=6;
                }
                break;
            case ISIRI:
                {
                alt58=7;
                }
                break;
            case ISURI:
                {
                alt58=8;
                }
                break;
            case ISBLANK:
                {
                alt58=9;
                }
                break;
            case ISLITERAL:
                {
                alt58=10;
                }
                break;
            case REGEX:
                {
                alt58=11;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 58, 0, input);

                throw nvae;
            }

            switch (alt58) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:395:7: STR OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,STR,FOLLOW_STR_in_builtInCall1899); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1901); 
                    pushFollow(FOLLOW_expression_in_builtInCall1903);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1905); 

                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:396:7: LANG OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,LANG,FOLLOW_LANG_in_builtInCall1913); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1915); 
                    pushFollow(FOLLOW_expression_in_builtInCall1917);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1919); 

                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:397:7: LANGMATCHES OPEN_BRACE expression COMMA expression CLOSE_BRACE
                    {
                    match(input,LANGMATCHES,FOLLOW_LANGMATCHES_in_builtInCall1927); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1929); 
                    pushFollow(FOLLOW_expression_in_builtInCall1931);
                    expression();

                    state._fsp--;

                    match(input,COMMA,FOLLOW_COMMA_in_builtInCall1933); 
                    pushFollow(FOLLOW_expression_in_builtInCall1935);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1937); 

                    }
                    break;
                case 4 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:398:7: DATATYPE OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,DATATYPE,FOLLOW_DATATYPE_in_builtInCall1945); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1947); 
                    pushFollow(FOLLOW_expression_in_builtInCall1949);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1951); 

                    }
                    break;
                case 5 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:399:7: BOUND OPEN_BRACE var CLOSE_BRACE
                    {
                    match(input,BOUND,FOLLOW_BOUND_in_builtInCall1959); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1961); 
                    pushFollow(FOLLOW_var_in_builtInCall1963);
                    var();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1965); 

                    }
                    break;
                case 6 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:400:7: SAMETERM OPEN_BRACE expression COMMA expression CLOSE_BRACE
                    {
                    match(input,SAMETERM,FOLLOW_SAMETERM_in_builtInCall1973); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1975); 
                    pushFollow(FOLLOW_expression_in_builtInCall1977);
                    expression();

                    state._fsp--;

                    match(input,COMMA,FOLLOW_COMMA_in_builtInCall1979); 
                    pushFollow(FOLLOW_expression_in_builtInCall1981);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1983); 

                    }
                    break;
                case 7 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:401:7: ISIRI OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,ISIRI,FOLLOW_ISIRI_in_builtInCall1991); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall1993); 
                    pushFollow(FOLLOW_expression_in_builtInCall1995);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall1997); 

                    }
                    break;
                case 8 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:402:7: ISURI OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,ISURI,FOLLOW_ISURI_in_builtInCall2005); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall2007); 
                    pushFollow(FOLLOW_expression_in_builtInCall2009);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall2011); 

                    }
                    break;
                case 9 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:403:7: ISBLANK OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,ISBLANK,FOLLOW_ISBLANK_in_builtInCall2019); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall2021); 
                    pushFollow(FOLLOW_expression_in_builtInCall2023);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall2025); 

                    }
                    break;
                case 10 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:404:7: ISLITERAL OPEN_BRACE expression CLOSE_BRACE
                    {
                    match(input,ISLITERAL,FOLLOW_ISLITERAL_in_builtInCall2033); 
                    match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_builtInCall2035); 
                    pushFollow(FOLLOW_expression_in_builtInCall2037);
                    expression();

                    state._fsp--;

                    match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_builtInCall2039); 

                    }
                    break;
                case 11 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:405:7: regexExpression
                    {
                    pushFollow(FOLLOW_regexExpression_in_builtInCall2047);
                    regexExpression();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "builtInCall"


    // $ANTLR start "regexExpression"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:408:1: regexExpression : REGEX OPEN_BRACE expression COMMA expression ( COMMA expression )? CLOSE_BRACE ;
    public final void regexExpression() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:409:5: ( REGEX OPEN_BRACE expression COMMA expression ( COMMA expression )? CLOSE_BRACE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:409:7: REGEX OPEN_BRACE expression COMMA expression ( COMMA expression )? CLOSE_BRACE
            {
            match(input,REGEX,FOLLOW_REGEX_in_regexExpression2064); 
            match(input,OPEN_BRACE,FOLLOW_OPEN_BRACE_in_regexExpression2066); 
            pushFollow(FOLLOW_expression_in_regexExpression2068);
            expression();

            state._fsp--;

            match(input,COMMA,FOLLOW_COMMA_in_regexExpression2070); 
            pushFollow(FOLLOW_expression_in_regexExpression2072);
            expression();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:409:52: ( COMMA expression )?
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==COMMA) ) {
                alt59=1;
            }
            switch (alt59) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:409:54: COMMA expression
                    {
                    match(input,COMMA,FOLLOW_COMMA_in_regexExpression2076); 
                    pushFollow(FOLLOW_expression_in_regexExpression2078);
                    expression();

                    state._fsp--;


                    }
                    break;

            }

            match(input,CLOSE_BRACE,FOLLOW_CLOSE_BRACE_in_regexExpression2083); 

            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "regexExpression"


    // $ANTLR start "iriRefOrFunction"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:412:1: iriRefOrFunction : iriRef ( argList )? ;
    public final void iriRefOrFunction() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:413:5: ( iriRef ( argList )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:413:7: iriRef ( argList )?
            {
            pushFollow(FOLLOW_iriRef_in_iriRefOrFunction2100);
            iriRef();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:413:14: ( argList )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==OPEN_BRACE) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:413:14: argList
                    {
                    pushFollow(FOLLOW_argList_in_iriRefOrFunction2102);
                    argList();

                    state._fsp--;


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "iriRefOrFunction"


    // $ANTLR start "rdfLiteral"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:416:1: rdfLiteral : string ( LANGTAG | ( REFERENCE iriRef ) )? ;
    public final void rdfLiteral() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:5: ( string ( LANGTAG | ( REFERENCE iriRef ) )? )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:7: string ( LANGTAG | ( REFERENCE iriRef ) )?
            {
            pushFollow(FOLLOW_string_in_rdfLiteral2120);
            string();

            state._fsp--;

            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:14: ( LANGTAG | ( REFERENCE iriRef ) )?
            int alt61=3;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==LANGTAG) ) {
                alt61=1;
            }
            else if ( (LA61_0==REFERENCE) ) {
                alt61=2;
            }
            switch (alt61) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:16: LANGTAG
                    {
                    match(input,LANGTAG,FOLLOW_LANGTAG_in_rdfLiteral2124); 

                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:26: ( REFERENCE iriRef )
                    {
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:26: ( REFERENCE iriRef )
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:417:28: REFERENCE iriRef
                    {
                    match(input,REFERENCE,FOLLOW_REFERENCE_in_rdfLiteral2130); 
                    pushFollow(FOLLOW_iriRef_in_rdfLiteral2132);
                    iriRef();

                    state._fsp--;


                    }


                    }
                    break;

            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "rdfLiteral"


    // $ANTLR start "numericLiteral"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:420:1: numericLiteral : ( numericLiteralUnsigned | numericLiteralPositive | numericLiteralNegative );
    public final void numericLiteral() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:421:5: ( numericLiteralUnsigned | numericLiteralPositive | numericLiteralNegative )
            int alt62=3;
            switch ( input.LA(1) ) {
            case INTEGER:
            case DECIMAL:
            case DOUBLE:
                {
                alt62=1;
                }
                break;
            case INTEGER_POSITIVE:
            case DECIMAL_POSITIVE:
            case DOUBLE_POSITIVE:
                {
                alt62=2;
                }
                break;
            case INTEGER_NEGATIVE:
            case DECIMAL_NEGATIVE:
            case DOUBLE_NEGATIVE:
                {
                alt62=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }

            switch (alt62) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:421:7: numericLiteralUnsigned
                    {
                    pushFollow(FOLLOW_numericLiteralUnsigned_in_numericLiteral2154);
                    numericLiteralUnsigned();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:421:32: numericLiteralPositive
                    {
                    pushFollow(FOLLOW_numericLiteralPositive_in_numericLiteral2158);
                    numericLiteralPositive();

                    state._fsp--;


                    }
                    break;
                case 3 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:421:57: numericLiteralNegative
                    {
                    pushFollow(FOLLOW_numericLiteralNegative_in_numericLiteral2162);
                    numericLiteralNegative();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "numericLiteral"


    // $ANTLR start "numericLiteralUnsigned"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:424:1: numericLiteralUnsigned : ( INTEGER | DECIMAL | DOUBLE );
    public final void numericLiteralUnsigned() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:425:5: ( INTEGER | DECIMAL | DOUBLE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( input.LA(1)==INTEGER||(input.LA(1)>=DECIMAL && input.LA(1)<=DOUBLE) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "numericLiteralUnsigned"


    // $ANTLR start "numericLiteralPositive"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:430:1: numericLiteralPositive : ( INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE );
    public final void numericLiteralPositive() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:431:5: ( INTEGER_POSITIVE | DECIMAL_POSITIVE | DOUBLE_POSITIVE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( (input.LA(1)>=INTEGER_POSITIVE && input.LA(1)<=DOUBLE_POSITIVE) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "numericLiteralPositive"


    // $ANTLR start "numericLiteralNegative"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:436:1: numericLiteralNegative : ( INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE );
    public final void numericLiteralNegative() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:437:5: ( INTEGER_NEGATIVE | DECIMAL_NEGATIVE | DOUBLE_NEGATIVE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( (input.LA(1)>=INTEGER_NEGATIVE && input.LA(1)<=DOUBLE_NEGATIVE) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "numericLiteralNegative"


    // $ANTLR start "booleanLiteral"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:442:1: booleanLiteral : ( TRUE | FALSE );
    public final void booleanLiteral() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:443:5: ( TRUE | FALSE )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( (input.LA(1)>=TRUE && input.LA(1)<=FALSE) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "booleanLiteral"


    // $ANTLR start "string"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:447:1: string : ( STRING_LITERAL1 | STRING_LITERAL2 | STRING_LITERAL_LONG1 | STRING_LITERAL_LONG2 );
    public final void string() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:448:5: ( STRING_LITERAL1 | STRING_LITERAL2 | STRING_LITERAL_LONG1 | STRING_LITERAL_LONG2 )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( (input.LA(1)>=STRING_LITERAL1 && input.LA(1)<=STRING_LITERAL_LONG2) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "string"


    // $ANTLR start "iriRef"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:454:1: iriRef : ( IRI_REF | prefixedName );
    public final void iriRef() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:455:5: ( IRI_REF | prefixedName )
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==IRI_REF) ) {
                alt63=1;
            }
            else if ( (LA63_0==PNAME_NS||LA63_0==PNAME_LN) ) {
                alt63=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 63, 0, input);

                throw nvae;
            }
            switch (alt63) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:455:7: IRI_REF
                    {
                    match(input,IRI_REF,FOLLOW_IRI_REF_in_iriRef2344); 

                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:456:7: prefixedName
                    {
                    pushFollow(FOLLOW_prefixedName_in_iriRef2352);
                    prefixedName();

                    state._fsp--;


                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "iriRef"


    // $ANTLR start "prefixedName"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:459:1: prefixedName : ( PNAME_LN | PNAME_NS );
    public final void prefixedName() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:460:5: ( PNAME_LN | PNAME_NS )
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:
            {
            if ( input.LA(1)==PNAME_NS||input.LA(1)==PNAME_LN ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "prefixedName"


    // $ANTLR start "blankNode"
    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:464:1: blankNode : ( BLANK_NODE_LABEL | OPEN_SQUARE_BRACE CLOSE_SQUARE_BRACE );
    public final void blankNode() throws RecognitionException {
        try {
            // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:465:5: ( BLANK_NODE_LABEL | OPEN_SQUARE_BRACE CLOSE_SQUARE_BRACE )
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==BLANK_NODE_LABEL) ) {
                alt64=1;
            }
            else if ( (LA64_0==OPEN_SQUARE_BRACE) ) {
                alt64=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 64, 0, input);

                throw nvae;
            }
            switch (alt64) {
                case 1 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:465:7: BLANK_NODE_LABEL
                    {
                    match(input,BLANK_NODE_LABEL,FOLLOW_BLANK_NODE_LABEL_in_blankNode2394); 

                    }
                    break;
                case 2 :
                    // D:\\workspace\\rdf3x GUI\\src\\sparql\\parser\\MySparql.g:466:7: OPEN_SQUARE_BRACE CLOSE_SQUARE_BRACE
                    {
                    match(input,OPEN_SQUARE_BRACE,FOLLOW_OPEN_SQUARE_BRACE_in_blankNode2402); 
                    match(input,CLOSE_SQUARE_BRACE,FOLLOW_CLOSE_SQUARE_BRACE_in_blankNode2404); 

                    }
                    break;

            }
        }

          catch(RecognitionException e){
            throw e;
          }
        finally {
        }
        return ;
    }
    // $ANTLR end "blankNode"

    // Delegated rules


 

    public static final BitSet FOLLOW_prologue_in_query64 = new BitSet(new long[]{0x0000000000007100L});
    public static final BitSet FOLLOW_selectQuery_in_query68 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_constructQuery_in_query72 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_describeQuery_in_query76 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_askQuery_in_query80 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_query84 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_baseDecl_in_prologue101 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_prefixDecl_in_prologue104 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_BASE_in_baseDecl122 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IRI_REF_in_baseDecl124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PREFIX_in_prefixDecl141 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_PNAME_NS_in_prefixDecl143 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_IRI_REF_in_prefixDecl145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SELECT_in_selectQuery162 = new BitSet(new long[]{0x0000018000000E00L});
    public static final BitSet FOLLOW_set_in_selectQuery164 = new BitSet(new long[]{0x0000018000000800L});
    public static final BitSet FOLLOW_var_in_selectQuery177 = new BitSet(new long[]{0x0000018002028000L});
    public static final BitSet FOLLOW_ASTERISK_in_selectQuery182 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_datasetClause_in_selectQuery186 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_whereClause_in_selectQuery189 = new BitSet(new long[]{0x0000000001440000L});
    public static final BitSet FOLLOW_solutionModifier_in_selectQuery191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONSTRUCT_in_constructQuery208 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_constructTemplate_in_constructQuery210 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_datasetClause_in_constructQuery212 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_whereClause_in_constructQuery215 = new BitSet(new long[]{0x0000000001440000L});
    public static final BitSet FOLLOW_solutionModifier_in_constructQuery217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DESCRIBE_in_describeQuery234 = new BitSet(new long[]{0x00000180000008A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_varOrIRIref_in_describeQuery238 = new BitSet(new long[]{0x00000180034680A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_ASTERISK_in_describeQuery243 = new BitSet(new long[]{0x0000000003468000L});
    public static final BitSet FOLLOW_datasetClause_in_describeQuery247 = new BitSet(new long[]{0x0000000003468000L});
    public static final BitSet FOLLOW_whereClause_in_describeQuery250 = new BitSet(new long[]{0x0000000001440000L});
    public static final BitSet FOLLOW_solutionModifier_in_describeQuery253 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ASK_in_askQuery270 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_datasetClause_in_askQuery272 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_whereClause_in_askQuery275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FROM_in_datasetClause292 = new BitSet(new long[]{0x00000180000100A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_defaultGraphClause_in_datasetClause296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_namedGraphClause_in_datasetClause300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_sourceSelector_in_defaultGraphClause319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NAMED_in_namedGraphClause336 = new BitSet(new long[]{0x00000180000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_sourceSelector_in_namedGraphClause338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_sourceSelector355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WHERE_in_whereClause372 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_whereClause375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_orderClause_in_solutionModifier392 = new BitSet(new long[]{0x0000000001400002L});
    public static final BitSet FOLLOW_limitOffsetClauses_in_solutionModifier395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_limitClause_in_limitOffsetClauses415 = new BitSet(new long[]{0x0000000001400002L});
    public static final BitSet FOLLOW_offsetClause_in_limitOffsetClauses417 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_offsetClause_in_limitOffsetClauses422 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_limitClause_in_limitOffsetClauses424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ORDER_in_orderClause444 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_BY_in_orderClause446 = new BitSet(new long[]{0xFFE00181003000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_orderCondition_in_orderClause448 = new BitSet(new long[]{0xFFE00181003000A2L,0x0000000000010000L});
    public static final BitSet FOLLOW_set_in_orderCondition468 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_brackettedExpression_in_orderCondition478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_constraint_in_orderCondition490 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_orderCondition494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LIMIT_in_limitClause513 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_INTEGER_in_limitClause515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OFFSET_in_offsetClause532 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_INTEGER_in_offsetClause534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_CURLY_BRACE_in_groupGraphPattern569 = new BitSet(new long[]{0x000001A1BA8280A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_triplesBlock_in_groupGraphPattern586 = new BitSet(new long[]{0x00000000BA028000L});
    public static final BitSet FOLLOW_graphPatternNotTriples_in_groupGraphPattern599 = new BitSet(new long[]{0x000001A1BE8280A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_filter_in_groupGraphPattern603 = new BitSet(new long[]{0x000001A1BE8280A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_DOT_in_groupGraphPattern608 = new BitSet(new long[]{0x000001A1BA8280A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_triplesBlock_in_groupGraphPattern614 = new BitSet(new long[]{0x00000000BA028000L});
    public static final BitSet FOLLOW_CLOSE_CURLY_BRACE_in_groupGraphPattern626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesSameSubject_in_triplesBlock658 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_DOT_in_triplesBlock662 = new BitSet(new long[]{0x000001A1008000A2L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_triplesBlock_in_triplesBlock666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_optionalGraphPattern_in_graphPatternNotTriples697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_groupOrUnionGraphPattern_in_graphPatternNotTriples701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_graphGraphPattern_in_graphPatternNotTriples705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPTIONAL_in_optionalGraphPattern722 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_optionalGraphPattern724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GRAPH_in_graphGraphPattern741 = new BitSet(new long[]{0x00000180000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_varOrIRIref_in_graphGraphPattern743 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_graphGraphPattern745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern762 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_UNION_in_groupOrUnionGraphPattern766 = new BitSet(new long[]{0x0000000002028000L});
    public static final BitSet FOLLOW_groupGraphPattern_in_groupOrUnionGraphPattern768 = new BitSet(new long[]{0x0000000040000002L});
    public static final BitSet FOLLOW_FILTER_in_filter788 = new BitSet(new long[]{0xFFE00181000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_constraint_in_filter790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_brackettedExpression_in_constraint807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_builtInCall_in_constraint811 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_constraint815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_functionCall832 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_argList_in_functionCall834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_argList853 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_argList855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_argList859 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_argList861 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_COMMA_in_argList865 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_argList867 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_argList872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_CURLY_BRACE_in_constructTemplate891 = new BitSet(new long[]{0x000001A1088000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_constructTriples_in_constructTemplate893 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_CLOSE_CURLY_BRACE_in_constructTemplate896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesSameSubject_in_constructTriples913 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_DOT_in_constructTriples917 = new BitSet(new long[]{0x000001A1008000A2L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_constructTriples_in_constructTriples919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_triplesSameSubject948 = new BitSet(new long[]{0x00000190000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_triplesSameSubject964 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNode_in_triplesSameSubject983 = new BitSet(new long[]{0x00000190000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_propertyList_in_triplesSameSubject1003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_verb_in_propertyListNotEmpty1038 = new BitSet(new long[]{0x000001A1008000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_objectList_in_propertyListNotEmpty1054 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_SEMICOLON_in_propertyListNotEmpty1071 = new BitSet(new long[]{0x00000198000000A2L,0x0000000000010000L});
    public static final BitSet FOLLOW_verb_in_propertyListNotEmpty1104 = new BitSet(new long[]{0x000001A1008000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_objectList_in_propertyListNotEmpty1128 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_propertyList1176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_object_in_objectList1202 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_COMMA_in_objectList1220 = new BitSet(new long[]{0x000001A1008000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_object_in_objectList1244 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_graphNode_in_object1298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrIRIref_in_verb1315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_A_in_verb1323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_collection_in_triplesNode1340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blankNodePropertyList_in_triplesNode1348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_SQUARE_BRACE_in_blankNodePropertyList1365 = new BitSet(new long[]{0x00000190000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_propertyListNotEmpty_in_blankNodePropertyList1367 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_CLOSE_SQUARE_BRACE_in_blankNodePropertyList1369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_collection1386 = new BitSet(new long[]{0x000001A1008000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_graphNode_in_collection1388 = new BitSet(new long[]{0x000001A3008000A0L,0x000000000003FFFCL});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_collection1391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varOrTerm_in_graphNode1408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_triplesNode_in_graphNode1412 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_varOrTerm1429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_graphTerm_in_varOrTerm1437 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_varOrIRIref1454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_varOrIRIref1458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_var0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_graphTerm1500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rdfLiteral_in_graphTerm1508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteral_in_graphTerm1516 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanLiteral_in_graphTerm1524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_blankNode_in_graphTerm1532 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_graphTerm1540 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_graphTerm1542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditionalOrExpression_in_expression1559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression1576 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_OR_in_conditionalOrExpression1580 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_conditionalAndExpression_in_conditionalOrExpression1582 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_valueLogical_in_conditionalAndExpression1602 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_AND_in_conditionalAndExpression1606 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_valueLogical_in_conditionalAndExpression1608 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_relationalExpression_in_valueLogical1628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1645 = new BitSet(new long[]{0x0001F80000000002L});
    public static final BitSet FOLLOW_EQUAL_in_relationalExpression1649 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1651 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_EQUAL_in_relationalExpression1655 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LESS_in_relationalExpression1661 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GREATER_in_relationalExpression1667 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LESS_EQUAL_in_relationalExpression1673 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1675 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GREATER_EQUAL_in_relationalExpression1679 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_numericExpression_in_relationalExpression1681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_additiveExpression_in_numericExpression1701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression1718 = new BitSet(new long[]{0x0006000000800002L,0x00000000000003FCL});
    public static final BitSet FOLLOW_PLUS_in_additiveExpression1722 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression1724 = new BitSet(new long[]{0x0006000000800002L,0x00000000000003FCL});
    public static final BitSet FOLLOW_MINUS_in_additiveExpression1728 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_multiplicativeExpression_in_additiveExpression1730 = new BitSet(new long[]{0x0006000000800002L,0x00000000000003FCL});
    public static final BitSet FOLLOW_numericLiteralPositive_in_additiveExpression1734 = new BitSet(new long[]{0x0006000000800002L,0x00000000000003FCL});
    public static final BitSet FOLLOW_numericLiteralNegative_in_additiveExpression1738 = new BitSet(new long[]{0x0006000000800002L,0x00000000000003FCL});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression1758 = new BitSet(new long[]{0x0008000000000802L});
    public static final BitSet FOLLOW_ASTERISK_in_multiplicativeExpression1762 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression1764 = new BitSet(new long[]{0x0008000000000802L});
    public static final BitSet FOLLOW_DIVIDE_in_multiplicativeExpression1768 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_unaryExpression_in_multiplicativeExpression1770 = new BitSet(new long[]{0x0008000000000802L});
    public static final BitSet FOLLOW_NOT_in_unaryExpression1790 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression1792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_unaryExpression1800 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression1802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_unaryExpression1810 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression1812 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primaryExpression_in_unaryExpression1820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_brackettedExpression_in_primaryExpression1837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_builtInCall_in_primaryExpression1841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRefOrFunction_in_primaryExpression1845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rdfLiteral_in_primaryExpression1849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteral_in_primaryExpression1853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_booleanLiteral_in_primaryExpression1857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_in_primaryExpression1861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_brackettedExpression1878 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_brackettedExpression1880 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_brackettedExpression1882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STR_in_builtInCall1899 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1901 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1903 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1905 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LANG_in_builtInCall1913 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1915 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1917 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LANGMATCHES_in_builtInCall1927 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1929 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1931 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_COMMA_in_builtInCall1933 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1935 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DATATYPE_in_builtInCall1945 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1947 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1949 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BOUND_in_builtInCall1959 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1961 = new BitSet(new long[]{0x0000018000000000L});
    public static final BitSet FOLLOW_var_in_builtInCall1963 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SAMETERM_in_builtInCall1973 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1975 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1977 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_COMMA_in_builtInCall1979 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1981 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISIRI_in_builtInCall1991 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall1993 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall1995 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall1997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISURI_in_builtInCall2005 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall2007 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall2009 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall2011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISBLANK_in_builtInCall2019 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall2021 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall2023 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall2025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ISLITERAL_in_builtInCall2033 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_builtInCall2035 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_builtInCall2037 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_builtInCall2039 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_regexExpression_in_builtInCall2047 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REGEX_in_regexExpression2064 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_OPEN_BRACE_in_regexExpression2066 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_regexExpression2068 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_COMMA_in_regexExpression2070 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_regexExpression2072 = new BitSet(new long[]{0x0000000600000000L});
    public static final BitSet FOLLOW_COMMA_in_regexExpression2076 = new BitSet(new long[]{0xFFF60181008000A0L,0x000000000001FFFCL});
    public static final BitSet FOLLOW_expression_in_regexExpression2078 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_CLOSE_BRACE_in_regexExpression2083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iriRef_in_iriRefOrFunction2100 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_argList_in_iriRefOrFunction2102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_string_in_rdfLiteral2120 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000003L});
    public static final BitSet FOLLOW_LANGTAG_in_rdfLiteral2124 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_REFERENCE_in_rdfLiteral2130 = new BitSet(new long[]{0x00000180000000A0L,0x0000000000010000L});
    public static final BitSet FOLLOW_iriRef_in_rdfLiteral2132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralUnsigned_in_numericLiteral2154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralPositive_in_numericLiteral2158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numericLiteralNegative_in_numericLiteral2162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralUnsigned0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralPositive0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_numericLiteralNegative0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_booleanLiteral0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_string0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IRI_REF_in_iriRef2344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_prefixedName_in_iriRef2352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_prefixedName0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BLANK_NODE_LABEL_in_blankNode2394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_SQUARE_BRACE_in_blankNode2402 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_CLOSE_SQUARE_BRACE_in_blankNode2404 = new BitSet(new long[]{0x0000000000000002L});

}