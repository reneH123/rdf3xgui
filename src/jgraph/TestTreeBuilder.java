package jgraph;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import jgraph.nodes.MergeJoin;
import jgraph.nodes.TreeNode;
import junit.framework.TestCase;

public class TestTreeBuilder extends TestCase {
	private TreeBuilder builder;
	private TreeNode node;
	private TreeNodeVisitor visitor;
	private String out;

	// test helper
	private final static String[] merge = { "1", "MergeJoin", "me1", "me2" };
	private final static String[] sel = { "2", "Selection", "se1", "se2" };
	private final static String[] ais = { "3", "AggregatedIndexScan", "da1",
			"da2" };

	public String[] fmerge(int index) {
		String[] bin2 = merge.clone();
		bin2[0] = new Integer(index).toString();
		return bin2;
	}

	public String[] fsel(int index) {
		String[] fun2 = sel.clone();
		fun2[0] = new Integer(index).toString();
		return fun2;
	}

	public String[] fais(int index) {
		String[] no2 = ais.clone();
		no2[0] = new Integer(index).toString();
		return no2;
	}

	// @Before
	public void setUp() throws Exception {
		builder = new TreeBuilder();
		node = null;
		visitor = new TreeNodeVisitor();
	}

	// new node is inserted into the first level
	public void testFirstLevel() {
		// empty node
		builder.create(null);
		node = builder.getRoot();
		assertNull(node);

		// leaf node
		builder.create(fais(1));
		node = builder.getRoot();
		assertNotNull(node);
		builder.create(fais(2));
		node = builder.getRoot();
		assertNull(node);
	}

	// new node is inserted into the second level
	public void testSecondLevel0() {
		// before
		builder.create(fsel(1));
		node = builder.getRoot();
		assertFalse(node.isGround());
		assertNotNull(node);
		out = node.accept(visitor);
		assertEquals(out, "Selection()");

		// after
		builder.create(fais(2));
		node = builder.getRoot();
		assertTrue(node.isGround());
		assertNotNull(node);
		out = node.accept(visitor);
		assertEquals(out, "Selection(AggregatedIndexScan)");
	}

	public void testSecondLevel1a() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		builder.create(fais(2));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,AggregatedIndexScan)");
		assertTrue(node.isGround());
		((MergeJoin) node).setLeft(null);
		assertFalse(node.isGround());
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,AggregatedIndexScan)");
		builder.setRoot(node);

		// after
		builder.create(fais(2));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,AggregatedIndexScan)");
		assertTrue(node.isGround());
	}

	public void testSecondLevel2a() {
		// before
		builder.create(fmerge(1));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,)");
	}

	public void testSecondLevel3a() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		builder.create(fsel(2));
		builder.create(fais(3));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,Selection(AggregatedIndexScan))");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(AggregatedIndexScan,Selection(AggregatedIndexScan))");
	}

	public void testSecondLevel4a() {
		// before
		builder.create(merge);
		builder.create(fais(2));
		builder.create(fsel(2));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,Selection())");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,Selection())");
	}

	public void testSecondLevel5a() {
		// before
		builder.create(merge);
		builder.create(fais(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fais(3));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan))");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan))");
	}

	public void testSecondLevel6a() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		builder.create(fmerge(2));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,MergeJoin(,))");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,MergeJoin(,))");
	}

	public void testSecondLevel7a() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fais(3));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,MergeJoin(,AggregatedIndexScan))");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(AggregatedIndexScan,MergeJoin(,AggregatedIndexScan))");
	}

	public void testSecondLevel8a() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		node = builder.getRoot();
		((MergeJoin) node).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(,MergeJoin(AggregatedIndexScan,))");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,))");
	}

	public void testSecondLevel1b() {
		// before
		builder.create(fmerge(1));
		builder.create(fais(2));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(AggregatedIndexScan,AggregatedIndexScan)");
	}

	public void testSecondLevel2b() {
		// before
		builder.create(fmerge(1));
		builder.create(fsel(2));
		builder.create(fais(3));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(Selection(AggregatedIndexScan),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(Selection(AggregatedIndexScan),AggregatedIndexScan)");
	}

	public void testSecondLevel3b() {
		// before
		builder.create(fmerge(1));
		builder.create(fsel(2));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(Selection(),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(Selection(),AggregatedIndexScan)");
	}

	public void testSecondLevel4b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fais(3));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,AggregatedIndexScan),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,AggregatedIndexScan),AggregatedIndexScan)");
	}

	public void testSecondLevel5b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		node = builder.getRoot();
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(MergeJoin(,),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(MergeJoin(,),AggregatedIndexScan)");
	}

	public void testSecondLevel6b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fais(3));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(MergeJoin(,AggregatedIndexScan),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(MergeJoin(,AggregatedIndexScan),AggregatedIndexScan)");
	}

	public void testSecondLevel7b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fais(3));
		node = builder.getRoot();
		out = node.accept(visitor);
		out = node.accept(visitor);
		assertEquals(out, "MergeJoin(MergeJoin(AggregatedIndexScan,),)");

		// after
		builder.create(fais(2));
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,),AggregatedIndexScan)");
	}

	public void testNthLevel1a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");

		// after
		builder.create(fais(3));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel1b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,)),MergeJoin(,MergeJoin(,AggregatedIndexScan)))");

		// after
		builder.create(fais(3));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)),MergeJoin(,MergeJoin(,AggregatedIndexScan)))");
	}

	public void testNthLevel2a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel2b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(,)),MergeJoin(,MergeJoin(,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)),MergeJoin(,MergeJoin(,)))");
	}

	public void testNthLevel2c() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(,AggregatedIndexScan)),MergeJoin(,MergeJoin(,AggregatedIndexScan)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(,AggregatedIndexScan)))");
	}

	public void testNthLevel3a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel3b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)),MergeJoin(,MergeJoin(,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(,)))");
	}

	public void testNthLevel3c() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(out,
				"MergeJoin(MergeJoin(,MergeJoin(,)),MergeJoin(,MergeJoin(,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,)),MergeJoin(,MergeJoin(,)))");
	}

	public void testNthLevel4a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");

		// after
		builder.create(fais(3));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel4b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(,AggregatedIndexScan)))");

		// after
		builder.create(fais(3));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(,AggregatedIndexScan)))");
	}

	public void testNthLevel4c() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getLeft()).getRight())
				.setRight(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(,)),MergeJoin(,MergeJoin(,)))");

		// after
		builder.create(fais(3));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(,)),MergeJoin(AggregatedIndexScan,MergeJoin(,)))");
	}

	public void testNthLevel5a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(,AggregatedIndexScan)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel5b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)))");
	}

	public void testNthLevel5c() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(AggregatedIndexScan,)))");
	}

	public void testNthLevel6a() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(AggregatedIndexScan,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}

	public void testNthLevel6b() {
		// before
		builder.create(fmerge(1));
		builder.create(fmerge(2));
		builder.create(fmerge(2));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(3));
		builder.create(fmerge(3));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		builder.create(fais(4));
		node = builder.getRoot();
		((MergeJoin) ((MergeJoin) node).getLeft()).setLeft(null);
		((MergeJoin) ((MergeJoin) node).getRight()).setLeft(null);
		((MergeJoin) ((MergeJoin) ((MergeJoin) node).getRight()).getRight())
				.setRight(null);
		builder.setRoot(node);
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(AggregatedIndexScan,)))");

		// after
		builder.create(fais(4));
		out = node.accept(visitor);
		assertEquals(
				out,
				"MergeJoin(MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)),MergeJoin(,MergeJoin(AggregatedIndexScan,AggregatedIndexScan)))");
	}
}