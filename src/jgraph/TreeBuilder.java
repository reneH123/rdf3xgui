package jgraph;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import jgraph.nodes.AggregatedIndexScan;
import jgraph.nodes.BinaryOperator;
import jgraph.nodes.IndexScan;
import jgraph.nodes.IndexSeek;
import jgraph.nodes.InnerJoin;
import jgraph.nodes.Leaf;
import jgraph.nodes.MergeJoin;
import jgraph.nodes.OuterJoin;
import jgraph.nodes.Selection;
import jgraph.nodes.SemiJoin;
import jgraph.nodes.Sort;
import jgraph.nodes.TreeNode;
import jgraph.nodes.UnaryOperator;

public class TreeBuilder {
	private TreeNode root;
	
	public TreeNode getRoot(){
		return root;
	}
	
	public void setRoot(TreeNode root){
		this.root = root;
	}
	
	public TreeBuilder() {
		root=null;
	}

	/**
	 * returns null if node was not inserted
	 *         otherwise the new tree
	 */
	private TreeNode insert(int startLevel, int level, TreeNode root,
			TreeNode newNode) {
		if (startLevel > level) {
			return null;
		}
		if (root == null) {
			if (startLevel == level){
				return newNode;
			}
			return null;
		}

		// 1. case: leaf
		if (root instanceof Leaf) {
			return null;
		}
		// 2. case: unary node
		if (root instanceof UnaryOperator) {
			if (((UnaryOperator) root).getChild() == null) {
				// insert here if (startLevel==level)
				if (startLevel + 1 == level) {
					((UnaryOperator) root).setChild(newNode);
					//return true;
					return root;
				}
				// else descendant nodes are missing
				return null;
			} else {
				// assert: (startLevel<level)
				if (startLevel < level) {
					TreeNode newChild = insert(startLevel + 1, level, ((UnaryOperator) root)
									.getChild(), newNode);
					((UnaryOperator)root).setChild(newChild);
					return root;
				}
				return null;
			}
		}
		// 3. case: binary node; pre-order traversal
		if (root instanceof BinaryOperator) {
			// flat checking
			if (((BinaryOperator) root).getLeft() == null) {
				// insert here if (startLevel==level)
				if (startLevel + 1 == level) {
					((BinaryOperator) root).setLeft(newNode);
					//return true;
					return root;
				}
			} else if (((BinaryOperator) root).getRight() == null) {
				// insert here if (startLevel==level)
				if (startLevel + 1 == level) {
					((BinaryOperator) root).setRight(newNode);
					//return true;
					return root;
				}
			}

			// deep checking; left-> right
			if (startLevel < level) {
				// ... and right
				TreeNode newNodeLeft = insert(startLevel + 1, level,
						((BinaryOperator) root).getLeft(), newNode);
				if (newNodeLeft!=null){
					((BinaryOperator)root).setLeft(newNodeLeft);
					return root;
				}
				TreeNode newNodeRight = insert(startLevel + 1, level,
						((BinaryOperator) root).getRight(), newNode);
				if (newNodeRight!=null){
					((BinaryOperator)root).setRight(newNodeRight);
					return root;
				}
			}
			// level has been achieved
			return null;
		}

		// unknown node identifier
		return null;
	}
	
	/**
	 * create a new entry in the (2,1)-tree
	 * 
	 * @param node
	 */
	public void create(String[] node) {
		if (node == null) {
			return;
		}

		int l = node.length;
		if (l != 4) {
			System.err.println("Explanation is not valid!");
			return;
		}

		String type = node[1];
		TreeNode t;
		
		// clear quotation marks if there are some
		if (type.startsWith("\"")&&(type.endsWith("\""))){
			type = type.substring(1,type.length()-1);
		}

		if (type.equals("IndexScan")) {
			t = new IndexScan();
		}
		else if (type.equals("AggregatedIndexScan")) {
			t = new AggregatedIndexScan();
		}
		else if (type.equals("IndexSeek")) {
			t = new IndexSeek();
		}
		else if (type.equals("Sort")) {
			t = new Sort();
		}
		else if (type.equals("InnerJoin")) {
			t = new InnerJoin();
		}
		else if (type.equals("MergeJoin")) {
			t = new MergeJoin();
		}
		else if (type.equals("OuterJoin")) {
			t = new OuterJoin();
		}
		else if (type.equals("Selection")) {
			t = new Selection();
		}
		else if (type.equals("SemiJoin")) {
			t = new SemiJoin();
		}
		else{
			System.err.println("unknown node identifier::" + type);
			return;
		}
		t.setDetail1(node[2]);
		t.setDetail2(node[3]);

		int level = new Integer(node[0]).intValue();
		if (level < 1) {
			System.err.println("minimal insert niveau has to be at least 1 !");
			return;
		}

		root = insert(1, level, root, t);
	}
}