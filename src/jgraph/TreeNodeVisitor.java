package jgraph;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import jgraph.nodes.TreeNode;

public class TreeNodeVisitor {
	public String visit(TreeNode node) {
		if (node == null) {
			return null;
		}
		return node.getName();
	}
}