package jgraph;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import gui.start.MainWindow;

import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.ToolTipManager;

import jgraph.nodes.TreeNode;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;

public class GraphCellVisitor {

	public DefaultGraphCell[] visit(TreeNode node) {
		LinkedList<DefaultGraphCell> result = node.flatten();

		int size = result.size();
		DefaultGraphCell[] array = new DefaultGraphCell[size];
		Iterator<DefaultGraphCell> it = result.iterator();

		for (int i = 0; i < size; i++) {
			array[i] = it.next();
		}

		return array;
	}

	public static JGraph createJGraph(DefaultGraphCell[] cells,
			final MainWindow frame) {
		// Switch off D3D because of Sun XOR painting bug
		System.setProperty("sun.java2d.d3d", "false");
		
		JGraph jGraph = new JGraph(new DefaultGraphModel()) {
			private static final long serialVersionUID = 1L;

			@Override
			public String getToolTipText(MouseEvent e) {
				if (e != null) {
					// Fetch Cell under Mousepointer
					Object c = getFirstCellForLocation(e.getX(), e.getY());
					if ((c instanceof TreeNode)==false){
						return null;
					}
					if (c != null)
						return ((TreeNode)c).getDetail1() + ",  " + ((TreeNode)c).getDetail2();
				}
				return null;
			}
		};
		ToolTipManager.sharedInstance().registerComponent(jGraph);
		
		GraphListener listener = new GraphListener();
		listener.setRef(jGraph);
		jGraph.addMouseListener(listener);

		jGraph.setCloneable(false);
		jGraph.setInvokesStopCellEditing(true);
		jGraph.setJumpToDefaultPort(true);

		jGraph.getGraphLayoutCache().insert(cells);
		jGraph.setEnabled(true);
		jGraph.setEditable(false);

		return jGraph;
	}
}