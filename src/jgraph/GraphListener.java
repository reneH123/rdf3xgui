package jgraph;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jgraph.JGraph;

public class GraphListener implements ActionListener, MouseListener {
	private JGraph ref = null;

	public JGraph getRef() {
		return ref;
	}

	public void setRef(JGraph ref) {
		this.ref = ref;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {

		if (ae.getActionCommand().equals("Save as PNG...")) {
			if (getRef() == null) {
				System.out.println("Graph to export was empty!");
				return;
			}
			JFileChooser fileChooser = new JFileChooser();
			FileNameExtensionFilter pngFilter = new FileNameExtensionFilter(
					"Portable Network Graphics (png)", "png");
			fileChooser.addChoosableFileFilter(pngFilter);
			fileChooser.setFileFilter(pngFilter);
			int result = fileChooser.showSaveDialog(null);
			String imageFileName = null;

			if (result == JFileChooser.APPROVE_OPTION) {
				imageFileName = fileChooser.getSelectedFile().toString();

				FileFilter selected = fileChooser.getFileFilter();
				try {
					if (selected.equals(pngFilter)) {
						if (imageFileName.contains(".") == false) {
							imageFileName += ".png";
						}
					} else {
						imageFileName += ".png";
					}
				} catch (Exception e) {
					System.err.println("Could not save tableModel into file '"
							+ imageFileName + "'!");
					return;
				}
			}

			File f = new File(imageFileName);
			try {
				BufferedOutputStream out = new BufferedOutputStream(
						new FileOutputStream(f.toString()));
				BufferedImage image = getRef().getImage(
						getRef().getBackground(), 0);
				ImageIO.write(image, "png", out);
				out.close();
			} catch (Exception e) {
				System.out.println("Exception during PNG export into file "
						+ imageFileName + " !");
			}

		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// right mouse click
		if (SwingUtilities.isRightMouseButton(e)) {
			JPopupMenu popup = new JPopupMenu();

			JMenuItem mi = new JMenuItem("Save as PNG...");
			mi.addActionListener(this);
			popup.add(mi);

			popup.show(e.getComponent(), e.getX(), e.getY());
		}
	}
}
