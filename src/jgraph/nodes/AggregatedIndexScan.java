package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class AggregatedIndexScan extends Leaf {
	private static final long serialVersionUID = 1L;

	public AggregatedIndexScan() {
		super("AggregatedIndexScan");
	}
	
	@Override
	public String toString() {
		return "AggregatedIndexScan";
	}
}
