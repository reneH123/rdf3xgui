package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.LinkedList;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

public abstract class UnaryOperator extends TreeNode {
	private static final long serialVersionUID = 1L;
	
	public UnaryOperator(String name) {
		super(name);
	}
	
	private TreeNode child;

	public TreeNode getChild() {
		return child;
	}

	public void setChild(TreeNode child) {
		this.child = child;
	}
	
	@Override
	public String getName() {
		String c = "";
		if (getChild()!=null){
			c = getChild().getName();
		}
		return toString() + "(" + c + ")";
	}

	@Override
	public boolean isGround() {
		if (getChild() == null) {
			return false;
		}
		return getChild().isGround();
	}
	
	public int height(){
		return 1+child.height();
	}
	public int leafsCount(){
		return child.leafsCount();
	}
	
	protected LinkedList<DefaultGraphCell> flatten2(int x, int y){
		LinkedList<DefaultGraphCell> listChild = child.flatten2(x-1,y);
		setPosX(x);
		setPosY(y);
		setUpGraphCell();
				
		DefaultEdge edge = createEdge(this,child);
		
		LinkedList<DefaultGraphCell> list = new LinkedList<DefaultGraphCell>();
		list.add(this);
		list.add(edge);
		list.addAll(listChild);
		return list;
	}
}