package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class IndexScan extends Leaf {
	private static final long serialVersionUID = 1L;

	public IndexScan() {
		super("IndexScan");
	}

	@Override
	public String toString() {
		return "IndexScan";
	}
}
