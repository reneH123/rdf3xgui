package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class OuterJoin extends BinaryOperator {
	private static final long serialVersionUID = 1L;

	public OuterJoin() {
		super("OuterJoin");
	}

	public String toString() {
		return "OuterJoin";
	}
}
