package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class InnerJoin extends BinaryOperator {
	private static final long serialVersionUID = 1L;

	public InnerJoin() {
		super("InnerJoin");
	}

	@Override
	public String toString() {
		return "InnerJoin";
	}
}
