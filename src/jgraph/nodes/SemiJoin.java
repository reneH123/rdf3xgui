package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class SemiJoin extends BinaryOperator {
	private static final long serialVersionUID = 1L;

	public SemiJoin() {
		super("SemiJoin");
	}

	public String toString() {
		return "SemiJoin";
	}
}
