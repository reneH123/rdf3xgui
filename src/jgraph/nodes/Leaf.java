package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.LinkedList;

import org.jgraph.graph.DefaultGraphCell;

public abstract class Leaf extends TreeNode {
	private static final long serialVersionUID = 1L;
	
	public Leaf(String name) {
		super(name);
	}
	
	//@Override
	public String getName() {
		return toString();
	}

	@Override
	public boolean isGround() {
		return true;
	}

	public int height() {
		return 1;
	}

	public int leafsCount() {
		return 1;
	}

	protected LinkedList<DefaultGraphCell> flatten2(int x, int y) {
		setPosX(x);
		setPosY(y);
		setUpGraphCell();
		
		LinkedList<DefaultGraphCell> list = new LinkedList<DefaultGraphCell>();
		list.add(this);
		return list;
	}
}