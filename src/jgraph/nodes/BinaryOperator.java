package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.util.LinkedList;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;

public abstract class BinaryOperator extends TreeNode {
	private static final long serialVersionUID = 1L;
	
	private TreeNode left;
	private TreeNode right;

	public TreeNode getLeft() {
		return left;
	}

	public void setLeft(TreeNode left) {
		this.left = left;
	}

	public TreeNode getRight() {
		return right;
	}

	public void setRight(TreeNode right) {
		this.right = right;
	}

	private void create(TreeNode left, TreeNode right) {
		setLeft(left);
		setRight(right);
	}

	public BinaryOperator(String name) {
		super(name);
		create(null, null);
	}

	public BinaryOperator(String name, TreeNode left) {
		super(name);
		create(left, null);
	}

	public BinaryOperator(String name, TreeNode left, TreeNode right) {
		super(name);
		create(left, right);
	}
	
	@Override
	public String getName() {
		String l = "";
		String r = "";
		if (getLeft() != null) {
			l = getLeft().getName();
		}
		if (getRight() != null) {
			r = getRight().getName();
		}
		String n = toString();
		return n + "(" + l + "," + r + ")";
	}

	@Override
	public boolean isGround() {
		if (getLeft() == null) {
			return false;
		}
		if (getRight() == null) {
			return false;
		}
		return getLeft().isGround() || getRight().isGround();
	}

	public int height() {
		return 1 + Math.max(left.height(), right.height());
	}

	public int leafsCount() {
		return this.left.leafsCount() + this.right.leafsCount();
	}
	
	protected LinkedList<DefaultGraphCell> flatten2(int x, int y) {
		int offset = getLeft().leafsCount();
		LinkedList<DefaultGraphCell> leftList = getLeft().flatten2(x-1, y);
		LinkedList<DefaultGraphCell> rightList = getRight().flatten2(x-1, y+offset);
		
		setPosX(x);
		setPosY(y);
		setUpGraphCell();
		
		DefaultEdge edge1 = createEdge(this, getLeft());
		DefaultEdge edge2 = createEdge(this, getRight());
		
		LinkedList<DefaultGraphCell> list = new LinkedList<DefaultGraphCell>();
		list.add(this);
		list.add(edge1);
		list.add(edge2);
		list.addAll(leftList);
		list.addAll(rightList);
		return list;
	}
}