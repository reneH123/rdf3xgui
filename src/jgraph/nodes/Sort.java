package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class Sort extends UnaryOperator {
	private static final long serialVersionUID = 1L;

	public Sort() {
		super("Sort");
	}

	@Override
	public String toString() {
		return "Sort";
	}
}
