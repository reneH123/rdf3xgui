package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class Selection extends UnaryOperator {
	private static final long serialVersionUID = 1L;

	public Selection() {
		super("Selection");
	}

	@Override
	public String toString() {
		return "Selection";
	}
}
