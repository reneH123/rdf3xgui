package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class MergeJoin extends BinaryOperator {
	private static final long serialVersionUID = 1L;

	public MergeJoin() {
		super("MergeJoin");
	}
	
	@Override
	public String toString() {
		return "MergeJoin";
	}
}
