package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

public class IndexSeek extends Leaf {
	private static final long serialVersionUID = 1L;

	public IndexSeek() {
		super("IndexSeek");
	}

	@Override
	public String toString() {
		return "IndexSeek";
	}
}
