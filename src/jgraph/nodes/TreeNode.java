package jgraph.nodes;

/**
 * @author Rene Haberland
 * @date 01.07.2009
 * 
 */

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;

import javax.swing.ImageIcon;

import jgraph.GraphCellVisitor;
import jgraph.TreeNodeVisitor;

import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

public abstract class TreeNode extends DefaultGraphCell {
	private static final long serialVersionUID = 1L;

	private String detail1;
	private String detail2;
	private int posX;
	private int posY;
	private int nodeDist = 50;
	private int cellSize = 90;

	public TreeNode(String name) {
		super(name);
	}

	public String getDetail1() {
		return detail1;
	}

	public void setDetail1(String detail1) {
		this.detail1 = detail1;
	}

	public String getDetail2() {
		return detail2;
	}

	public void setDetail2(String detail2) {
		this.detail2 = detail2;
	}

	public DefaultGraphCell[] accept(GraphCellVisitor visitor) {
		return visitor.visit(this);
	}

	public String accept(TreeNodeVisitor visitor) {
		return visitor.visit(this);
	}

	@Override
	public abstract String toString();

	// name of the class
	public abstract String getName();

	// check if tree is ground (all nodes have been instantiated)
	public abstract boolean isGround();

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getNodeDist() {
		return nodeDist;
	}

	public void setNodeDist(int nodeDist) {
		this.nodeDist = nodeDist;
	}

	public int getCellSize() {
		return cellSize;
	}

	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}

	protected void setUpGraphCell() {
		GraphConstants.setBounds(getAttributes(), new Rectangle2D.Double(
				getPosX() * (getCellSize() + getNodeDist()), getPosY()
						* (getCellSize() + getNodeDist()), getCellSize(),
				getCellSize()));
		ImageIcon icon = new ImageIcon(getClass().getResource(
				"../../images/query" + toString() + ".gif"));

		GraphConstants.setIcon(getAttributes(), icon);
		// Add a Floating Port
		addPort();
	}

	protected DefaultEdge createEdge(DefaultGraphCell cellSrc,
			DefaultGraphCell cellDest) {
		DefaultEdge edge = new DefaultEdge();

		// Fetch the ports from the new vertices, and connect them with the edge
		edge.setSource(cellDest.getChildAt(0));
		edge.setTarget(cellSrc.getChildAt(0));

		GraphConstants.setLineWidth(edge.getAttributes(), 8);
		GraphConstants.setLineColor(edge.getAttributes(), Color.gray);
		GraphConstants.setLineStyle(edge.getAttributes(),
				GraphConstants.STYLE_ORTHOGONAL);
		GraphConstants.setLineEnd(edge.getAttributes(),
				GraphConstants.ARROW_SIMPLE);
		GraphConstants.setEndFill(edge.getAttributes(), true);

		return edge;
	}

	public abstract int height();

	protected abstract LinkedList<DefaultGraphCell> flatten2(int x, int y);

	public LinkedList<DefaultGraphCell> flatten() {
		return flatten2(height(), 0);
	}

	public abstract int leafsCount();
}